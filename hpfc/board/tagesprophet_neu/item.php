<?php

/**
 * Artikel.
 *
 * Welche Artikel angefordert wird, wird hier überprüft
 * item.php
 *
 * @create      on 20.04.2008
 * @modify      on 15.02.2009
 *
 * @author      Jana Pape (j.pape@dreagan-lin.de)
 *
 * @package     HPFC\Tagesprophet
 *
 * @copyright   2009 Jana Pape
 * All rights reserved.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License (GPL)
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * To read the license please visit http://www.gnu.org/copyleft/gpl.html
 */
declare(strict_types=1);

use Hpfc\DailyProphetArchive\Item;

if (!isset($_GET['readers']) && is_numeric($_GET['item'])) {
    $status = Item::getValue('id_number', $_GET['item'], 'status');
    switch ($status) {
        case 'NEW':
            if ($admin !== null) {
                $objItem = Item::getInstance((int) $_GET['item']);
                $item = $objItem->getData();
            } else {
                $print = 'Der ausgewählte Artikel ist nicht vorhanden';
                $print = "<br /> Sie werden in wenigen Augenblicken zur <a href='?'>aktuellen Ausgabe</a> weitergeleitet.";
                $meta = "<meta http-equiv='Refresh' content='1; url=hpfc_tagesprophet.php'>";
                $file = 'index';
            }
            break;
        case 'NOW':
        case 'ARCHIV':
            $objItem = Item::getInstance((int) $_GET['item']);
            $item = $objItem->getData();
            $title = "$item[title] | " . $title;
            $canonicalUrl = "https://www.hp-fc.de/hpfc/board/hpfc_tagesprophet.php?item=$_GET[item]";
            preg_match("/<img[^>]+src=[\"']([^\"']+)[\"']/", (string) $item['text'], $imgSrcMatches);
            $imageSrc = $imgSrcMatches[1];
            if ($imageSrc && !str_starts_with((string) $imageSrc, 'http')) {
                $imageSrc = 'https://www.hp-fc.de/hpfc/board/' . $imageSrc;
            }
            preg_match("#^((([^\.!?]+[\.!?][^\w<]*)+[\s<]){2})#sU", (string) $item['text'], $summaryMatches);
            $summary = trim(strip_tags((string) $summaryMatches[1]));
            $jsonSummary = str_replace('"', '\"', $summary);
            $imageJson = $imageSrc ? '"image": "' . $imageSrc . '",' : '';
            $dateModified = $item['update_date'] ?? $item['issue_date'];
            $authorType = isset($item['user'][0]['username']) && $item['user'][0]['username'] ? 'Person' : 'Organization';
            $author = isset($item['user'][0]['username']) && $item['user'][0]['username'] ? $item['user'][0]['username'] : 'HP-FC';
            $meta .= "<link rel='canonical' href='$canonicalUrl'>
      <meta name='description' content='$summary Ein Artikel des Tagespropheten, der Zeitung aus der Welt von Harry Potter.'>
      <script type=\"application/ld+json\">
      {
        \"@context\": \"http://schema.org\",
        \"@type\": \"NewsArticle\",
        \"mainEntityOfPage\": {
          \"@type\": \"WebPage\",
          \"@id\": \"$canonicalUrl\"
        },
        \"headline\": \"$item[title]\",
        $imageJson
        \"datePublished\": \"$item[issue_date]\",
        \"dateModified\": \"$dateModified\",
        \"author\": {
          \"@type\": \"$authorType\",
          \"name\": \"$author\"
        },
         \"publisher\": {
          \"@type\": \"Organization\",
          \"name\": \"HP-FC\",
          \"logo\": {
            \"@type\": \"ImageObject\",
            \"url\": \"https://www.hp-fc.de/hpfc/board/tagesprophet_neu/style/images/tp_logo_google.png\",
            \"height\": 60,
            \"width\": 396
          }
        },
        \"description\": \"$jsonSummary\"
      }
      </script>";
            break;
        default:
            $print = 'Der ausgewählte Artikel ist nicht vorhanden';
            $print = "<br /> Sie werden in wenigen Augenblicken zur <a href='?'>aktuellen Ausgabe</a> weitergeleitet.";
            $meta = "<meta http-equiv='Refresh' content='1; url=hpfc_tagesprophet.php'>";
            $file = 'index';
            break;
    }
}
