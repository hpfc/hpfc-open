<?php

/**
 * Sitemap
 *
 * Es wird eine Sitemap über alle Artikel des Tagespropheten ausgegeben, nach dem Standard von https://www.sitemaps.org/.
 *
 * @package     HPFC\Tagesprophet
 * @author      Jannis Grimm (jannis@huffle.de)
 * @version     2.0
 * All rights reserved.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License (GPL)
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * To read the license please visit http://www.gnu.org/copyleft/gpl.html
 */
declare(strict_types=1);

$itemsQuery = $db->query(
    'SELECT item.id_number, item.id_issue, issue.publish_date, item.insert_date, item.update_date
  FROM ' . TITEM . ' item
  INNER JOIN ' . TISSUE . " issue ON (item.id_issue = issue.id_number)
  WHERE
    issue.status IN ('NOW', 'ARCHIV')
  ORDER BY item.id_number DESC"
);

$indexLastMod = '2000-01-01';
$issueLastMods = [];
$items = [];
while ($dbItem = $db->fetch_array($itemsQuery)) {
    $item = [];
    $item['id'] = $dbItem['id_number'];
    $item['lastmod'] = max($dbItem['publish_date'], $dbItem['insert_date'], $dbItem['update_date']);
    $items[] = $item;
    $indexLastMod = max($indexLastMod, $item['lastmod']);
    $issueId = $dbItem['id_issue'];
    $issueLastMods[$issueId] = max($issueLastMods[$issueId] ?? '2000-01-01', $item['lastmod']);
}
?>
