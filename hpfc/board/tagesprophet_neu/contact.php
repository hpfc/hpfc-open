<?php

/**
 * Kontaktausgabeausgabe.
 *
 * Es werden Kontaktanzeigen augegeben,
 * beantwortet oder auch aufgegeben.
 * Falls ein nicht archivierter, vorhandener oder
 * veröffentlichter Kontakt angewählt wird, erscheint
 * Userabhängig eine entsprechende Meldung.
 *
 * contact.php
 *
 * @create      on 20.04.2008
 * @modify      on 15.02.2009
 *
 * @author      Jana Pape (j.pape@dreagan-lin.de)
 *
 * @package     HPFC\Tagesprophet
 *
 * @copyright   2009 Jana Pape
 * All rights reserved.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License (GPL)
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * To read the license please visit http://www.gnu.org/copyleft/gpl.html
 */
declare(strict_types=1);

use Hpfc\DailyProphetArchive\Contact;
use Hpfc\DailyProphetArchive\Issue;

/**
 *  Welche Styledatei muss geladen werden?
 *  Vorgabe, dass Kontaktanzeigen der aktuellen Ausgabe ausgewählt werden.
 */
$file = 'contact';
$issue = Issue::getValue('status', 'NOW', 'id_number');
$cause1 = [
    'id_issue' => $issue,
    'status' => 'NOT NEW',
];
$cause2 = [
    'del' => '0',
];
$cause = array_merge($cause1, $cause2);
$liste = Contact::getList($cause);

$reply['text'] = null;
/**
 * Es wird überprüft ob eine Kontakt-Id übergeben
 * worden ist, und überprüft ob der User die
 * Berechtigung hat, diese Kontaktanzeige zu sehen.
 */
if (is_numeric($_GET['contact'])) {
    $status = Contact::getValue('id_number', $_GET['contact'], 'status');
    switch ($status) {
        case 'NEW':
            if ($admin !== null) {
                $liste = null;
                $objContact = Contact::getInstance($_GET['contact']);
                $contact = $objContact->getData();
            } else {
                $print = 'testDie ausgewählte Kontaktanzeige ist nicht vorhanden';
                $print .= "<br /> Sie werden in wenigen Augenblicken zur <a href='?'>aktuellen Ausgabe</a> weitergeleitet.";
                $meta = "<meta http-equiv='Refresh' content='1; url=hpfc_tagesprophet.php'>";
                $file = 'index';
            }
            break;
        case 'NOW':
        case 'ARCHIV':
            $liste = null;
            $objContact = Contact::getInstance($_GET['contact']);
            $contact = $objContact->getData();
            break;
        default:
            $print = 'Die ausgewählte Kontaktanzeige ist nicht vorhanden';
            $print .= "<br /> Sie werden in wenigen Augenblicken zur <a href='?'>aktuellen Ausgabe</a> weitergeleitet.";
            $meta = "<meta http-equiv='Refresh' content='1; url=hpfc_tagesprophet.php'>";
            $file = 'index';
            break;
    }

    /**
     * Aufgabe einer Kontaktanzeige.
     */
    $liste = [
        '0' => $contact,
    ];
    if ($_GET['action'] === 'reply') {
        $file = 'contact_form';
        $reply['subject'] = 'Antwort auf deine Kontaktanzeige im Tagespopheten von ' . $_POST['username'];
        $reply['text'] = strip_tags(ltrim(rtrim((string) $_POST['contact_reply'])));
        switch ($_POST['php_submit']) {
            case $language['PREVIEW']:
                // Vorschau der Antwort
                $contact_reply = $reply;
                break;
            case $language['send']:
                // Antwort senden
                mailer($contact['email'], $reply['subject'], $reply['text']);
                $print = 'Die Antwort wurde an ' . $contact['username'] . ' übermittelt';
                $file = 'index';
                break;
            default:
                $reply = [];
                break;
        }
    }
}

if ($_GET['issue'] !== 0) {
    $cause1 = [
        'id_issue' => $_GET['issue'],
        'status' => 'NOT NEW',
    ];
    $cause2 = [
        'del' => '0',
    ];
    $cause = array_merge($cause1, $cause2);
    $liste = Contact::getList($cause);
}
/**
 * Es wird eine Kontaktanzeige aufgegeben.
 */
if ($_GET['action'] === 'new') {
    $file = 'contact_form';
    $contact_temp['username'] = $_POST['username'];
    $contact_temp['userid'] = $_POST['userid'];
    $contact_temp['email'] = $wbbuserdata['email'];
    $array = [
        '\"' => '"',
    ];
    $text = strtr($_POST['contact_text'], $array);
    $contact_temp['text'] = strip_tags($text);
    $contact_temp['title'] = strip_tags((string) $_POST['title']);
    $objContact = Contact::getInstance();
    $objContact->checkData($contact_temp);
    $contact = $objContact->getData();
    if ($contact['text_error'] !== '' || $contact['title_error'] !== '') {
        $_POST['php_submit'] = $language['PREVIEW'];
    }
    switch ($_POST['php_submit']) {
        case $language['PREVIEW']:
            //TODO Vorschau Antwort

            break;
        case $language['send']:
            // Antwort senden
            $objContact->insertData();
            $print = 'Die Kontaktanzeige wurde aufgegeben und wird in einer der n&auml;chsten Ausgaben erscheinen.';
            $file = 'index';
            break;
        default:
            $contact = [];
            break;
    }
}
