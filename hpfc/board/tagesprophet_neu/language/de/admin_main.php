<?php

/**
 * Sprachvariablen im Adminbereich
 *
 * admin_main.php
 * @create      on 20.04.2008
 * @modify      on 15.02.2009
 *
 *
 * @author      Jana Pape (j.pape@dreagan-lin.de)
 * @version     2.0
 * @package     HPFC\Tagesprophet\Administration
 * @copyright   2009 Jana Pape
 * All rights reserved.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License (GPL)
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * To read the license please visit http://www.gnu.org/copyleft/gpl.html
 *
 *
 *
 */
declare(strict_types=1);

$language['SAVE'] = 'Speichern';
$language['ISSUEPUBLISH'] = 'In neuer Ausgabe ver&ouml;ffentlichen';
$language['INISSUE'] = 'In aktueller Ausgabe ver&ouml;ffentlichen';
$language['ISSUENEW'] = 'Ausgabe in Vorbereitung';
$language['ISSUENOW'] = 'Aktuelle Ausgabe';
$language['ALLISSUE'] = 'Alle Ausgaben';
$language['EDIT'] = '&Uuml;bernehmen';
$language['NEWITEM'] = 'Neuer Artikel';
$language['NEWISSUE'] = 'neue Ausgabe';
$language['SUMMARY'] = 'Zusammenfassung';
$language['CATEGORY'] = 'Kategorie';
$language['KIND'] = 'Art';
$language['ID'] = 'Sch&uuml;lerID';
$language['AUTHORDRAWER1'] = 'Anzahl Autor und Zeichner';
$language['AUTHORDRAWER2'] = 'Autor und Zeichner';
$language['SAVEITEM'] = 'Artikel Speichern';
$language['SAVEISSUE'] = 'In Ausgabe übernehmen';
$language['deleteItem'] = 'Der Artikel wurde erfolgreich gel&ouml;scht.';
$language['saveItem'] = 'Der Artikel wurde erfolgreich gespeichert.';
$language['ADMIN'] = 'Administration';
$language['WAITING'] = 'Warteschleife';
$language['PUBLISH'] = 'Ver&ouml;ffetnlichen';
$language['red_welcome'] = 'Willkommen im redaktionellen Bereich des Tagespropheten';
$language['correct'] = 'korrigiert? ';
$language['remove'] = 'entfernen';
$language['upload'] = 'Bild hochladen';
$language['first_correct'] = 'Erstkorrektur';
$language['second_correct'] = 'Zweitkorrektur';
$language['sign'] = 'Endabnahme';
$language['INNEW'] = 'In neue Ausgabe verschieben';
$language['user'] = 'User';
?>
