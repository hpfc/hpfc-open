<?php

/**
 * Sprachvariablen
 *
 * main.php
 * @create      on 20.04.2008
 * @modify      on 15.02.2009
 *
 *
 * @author      Jana Pape (j.pape@dreagan-lin.de)
 * @version     2.0
 * @package     HPFC\Tagesprophet
 * @copyright   2009 Jana Pape
 * All rights reserved.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License (GPL)
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * To read the license please visit http://www.gnu.org/copyleft/gpl.html
 *
 *
 *
 */
declare(strict_types=1);

$language['PREVIEW'] = 'Vorschau';
$language['ISSUENOW'] = 'Aktuelle Ausgabe';
$language['ALLISSUE'] = 'Alle Ausgaben';
$language['RESET'] = 'Zur&uuml;cksetzen';
$language['ITEM'] = 'Artikel';
$language['ALLITEM'] = 'Alle Artikel';
$language['NEWITEM'] = 'Neuer Artikel';
$language['ISSUE'] = 'Ausgabe';
$language['ISSUENR'] = 'Ausgabe Nummer ';
$language['NEWISSUE'] = 'neue Ausgabe';
$language['SUMMARY'] = 'Zusammenfassung';
$language['CATEGORY'] = 'Kategorie';
$language['AUTHOR'] = 'Autor';
$language['DRAWER'] = 'Zeichner';
$language['KIND'] = 'Art';
$language['START'] = 'Los';
$language['TITLE'] = 'Titel:';
$language['CATAGORY'] = 'Kategorie';
$language['reader'] = 'Leserbriefe';
$language['adver'] = 'Kontaktanzeigen';
$language['Archiv'] = 'Archiv';
$language['more'] = 'Weiterlesen';
$language['name'] = 'Tagesprophet';
$language['issueno'] = 'Ausgabe Nummer ';
$language['readers'] = 'Leserbriefe';
$language['wrotefrom'] = 'geschrieben von ';
$language['imagesfrom'] = 'Bilder von ';
$language['edit'] = 'Bearbeiten';
$language['send'] = 'Abschicken';
$language['RESET'] = 'Zur&uuml;cksetzen';
$language['text'] = 'Text:';
$language['contact'] = 'Kontaktanzeigen';
$language['page'] = 'Seite';
$language['readers_write'] = 'Leserbrief schreiben';
$language['readers_publish'] = 'Es wurden bisher noch keine Leserbriefe f&uuml;r diesen Artikel ver&ouml;ffentlicht.'

?>
