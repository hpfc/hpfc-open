<?php

/**
 * Archivausgabe.
 *
 * Es werden Einzelne Artikel, Ausgaben, Kategorien
 * angezeigt, die archiviert wurden.
 * Falls ein nicht archivierter, vorhandener oder
 * veröffentlichter Artikel angewählt wird, erscheint
 * Userabhängig eine entsprechende Meldung.
 *
 * archiv.php
 *
 * @create      on 20.04.2008
 * @modify      on 15.02.2009
 *
 * @author      Jana Pape (j.pape@dreagan-lin.de)
 *
 * @package     HPFC\Tagesprophet
 *
 * @copyright   2009 Jana Pape
 * All rights reserved.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License (GPL)
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * To read the license please visit http://www.gnu.org/copyleft/gpl.html
 */
declare(strict_types=1);

use Hpfc\DailyProphetArchive\Contact;
use Hpfc\DailyProphetArchive\Issue;
use Hpfc\DailyProphetArchive\Item;
use Hpfc\DailyProphetArchive\Readers;

/**
 * Auflistung wird auf nicht gelöschte und archivierte Artikel eingeschränkt.
 */
$cause2 = [
    'del' => '0',
];
$cause1 = [
    'status' => 'ARCHIV',
];
$cause = [...$cause1, ...$cause2];
$anzahl = 3 * $config['archivintervall'];
$order = ['publish_date'];
$archivliste = Issue::getList($cause, $order);
$file = 'archiv';

/**
 * Falls eine Item-Id übergeben worden ist,
 * wird überprüft, ob dieser Artikel archiviert ist,
 * bei aktuellen Artikeln wird in die aktuelle
 * Ausgabe umgeleitet, bei neuen Artikel wird geprüft,
 * ob der User die Berechtigung hat, diesen Artikel zu sehen.
 */
if (is_numeric($_GET['item'])) {
    $anzahl = 0;
    switch (Item::getValue('id_number', $_GET['item'], 'status')) {
        case 'ARCHIV':
            $objItem = Item::getInstance((int) $_GET['item']);
            $item = $objItem->getData();
            $file = 'item';
            break;
        case 'NOW':
            $print = 'Der ausgewählte Artikel befindet sich in der aktuellen Ausgabe';
            $print = "<br /> Sie werden in wenigen Augenblicken zur <a href='?'>dem Artikel</a> weitergeleitet.";
            $meta = "<meta http-equiv='Refresh' content='1; url=hpfc_tagesprophet.php?mod=issue&item=$_GET[item]'>";
            $file = 'index';
            break;
        case 'NEW':
            if ($admin === null) {
                $print = 'Der ausgewählte Artikel existiert nicht.';
                $file = 'index';
            } else {
                $print = 'Der ausgewählte Artikel wurde noch nicht veröffentlicht';
                $print = "<br /> Sie werden in wenigen Augenblicken zu <a href='?'>dem Artikel</a> weitergeleitet.";
                $meta = "<meta http-equiv='Refresh' content='1; url=hpfc_tagesprophet.php?mod=item&item=$_GET[item]'>";
                $file = 'index';
            }
            break;
        default:
            $print = 'Der ausgewählte Artikel existiert nicht.';
            $file = 'index';
            break;
    }
}

/**
 * Falls eine Id für einen Leserbrief
 * übergeben worden ist,
 * wird überprüft, ob dieser Leserbrief archiviert ist,
 * bei aktuellen Leserbriefen wird in die aktuelle
 * Ausgabe umgeleitet, bei neuen Leserbriefen wird geprüft,
 * ob der User die Berechtigung hat, diesen Leserbrief zu sehen.
 */
if (isset($_GET['readers'])) {
    $anzahl = 0;
    if (is_numeric($_GET['readers'])) {
        switch (Readers::getValue('id_number', $_GET['readers'], 'Status')) {
            case 'ARCHIV':
                $objReaders = Readers::getInstance($_GET['readers']);
                $readers = $objReaders->getData();
                $file = 'readers';
                break;
            case 'NOW':
                $print = 'Der ausgewählte Leserbrief befindet sich in der aktuellen Ausgabe';
                $print = "<br /> Sie werden in wenigen Augenblicken zur <a href='?'>dem Leserbrief</a> weitergeleitet.";
                $meta = "<meta http-equiv='Refresh' content='1; url=hpfc_tagesprophet.php?mod=issue&readers=$_GET[readers]'>";
                $file = 'index';
                break;
            case 'NEW':
                if ($admin === null) {
                    $print = 'Der ausgewählte Leserbrief existiert nicht.';
                    $file = 'index';
                } else {
                    $print = 'Der ausgewählte Leserbrief wurde noch nicht veröffentlicht';
                    $print = "<br /> Sie werden in wenigen Augenblicken zur <a href='?'>dem Leserbrief</a> weitergeleitet.";
                    $meta = "<meta http-equiv='Refresh' content='1; url=hpfc_tagesprophet.php?&mod=readers&readers=$_GET[readers]'>";
                    $file = 'index';
                }
                break;
            default:
                $print = 'Der ausgewählte Leserbrief existiert nicht.';
                $file = 'index';
                break;
        }
    } else {
        $cause1 = [
            'status' => 'ARCHIV',
        ];
        $cause = [...$cause1, ...$cause2];
        $liste = Readers::getList($cause);

        $file = 'issue';
    }
}

/**
 * Falls eine Konakt-Id übergeben worden ist,
 * wird überprüft, ob dieser Konakt archiviert ist,
 * bei aktuellen Kontakt wird in die aktuelle
 * Ausgabe umgeleitet, bei neuen Kontakt wird geprüft,
 * ob der User die Berechtigung hat, diesen Kontakt zu sehen.
 */
if (isset($_GET['contact'])) {
    if (is_numeric($_GET['contact'])) {
        switch (Contact::getValue('id_number', $_GET['contact'], 'status')) {
            case 'ARCHIV':
                $objContact = Contact::getInstance($_GET['contact']);
                $contact = $objContact->getData();
                $file = 'contact';
                break;
            case 'NOW':
                $print = 'Die ausgewählte Kontaktanzeige befindet sich in der aktuellen Ausgabe';
                $print .= "<br /> Sie werden in wenigen Augenblicken zur <a href='?'>der Kontaktanzeige</a> weitergeleitet.";
                $meta = "<meta http-equiv='Refresh' content='1; url=hpfc_tagesprophet.php?contact=$_GET[contact]'>";
                $file = 'index';
                break;
            case 'NEW':
                if ($admin === null) {
                    $print = 'Die ausgewählte Kontaktanzeige existiert nicht.';
                    $print .= "<br /> Sie werden in wenigen Augenblicken zur <a href='?'>aktuellen Ausgabe</a> weitergeleitet.";
                    $meta = "<meta http-equiv='Refresh' content='1; url=hpfc_tagesprophet.php'>";
                    $file = 'index';
                } else {
                    $print = 'Die ausgewählte Kontaktanzeige wurde noch nicht veröffentlicht';
                    $print .= "<br /> Sie werden in wenigen Augenblicken zur <a href='?'> Kontaktanzeige</a> weitergeleitet.";
                    $meta = "<meta http-equiv='Refresh' content='1; url=hpfc_tagesprophet.php?contact=$_GET[contact]'>";
                    $file = 'index';
                }
                break;
            default:
                $print = 'Die ausgewählte Kontaktanzeige existiert nicht.';
                $file = 'index';
                break;
        }
    } else {
        $cause1 = [
            'status' => 'ARCHIV',
        ];
        $cause = [...$cause1, ...$cause2];
        $liste = Contact::getList($cause);
        $file = 'issue';
    }
}
/**
 * Archivierte Ausgabe wird angezeigt, wenn
 * eine Ausgaben-Id übergeben worden ist.
 * Dabei wird eine Vorschau von jeden Artikel,
 * Leserbreif bzw. Kontaktanzeige angezeigt.
 */
if (is_numeric($_GET['issue']) && !isset($_GET['readers']) && !isset($_GET['contact'])) {
    $anzahl = 0;
    $objIssue = Issue::getInstance((int) $_GET['issue']);
    $issue = $objIssue->getData();
    $issue_header = "<p class='number'>" . $language['issueno'] . $issue['number'] . "</p>\n";
    $issue_header .= "<p class='date'>" . date('d.m.Y', strtotime((string) $issue['publish_date'])) . "</p></div>\n";
    $cause1 = [
        'id_issue' => $_GET['issue'],
    ];
    $cause = [...$cause1, ...$cause2];
    $liste1 = Item::getList($cause);
    $liste2 = Readers::getList($cause);
    $liste3 = Contact::getList($cause);
    /**
     * Folgende IF-Abfragenüberprüfen, ob die einzelnen
     * Arrays leer sind und vereinnen diese ggf.
     */
    if (empty($liste1)) {
        if (empty($liste2)) {
            if (empty($liste3)) {
                $print = 'In dieser Ausgabe befindet sich kein Artikel.';
                $file = 'index';
            } else {
                $liste = $liste3;
            }
        } elseif (empty($liste3)) {
            $liste = $liste2;
        } else {
            $liste = array_merge($liste2, $liste3);
        }
    } elseif (empty($liste2)) {
        $liste = empty($liste3) ? $liste1 : array_merge($liste1, $liste3);
    } elseif (empty($liste3)) {
        $liste = array_merge($liste1, $liste2);
    } else {
        $liste = array_merge($liste1, $liste2, $liste3);
    }
    $file = 'issue';
}

/**
 * Artikel, die archiviert wurden, werden
 * aufgelistet pro Kategorie oder die
 * Kategorien selbst können ausgewählt werden.
 */
if (isset($_GET['category'])) {
    if (is_numeric($_GET['category']) && is_numeric($_GET['issue'])) {
        $cause1 = [
            'category' => $_GET['category'],
            'id_issue' => $_GET['issue'],
        ];
        $cause = array_merge($cause1, $cause2);
        $liste = Item::getList($cause);
        $file = 'liste';
    } elseif (is_numeric($_GET['category'])) {
        $cause1 = [
            'category' => $_GET['category'],
            'status' => 'ARCHIV',
        ];
        $cause = array_merge($cause1, $cause2);
        $liste = Item::getList($cause);
        $file = 'issue';
    } else {
        /**
         * Auflistung aller Kategorien.
         */
        $liste = $catList;
        $file = 'liste';
    }
}

if (isset($liste) && count($liste) > 0) {
    $anzahl = count($liste);
}
