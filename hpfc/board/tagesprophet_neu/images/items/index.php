<?php

declare(strict_types=1);

use Hpfc\DailyProphetArchive\Config;

require_once __DIR__ . '\..\..\..\..\..\bootstrap.php';

$mod = null;
$globalpath = '../../';
$scriptpath = '../';
$server = 'http://www.meine-test-domain.de/';

$includepath = $scriptpath . 'include/';
$adminpath = $scriptpath . 'admin/';
$languagepath = $scriptpath . 'language/de/';
$stylepath = $scriptpath . 'style/';
$imagepath = $scriptpath . 'images/';
/**
 * Überprüfung, ob User Berechtigung hat im redaktionellen BEreich zu arbeiten.
 * Wenn nicht, dann wird ihm mitgeteilt, dass er keine Berechtigung hat und sich einloggen soll (Standardmeldung vom Forum).
 */
if ($wbbuserdata['admin'] !== 1 && $wbbuserdata['canpresse'] !== 1 && $wbbuserdata['TP'] !== 1 && $wbbuserdata['is_techno'] !== 1) {
    echo 'access_error';
    access_error();
} else {
    include_once($includepath . 'tables.php');
    include_once($languagepath . 'main.php');
    include_once($languagepath . 'admin_main.php');
    include_once($includepath . 'class.php');

    $config = Config::loadData();

    if (isset($_GET['mod'])) {
        $mod = strtolower((string) $_GET['mod']);
    }
    $file = $mod;
    switch ($mod) {
        case 'item':
        case 'issue':
        case 'readers':
        case 'contact':
        case 'admin':
            break;
        default:
            $file = 'index';
            $temp = '<p>' . $lang['red_welcome'] . '</p>';
            break;
    }
    $title = $lang['name'] . ' - Redaktion';
    include_once($adminpath . $file . '.php');
    include_once($adminpath . 'header.tpl.php');
    include_once($adminpath . $file . '.tpl.php');
    include_once($adminpath . 'footer.tpl.php');
    print $ausgabe;
}
