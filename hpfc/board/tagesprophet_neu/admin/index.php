<?php

/**
 * Coordination the adminarea.
 *
 * @package    HPFC\Tagesprophet\Administration
 *
 * @author    Jana Pape
 * @copyright    2009 Jana Pape
 * All rights reserved.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License (GPL)
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * To read the license please visit http://www.gnu.org/copyleft/gpl.html
 */
declare(strict_types=1);

use Hpfc\DailyProphetArchive\Config;

require_once __DIR__ . '/../../../../bootstrap.php';

$server = $_SERVER['HTTP_HOST'];

/**
 * Überprüfung, ob User Berechtigung hat, im redaktionellen Bereich zu arbeiten.
 * Wenn nicht, dann wird ihm mitgeteilt, dass er keine Berechtigung hat und sich einloggen soll (Standardmeldung vom Forum).
 */

if ($wbbuserdata['admin'] !== 1 && $wbbuserdata['canpresse'] !== 1 && $wbbuserdata['TP'] !== 1 && $wbbuserdata['is_techno'] !== 1) {
    access_error();
} else {
    include_once($includepath . 'tables.php');
    include_once($languageuagepath . 'main.php');
    include_once($languageuagepath . 'admin_main.php');
    $config = Config::loadData();
    if (isset($_GET['admin'])) {
        $admin = strtolower((string) $_GET['admin']);
    }
    $file = $admin;
    switch ($admin) {
        case 'item':
        case 'issue':
        case 'readers':
        case 'contact':
        case 'admin':
            break;
        case 'upload':
            $file = 'upload';
            break;
        default:
            $file = 'index';
            $temp = '<p>' . $language['red_welcome'] . '</p>';
            break;
    }
    $title = $language['name'] . ' - Redaktion';
    include_once($adminpath . $file . '.php');
    if ($file !== 'upload') {
        include_once($adminpath . 'header.tpl.php');
        include_once($adminpath . $file . '.tpl.php');
        include_once($adminpath . 'footer.tpl.php');
        print $ausgabe;
    }
}
