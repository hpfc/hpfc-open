<?php

/**
 * Shows the admin page (Template).
 *
 * @package    HPFC\Tagesprophet\Administration
 * @author    Jana Pape
 * @copyright    2009 Jana Pape
 * All rights reserved.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License (GPL)
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * To read the license please visit http://www.gnu.org/copyleft/gpl.html
 */
declare(strict_types=1);

$ausgabe .= '<h4>Administration</h4>';
$ausgabe .= $text;
$ausgabe .= "<form method='post' action='?mod=admin&admin=admin'>\n";
$ausgabe .= "<label for='name'>Name: <input type='text' name='name' size='50' value='" . $config['name'] . "' /></label><br />\n";
$ausgabe .= "<label for='home_path'>HomePath: <input type='text' name='home_path' size='50' value='" . $config['home_path'] . "' /></label><br />\n";
$ausgabe .= "<label for='dir_path'>Verzeichnis: <input type='text' name='dir_path' size='50' value='" . $config['dir_path'] . "' /></label><br />\n";
$ausgabe .= "<label for='intervall'>Artikel pro Seite: <input type='text' name='intervall' size='50' value='" . $config['intervall'] . "' /></label><br />\n";
$ausgabe .= "<label for='readers'>Leserbriefe: <textarea name='readers_notice' cols='80' rows='5'>" . $config['readers_notice'] . "</textarea></label><br />\n";
$ausgabe .= "<label for='readers_email'>Leserbriefautoren automatisch informieren <input type='checkbox' name='readers_email' value='1'";
if ($config['readers_email'] == 1) {
    $ausgabe .= ' checked';
}
$ausgabe .= '></label><br />';
$ausgabe .= "<label for='contact'>Anzeigen: <textarea name='contact_notice' cols='80' rows='5'>" . $config['contact_notice'] . "</textarea></label><br />\n";
$ausgabe .= "<label for='contact_email'>Anzeigenautoren automatisch informieren <input type='checkbox' name='contact_email' value='1'";
if ($config['contact_email'] == 1) {
    $ausgabe .= ' checked';
}
$ausgabe .= '></label><br />';
$ausgabe .= "<input type='submit' name='php_submit' value='" . $language['SAVE'] . "' />\n";
$ausgabe .= "<input type='reset' name='php_reset' value='" . $language['RESET'] . "' />\n";
$ausgabe .= "</form>\n";

$ausgabe .= "<form method='post' action='?mod=admin&admin=admin&delete'>\n";
$ausgabe .= "<input type='submit' name='php_submit' value='Papierkorb leeren' />\n";
$ausgabe .= "</form>\n";
?>
