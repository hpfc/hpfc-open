<?php

/**
 * Shows in the admin area the header(Template).
 *
 * @package    HPFC\Tagesprophet\Administration
 * @author    Jana Pape
 * @copyright    2009 Jana Pape
 * All rights reserved.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License (GPL)
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * To read the license please visit http://www.gnu.org/copyleft/gpl.html
 */
declare(strict_types=1);

$ausgabe = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">';
$ausgabe .= '<html xmlns="http://www.w3.org/1999/xhtml"><head>';
$ausgabe .= "\n";
if ($file == 'itemedit') {
    $ausgabe .= "<script type='text/javascript' src='" . $includepath . "tiny_mce/tiny_mce.js'></script>\n";
    $ausgabe .= "<script type='text/javascript' src='" . $includepath . "tiny_mce.js'></script>\n";
    $ausgabe .= "<script language='javascript' type='text/javascript'>\n";

    $ausgabe .= "<!--\n";
    $ausgabe .= "var win=null;\n";
    $ausgabe .= "onerror = stopError;\n";
    $ausgabe .= "function stopError(){\n";
    $ausgabe .= "	return true;\n";
    $ausgabe .= "}\n";
    $ausgabe .= "function pic_upload(){\n";
    $ausgabe .= "	myleft=20;\n";
    $ausgabe .= "	mytop=20;\n";
    $ausgabe .= "	settings='width=640,height=480,top=' + mytop + ',left=' + myleft + ',scrollbars=no,location=no,directories=no,status=no,menubar=no,toolbar=no,resizable=no,dependent=no';\n";
    $ausgabe .= "	win=window.open('hpfc_tagesprophet.php?mod=admin&admin=upload&item=" . $item['id_number'] . "','mypopup',settings);\n";
    $ausgabe .= "	win.focus();\n";
    $ausgabe .= "}\n";
    $ausgabe .= "// -->\n";
    $ausgabe .= "</script>\n";
}
$ausgabe .= "<link rel='stylesheet' type='text/css' href='" . $adminpath . "style.css'>\n";
$ausgabe .= '<title>' . $title . '</title></head>';
$ausgabe .= "<body>\n<div id='wrap'>";
$ausgabe .= "<div id='header'><h1>" . $language['name'] . "</h1>\n";
$ausgabe .= '<p>' . $issue_header . "</p>\n";
$ausgabe .= "<ul id='nav'>\n<li><a  href='?'>Aktuelle Ausgabe</a></li>\n";
$ausgabe .= '<li><a' . $archiv_active . " href='?mod=archiv'>Archiv</a></li>\n";
$ausgabe .= "<li class='active'><a href='?mod=admin' class='active'>Redaktion</a></li>\n";
$ausgabe .= "</ul>\n";
$ausgabe .= "<ul id='admin_nav'>\n<li  " . $item_active . "><a href='?mod=admin&admin=item' " . $item_active . '>' . $language['ITEM'] . "</a></li>\n";
$ausgabe .= '<li ' . $issue_active . '><a ' . $issue_active . " href='?mod=admin&admin=issue'>" . $language['ISSUE'] . "</a></li>\n";

if ($wbbuserdata['admin'] == 1 || $wbbuserdata['TP'] == 1 || $wbbuserdata['is_techno'] == 1) {
    $ausgabe .= '<li ' . $admin_active . '><a ' . $admin_active . " href='?mod=admin&admin=admin'>" . $language['ADMIN'] . "</a></li>\n";
}
$ausgabe .= "<li><a href='?mod=admin&admin=issue&action=edit&issue='>" . $language['NEWISSUE'] . "</a></li>\n";
$ausgabe .= '<li ' . $readers_active . '><a ' . $readers_active . " href='?mod=admin&admin=readers'>" . $language['reader'] . "</a></li>\n";
$ausgabe .= '<li ' . $anzeige_active . '><a  ' . $anzeige_active . " href='?mod=admin&admin=contact'>" . $language['adver'] . "</a></li>\n";
$ausgabe .= "</ul>\n";
$ausgabe .= $item_nav;
$ausgabe .= $readers_nav;
$ausgabe .= $contact_nav;
$ausgabe .= "</div>\n<div id='main'>";
