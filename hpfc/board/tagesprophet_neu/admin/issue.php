<?php

/**
 * Shows in the admin area the issue page.
 *
 * @package    HPFC\Tagesprophet\Administration
 *
 * @author    Jana Pape
 * @copyright    2009 Jana Pape
 * All rights reserved.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License (GPL)
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * To read the license please visit http://www.gnu.org/copyleft/gpl.html
 */
declare(strict_types=1);

use Hpfc\DailyProphetArchive\Contact;
use Hpfc\DailyProphetArchive\Issue;
use Hpfc\DailyProphetArchive\IssueAdmin;
use Hpfc\DailyProphetArchive\Item;
use Hpfc\DailyProphetArchive\Readers;

$issue = isset($_GET['issue']) ? strtoupper((string) $_GET['issue']) : '';
$cause2 = [
    'del' => '0',
];
$cause = [];
$order = ['id_number'];
$preview = null;
$file = 'issue';
$issue_active = "class='active'";

switch ($issue) {
    case 'ALL':
        $title .= 'Alle Ausgaben';
        $all_active = "class='active'";
        break;
    case 'NOW':
        $title .= 'Aktuelle Ausgabe';
        $cause1 = [
            'status' => 'NOW',
        ];
        $nowissue_active = "class='active'";
        break;
    case 'NEW':
        $title .= 'Ausgabe in Vorbereitung';
        $cause1 = [
            'status' => 'NEW',
        ];
        $newissue_active = "class='active'";
        break;
    case 'ARCHIV':
        $title .= 'Archivierte Ausgaben';
        $cause1 = [
            'status' => 'ARCHIV',
        ];
        $archiv_active = "class='active'";
        break;
    case 'DEL':
        if ($wbbuserdata['admin'] === 1 || $wbbuserdata['TP'] === 1 || $wbbuserdata['is_techno'] === 1) {
            $title .= 'Gel&ouml;schte Ausgaben';
            $cause2 = [
                'del' => '1',
            ];
            $del_active = "class='active'";
        } else {
            $title .= 'Alle Ausgaben';
            $all_active = "class='active'";
        }
        break;
    default:
        $action = isset($_GET['action']) ? strtolower((string) $_GET['action']) : '';
        switch ($action) {
            case 'publish':
                IssueAdmin::publishData($_GET['issue']);
                $file = 'index';
                $temp = '<p> Die Ausgabe wurde veröffentlicht</p>';
                break;
            case 'unpublish':
                IssueAdmin::unpublishData();
                $file = 'index';
                $temp = '<p> Die akteulle Ausgabe wurde zurückgesetzt.</p>';
                break;
            case 'edit':
                if (isset($_POST['php_submit'])) {
                    $objIssue = IssueAdmin::getInstance($_GET['issue']);
                    if ($_GET['issue_id'] !== 0) {
                        $cause = [
                            'id_issue' => $_GET['issue'],
                        ];
                        $itemliste = Item::getList($cause);
                        $readersliste = Readers::getList($cause);
                        $contactliste = Contact::getList($cause);
                    }
                    $temp_issue = [
                        'number' => $_POST['number'],
                        'text' => $_POST['my_text'],
                        'id_number' => $_POST['issue']['id_number'],
                    ];
                    $objIssue->checkData($temp_issue);
                    $issue = $objIssue->getData();
                    switch ($_POST['php_submit']) {
                        case $language['SAVE']:
                            // Speichern
                            $issue_id = $objIssue->saveData();
                            $temp = '<p>Die Ausgabe ' . $issue['number'] . ' wurde erfolgreich gespeichert.</p>';
                            $file = 'index';
                            break;
                        case $language['RESET']:
                            // Zur�cksetzen
                            $issue = [];
                            break;
                        default:
                            break;
                    }
                } else {
                    $objIssue = IssueAdmin::getInstance($_GET['issue']);
                    $issue = $objIssue->getData();
                    if ($_GET['issue'] !== 0) {
                        $cause = [
                            'id_issue' => $_GET['issue'],
                        ];
                        $itemliste = Item::getList($cause);
                        $readersliste = Readers::getList($cause);
                        $contactliste = Contact::getList($cause);
                    } else {
                        $issue_new = IssueAdmin::getIssue('NEW');
                        $issue_id = $issue_new['id_number'];
                        if ($issue_id !== 0) {
                            $file = 'index';
                            $temp = 'Es existiert schon eine Ausgabe in Vorbereitung.';
                        }
                        $cause = [
                            'id_issue' => $issue_id,
                        ];
                        $itemliste = Item::getList($cause);
                        $readersliste = Readers::getList($cause);
                        $contactliste = Contact::getList($cause);
                    }
                    $file = 'issueedit';
                }
                break;
            case 'del':
                $file = 'index';
                $objIssue = IssueAdmin::getInstance($_GET['issue']);
                $issue = $objIssue->getData();
                switch ($_GET['del']) {
                    case 'all':
                        $objIssue->delData('all');
                        $temp = '<p> Die Ausgabe ' . $issue['number'] . ' wurde erfolgreich gel&ouml;scht.</p>';
                        break;
                    case 'issue':
                        $objIssue->delData('issue');
                        $temp = '<p> Die Ausgabe ' . $issue['number'] . ' wurde erfolgreich gel&ouml;scht und die Artikel zurück in die Warteschleife geschoben.</p>';
                        break;
                    default:
                        $temp = '<p> Soll die gesamte Ausgabe ' . $issue['number'] . ' inklusive all ihrer Artikel gelöscht werden?</p>';
                        $temp .= "<form method='post' action='?mod=admin&admin=issue&action=del&issue=" . $issue['number'] . "&del=all'>\n<input type='submit' name='issue_submit' value='Alles L&ouml;schen' />\n</form>";
                        if ($issue['status'] !== 'ARCHIV') {
                            $temp .= '<p>soll nur die Ausgabe ' . $issue['number'] . ' gelöscht werden und die Artikel zurück in die Warteschleife geschoben werden?</p>';
                            $temp .= "<form method='post' action='?mod=admin&admin=issue&action=del&issue=" . $issue['number'] . "&del=issue'>\n<input type='submit' name='issue_submit' value='Ausgabe L&ouml;schen' />\n</form>";
                        }
                        break;
                }
                break;
            case 'undel':
                $file = 'index';
                $objIssue = IssueAdmin::getInstance($_GET['issue']);
                $issue = $objIssue->getData();
                $objIssue->undelete($_GET['issue']);
                $temp = '<p> Die Ausgabe <i>' . $issue['number'] . '</i> wurde erfolgreich wiederhergestellt.</p>';
                break;
            default:
                $title .= 'Alle Ausgaben';
                $file = 'issue';
                $all_active = "class='active'";
                break;
        }
        break;
}

$cause = $cause1 !== null ? array_merge($cause1, $cause2) : $cause2;
if ($file === 'issue') {
    $liste = Issue::getList($cause, $order, 'DESC');
}

$item_nav = "<ul id='item_nav'>";
$item_nav .= '<li  ' . $newissue_active . '><a ' . $newissue_active . " href='?mod=admin&admin=issue&issue=new'>in Vorbereitung</a></li>\n";
$item_nav .= '<li  ' . $nowissue_active . '><a ' . $nowissue_active . " href='?mod=admin&admin=issue&issue=now'>aktuelle Ausgabe(ver&ouml;ffentlicht)</a></li>\n";
$item_nav .= '<li  ' . $archiv_active . '><a ' . $archiv_active . " href='?mod=admin&admin=issue&issue=archiv'>Archivierte Ausgaben</a></li>\n";
if ($wbbuserdata['admin'] === 1 || $wbbuserdata['canpresse'] === 1 || $wbbuserdata['is_techno'] === 1) {
    $item_nav .= '<li  ' . $del_active . '><a  ' . $del_active . " href='?mod=admin&admin=issue&issue=del'>gel&ouml;schte Ausgaben</a></li>";
}
$item_nav .= '</ul>';
