<?php

/**
 * Shows in the admin area the item page (Template) .
 *
 * @package    HPFC\Tagesprophet\Administration
 * @author    Jana Pape
 * @copyright    2009 Jana Pape
 * All rights reserved.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License (GPL)
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * To read the license please visit http://www.gnu.org/copyleft/gpl.html
 */
declare(strict_types=1);

if ($liste == null) {
    $ausgabe .= 'Keine Artikel gefunden';
} else {
    $i = 0;
    $ausgabe .= "<table width='100%'><tr><th></th><th width='40%'>Titel</th><th>Korrigiert</th><th>Kategorie</th><th>Haus</th><th>Datum</th><th>Bearbeiten</th></tr>";
    foreach ($liste as $item) {
        if ($i % 2 != 0) {
            $ausgabe .= "<tr class='ungerade'>";
        } else {
            $ausgabe .= "<tr class='gerade'>";
        }
        //ausgabennummer
        $ausgabe .= "<td><a href='?mod=admin&admin=issue&action=edit&issue=" . $item['id_issue'] . "'>" . $item['issue_number'] . "</a></td>\n";
        //Titel
        $ausgabe .= "<td><a href='?mod=admin&admin=item&action=edit&item=" . $item['id_number'] . "'>" . $item['title'] . "</a></td>\n";
        //Korgiert
        $ausgabe .= '<td>' . $item['correct'] . "</td>\n";
        //Kategorie
        $ausgabe .= '<td>' . $item['category_name'] . "</td>\n";
        //Haus
        $ausgabe .= '<td>' . $item['haus'] . '</td>';
        //Datum
        $ausgabe .= '<td>' . $item['insert_date'] . "</td>\n<td><nobr>";
        //Bearbeiten
        if ($item['status'] != 'ARCHIV' && $item['del'] != '1') {
            $ausgabe .= "<a href='?mod=admin&admin=item&action=edit&item=" . $item['id_number'] . "'><img title='Artikel bearbeiten' alt='Artikel bearbeiten' src='" . $stylepath . "images/edit.gif' /></a>\n";
        }
        //In vorbereitet Ausgabe einfügen
        if ($item['status'] == 'NEW' && $item['id_issue'] == 0 && $item['del'] != '1') {
            $ausgabe .= "<a href='?mod=admin&admin=item&action=insert_new&item=" . $item['id_number'] . "'><img title='Artikel zur neuen Ausgabe hinzufügen' alt='Artikel zur neuen Ausgabe hinzufügen' src='" . $stylepath . "images/add.gif' /></a>\n";
        }
        //Aus vorbereiteter Ausgabe entfernen
        if ($item['status'] == 'NEW' && $item['id_issue'] != 0 && $item['del'] != '1') {
            $ausgabe .= "<a href='?mod=admin&admin=item&action=uncase&item=" . $item['id_number'] . "'><img title='Artikel aus neuer Ausgabe entfernen' alt='Artikel aus neuer Ausgabe entfernen' src='" . $stylepath . "images/minus.gif' /></a>\n";
        }
        //Artikel löschen
        if (($item['status'] == 'NEW' || (($wbbuserdata['admin'] == 1 || $wbbuserdata['TP'] == 1 || $wbbuserdata['is_techno'] == 1) && $item['status'] != 'NEW')) && $item['del'] != '1') {
            $ausgabe .= "<a href='?mod=admin&admin=item&action=del&item=" . $item['id_number'] . "'><img title='Artikel l&ouml;schen' alt='Artikel l&ouml;schen' src='" . $stylepath . "images/delete.gif' /></a>\n";
        }
        //Artikel wiederherstellen
        if (($wbbuserdata['admin'] == 1 || $wbbuserdata['TP'] == 1 || $wbbuserdata['is_techno'] == 1) && $item['del'] != '0') {
            $ausgabe .= "<a href='?mod=admin&admin=item&action=undel&item=" . $item['id_number'] . "'><img title='Artikel wiederherstellen' alt='Artikel wiederherstellen' src='" . $stylepath . "images/delete.gif' /></a>\n";
        }
        //Vorschau
        if ($item['del'] != '1') {
            $ausgabe .= "<a href='?mod=item&item=" . $item['id_number'] . "'><img title='Artikelvorschau' alt='Artikelvorschau' src='" . $stylepath . "images/preview.gif' /></a></nobr></td>";
        }
        $ausgabe .= '</tr>';
        $i++;
    }
    $ausgabe .= '</table>';
}
?>
