<?php
/**
 * Shows in the admin area the issue page (Template).
 *
 * @package    HPFC\Tagesprophet\Administration
 * @author    Jana Pape
 * @copyright    2009 Jana Pape
 * All rights reserved.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License (GPL)
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * To read the license please visit http://www.gnu.org/copyleft/gpl.html
 */
declare(strict_types=1);


if ($liste == null) {
    $ausgabe .= 'Keine Ausgabe gefunden';
} else {
    $i = 0;
    if ($wbbuserdata['canpress'] == 1 || $wbbuserdata['is_techno'] == 1 || $wbbuserdata['admin'] == 1) {
        $ausgabe .= "<form method='post' action='?mod=admin&admin=issue&action=unpublish'>\n";
        $ausgabe .= "<input type='submit' name='issue_submit' value='aktuelle Ausgabe zur�ckrufen' />\n";
        $ausgabe .= "</form>\n";
    }
    $ausgabe .= '<table><tr><th>Nummer</th><th>Datum</th><th>Bearbeiten</th></tr>';
    foreach ($liste as $issue) {
        if ($i % 2 != 0) {
            $ausgabe .= "<tr class='ungerade'>";
        } else {
            $ausgabe .= "<tr class='gerade'>";
        }
        //Nummer
        $ausgabe .= "<td><a href='?mod=admin&admin=issue&action=edit&issue=" . $issue['id_number'] . "'>" . $issue['number'] . "</a></td>\n";
        //Ausgabe datum
        $ausgabe .= '<td>' . $issue['publish_date'] . "</td><td>\n";
        //Bearbeiten
        if ($issue['status'] != 'ARCHIV' && $issue['del'] != '1') {
            $ausgabe .= "<a href='?mod=admin&admin=issue&action=edit&issue=" . $issue['id_number'] . "'><img title='Ausgabe bearbeiten' alt='Aausgabe bearbeiten' src='" . $stylepath . "images/edit.gif' /></a>\n";
        }
        //Ausgabe löschen
        if ($issue['status'] == 'NEW' || (($wbbuserdata['admin'] == 1 || $wbbuserdata['canpresse'] == 1 || $wbbuserdata['is_techno'] == 1) && $issue['status'] != 'NEW' && $issue['del'] != '1')) {
            $ausgabe .= "<a href='?mod=admin&admin=issue&action=del&issue=" . $issue['id_number'] . "'><img title='Ausgabe l&ouml;schen' alt='Ausgabe l&ouml;schen' src='" . $stylepath . "images/delete.gif' /></a>\n";
        }
        //Ausgabe löschen
        if ((($wbbuserdata['admin'] == 1 || $wbbuserdata['canpresse'] == 1 || $wbbuserdata['is_techno'] == 1) && $issue['del'] == '1')) {
            $ausgabe .= "<a href='?mod=admin&admin=issue&action=undel&issue=" . $issue['id_number'] . "'><img title='Ausgabe wiederherstellen' alt='Ausgabe wiederherstellen' src='" . $stylepath . "images/delete.gif' /></a>\n";
        }
        //Vorschau
        if ($issue['del'] != '1') {
            $ausgabe .= "<a href='?mod=issue&issue=" . $issue['id_number'] . "'><img title='Ausgabenvorschau' alt='Artikelvorschau' src='" . $stylepath . "images/preview.gif' /></a></td></tr>\n";
        }
        $i++;
    }
    $ausgabe .= '</table>';
}
?>
