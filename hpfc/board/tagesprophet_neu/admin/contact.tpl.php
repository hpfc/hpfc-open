<?php

/**
 * Shows in the admin area the contact page (Template).
 *
 * @package    HPFC\Tagesprophet\Administration
 * @author    Jana Pape
 * @copyright    2009 Jana Pape
 * All rights reserved.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License (GPL)
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * To read the license please visit http://www.gnu.org/copyleft/gpl.html
 */
declare(strict_types=1);

if ($liste == null) {
    $ausgabe .= 'Keine Anzeige gefunden';
} else {
    $i = 0;
    $ausgabe .= "<table width='100%'><tr><th></th><th width='40%'>Titel</th><th>Korrigiert</th><th>Bearbeiten</th></tr>";
    foreach ($liste as $contact) {
        if ($i % 2 != 0) {
            $ausgabe .= "<tr class='ungerade'>";
        } else {
            $ausgabe .= "<tr class='gerade'>";
        }
        //ausgabennummer
        $ausgabe .= '<td><a href=' . $scriptpath . '?issue=' . $contact['id_issue'] . '>' . $contact['issue_number'] . "</a></td>\n";
        //Titel
        $ausgabe .= "<td><a href='?mod=admin&admin=contact&action=edit&contact=" . $contact['id_number'] . "'>" . $contact['title'] . "</a></td>\n";
        //Korgiert
        $ausgabe .= '<td>' . $contact['correct'] . "</td>\n<td><nobr>";
        //Kategorie
        if ($contact['status'] != 'ARCHIV' && $contact['del'] != '1') {
            $ausgabe .= "<a href='?mod=admin&admin=contact&action=edit&contact=" . $contact['id_number'] . "'><img title='Anzeige bearbeiten' alt='Anzeige bearbeiten' src='" . $stylepath . "images/edit.gif' /></a>\n";
        }
        //In vorbereitet Ausgabe einfügen
        if ($contact['status'] == 'NEW' && $contact['id_issue'] == 0 && $contact['del'] != '1') {
            $ausgabe .= "<a href='?mod=admin&admin=contact&action=insert_new&contact=" . $contact['id_number'] . "'><img title='Anzeige zur neuen Ausgabe hinzuf�gen' alt='Anzeige zur neuen Ausgabe hinzuf�gen' src='" . $stylepath . "images/add.gif' /></a>\n";
        }
        //Aus vorbereiteter Ausgabe entfernen
        if ($contact['status'] == 'NEW' && $contact['id_issue'] != 0 && $contact['del'] != '1') {
            $ausgabe .= "<a href='?mod=admin&admin=contact&action=uncase&contact=" . $contact['id_number'] . "'><img title='Anzeige zur neuer Ausgabe entfernen' alt='Anzeige aus neuer Ausgabe entfernen' src='" . $stylepath . "images/minus.gif' /></a>\n";
        }
        //Anzeige löschen
        if (($contact['status'] == 'NEW' || (($wbbuserdata['admin'] == 1 || $wbbuserdata['TP'] == 1 || $wbbuserdata['is_techno'] == 1) && $contact['status'] != 'NEW')) && $contact['del'] != '1') {
            $ausgabe .= "<a href='?mod=admin&admin=contact&action=del&contact=" . $contact['id_number'] . "'><img title='Anzeige l&ouml;schen' alt='Anzeige l&ouml;schen' src='" . $stylepath . "images/delete.gif' /></a>\n";
        }
        //Anzeige wiederherstellen
        if (($wbbuserdata['admin'] == 1 || $wbbuserdata['TP'] == 1 || $wbbuserdata['is_techno'] == 1) && $contact['del'] != '0') {
            $ausgabe .= "<a href='?mod=admin&admin=contact&action=undel&contact=" . $contact['id_number'] . "'><img title='Anzeige wiederherstellen' alt='Anzeige wiederherstellen' src='" . $stylepath . "images/delete.gif' /></a>\n";
        }
        //Vorschau
        if ($contact['del'] != '1') {
            $ausgabe .= "<a href='?contact=" . $contact['id_number'] . "'><img title='Anzeigevorschau' alt='Anzeigevorschau' src='" . $stylepath . "images/preview.gif' /></a></nobr></td></tr>\n";
        }
        $i++;
    }
    $ausgabe .= '</table>';
}
?>
