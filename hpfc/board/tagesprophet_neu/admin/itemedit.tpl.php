<?php

/**
 * Shows in the admin area the item page (Template) .
 *
 * @package    HPFC\Tagesprophet\Administration
 *
 * @author    Jana Pape
 * @copyright    2009 Jana Pape
 * All rights reserved.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License (GPL)
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * To read the license please visit http://www.gnu.org/copyleft/gpl.html
 */
declare(strict_types=1);

use Hpfc\DailyProphetArchive\Category;
use Hpfc\DailyProphetArchive\Item;

if ($item['grammar'] === null) {
    $item['grammar'] = 0;
}
if ($item['spelling'] === null) {
    $item['spelling'] = 0;
}
if ($item['term'] === null) {
    $item['term'] = 0;
}
if ($item['points'] === null) {
    $item['points'] = 0;
}
if ($item['notice'] === null) {
    $item['notice'] = '&nbsp;';
}
$count_user = $_POST['count_user'];
$ausgabe .= $preview;
$ausgabe .= "<form method='post' action='?mod=admin&admin=item&action=edit&item=" . $item['id_number'] . "'>\n";
$ausgabe .= '<label>' . $language['AUTHORDRAWER1'] . "<select size='1' name='count_user'>\n";
for ($j = 1; $j < 10; $j++) {
    $ausgabe .= "<option value='" . $j . "'";
    if ($j === $count_user) {
        $ausgabe .= ' selected';
    }
    $ausgabe .= '>' . $j . "</option>\n";
}
$ausgabe .= "</select></label><input type='submit' value=" . $language['START'] . " /></form>\n";

/**
 * Formular zur Eingabe eines neuen Artikels.
 */

$ausgabe .= "<form method='post' action='?mod=admin&admin=item&action=edit&item=" . $item['id_number'] . "'>\n";
/*
 * Notizen zur Artikelbewertung
 */
$ausgabe .= "<fieldset class='notice'><legend>Notizen - Fehler</legend>\n";
$ausgabe .= "<label for='spelling'>Rechtschreibung <input type='text' name='spelling' size='5' value='" . $item['spelling'] . "' /></label>\n";
$ausgabe .= "<label for='term'>Ausdruck <input type='text' name='term' size='5' value='" . $item['term'] . "' /></label>\n";
$ausgabe .= "<label for='grammar'>Grammatik <input type='text' name='grammar' size='5' value='" . $item['grammar'] . "' /></label>\n";
$ausgabe .= "<label for='points'>Punkte <input type='text' name='points' size='5' value='" . $item['points'] . "' /></label>\n";
$ausgabe .= "<label for='notice'>Notizen <br /><textarea id='notice' name='notice' rows='10' cols='20'>" . $item['notice'] . "</textarea></label>\n";

$ausgabe .= '</fieldset>';
/**
 * Eingabe der Autoren/Zeichner.
 */
$i = 0;
$ausgabe .= "<fieldset class='author'><legend>" . $language['AUTHORDRAWER2'] . "</legend>\n";
do {
    $ausgabe .= "\n<div class='author$i'>";
    $ausgabe .= "<label for='user[" . $i . "][user_id]'>" . $language['ID'] . "<input type='text' name='user[" . $i . "][user_id]' size='10' value='" . $user[$i]['user_id'] . "' /></label>\n";
    $ausgabe .= "<label for='user[" . $i . "]['kind']'>" . $language['KIND'] . "<select name='user[" . $i . "][kind]' size='1'>\n";
    $ausgabe .= "<option value='author'";
    if ($user[$i]['kind'] === 'author') {
        $ausgabe .= ' selected ';
    }
    $ausgabe .= '>' . $language['AUTHOR'] . "</option>\n";
    $ausgabe .= "<option value='drawer'";
    if ($user[$i]['kind'] === 'drawer') {
        $ausgabe .= ' selected ';
    }
    $ausgabe .= '>' . $language['DRAWER'] . '</option></select><br />' . $user[$i]['username'] . '(' . $user[$i]['haus'] . ")</label>\n";
    $upload_drawer = $user[$i]['username'];
    $ausgabe .= "<label for='user[$i][del]'>" . $language['remove'] . "<input type='checkbox' value=TRUE name='user[$i][del]'></label><br />\n";
    $ausgabe .= "</div>\n\n";
    $i++;
} while ($user[$i]['user_id'] || $count_user > ($i));
$ausgabe .= "</fieldset>\n";
/**
 * Artikeldaten inklusive Titel, Kategorie und Text.
 */
$ausgabe .= '<fieldset ><legend>' . $language['ITEM'] . "</legend>\n";
$ausgabe .= '<label for="title">' . $language['TITLE'] . "<input class='item' type='text' name='title' size='30' value='" . $item['title'] . "' /></label>\n";
$ausgabe .= "<label for='category'>" . $language['CATEGORY'] . "<select class='item'  name='category' size='1'>\n";
$category = Category::listData();
foreach ($category as $cat) {
    $ausgabe .= "<option value='" . $cat['id_number'] . "'";
    if ($item['category'] === $cat['id_number']) {
        $ausgabe .= ' selected ';
    }
    $ausgabe .= '>' . $cat['name'] . "</option>\n";
}
$ausgabe .= "</select></label>\n";
$ausgabe .= "<label for='corrector'>" . $language['correct'];
$ausgabe .= "<input  class='item' type='checkbox' name='corrector' value='yes'> " . $item['correct'];
$ausgabe .= "</label><br />\n";

$ausgabe .= "<input type='button' value='" . $language['upload'] . "' onClick='pic_upload();'><br />";

$i = 0;
while ($img[$i] !== null) {
    $ausgabe .= "<img width='100' height= '100' src='" . $config['home_path'] . 'images/items/' . $img[$i] . "' alt='' /> <br />";
    $ausgabe .= $config['home_path'] . 'images/items/' . $img[$i] . '<br />';
    $i++;
}
$ausgabe .= "<label for='my_text'>" . $language['ITEM'] . ':<br />';

// Gets replaced with TinyMCE, remember HTML in a textarea should be encoded -->
$ausgabe .= "<br /><textarea id='mytext' name='mytext' rows='15' cols='80' >\n";
$ausgabe .= $item['text'] . "</textarea></label>\n<br />";
if ($wbbuserdata['TP'] === 1 || $wbbuserdata['is_techno'] === 1 || $wbbuserdata['admin'] === 1) {
    $ausgabe .= '<a href="javascript:void(0);" onmousedown="tinyMCE.get(\'mytext\').show();">[Show]</a>';
    $ausgabe .= '	<a href="javascript:void(0);" onmousedown="tinyMCE.get(\'mytext\').hide();">[Hide]</a>';
}

$ausgabe .= "</fieldset>\n";
$ausgabe .= "<fieldset>\n";

if ($item['id_issue'] !== 0) {
    $corrector = Item::correctStatus($item['id_issue']);
    $ausgabe .= "<label for='sign'>" . $language['sign'];
    $ausgabe .= "<input type='checkbox' name='sign' ";
    if ($wbbuserdata['TP'] !== 1 && $wbbuserdata['is_techno'] !== 1 && $wbbuserdata['admin'] !== 1) {
        $ausgabe .= 'disabled  ';
    }

    if ($item['sign'] === 1) {
        $ausgabe .= "checked='checked' ";
    }
    $ausgabe .= "value='yes'> ";
    $ausgabe .= "</label><br />\n";
}
$ausgabe .= "<input type='hidden' name='id_number' value='" . $item['id_number'] . "' />\n";
$ausgabe .= "<input type='hidden' name='user_id' value='" . $item['red_id'] . "' />\n";
$ausgabe .= "<input type='hidden' name='new' value='" . $item['new'] . "' />\n";
$ausgabe .= "<input type='submit' name='php_submit' value='" . $language['PREVIEW'] . "'>\n";
$ausgabe .= "<input type='submit' name='php_submit' value='" . $language['SAVE'] . "' />\n";

$ausgabe .= "<input type='reset' name='php_reset' value='" . $language['RESET'] . "' /></fieldset>\n";
$ausgabe .= "</form>\n";
if ($wbbuserdata['canpress'] === 1 || $wbbuserdata['is_techno'] === 1 || $wbbuserdata['admin'] === 1) {
    $ausgabe .= "<form method='post' action='?mod=admin&admin=item&action=insert_now&item=" . $item['id_number'] . "'>\n";
    $ausgabe .= "<input type='hidden' name='id_number' value='" . $item['id_number'] . "' />\n";
    $ausgabe .= "<input type='submit' name='issuesubmitt' value='" . $language['INISSUE'] . "' />\n";
    $ausgabe .= '</form>';
}
$ausgabe .= "<form method='post' action='?mod=admin&admin=item&action=insert_new&item=" . $item['id_number'] . "'>\n";
$ausgabe .= "<input type='hidden' name='id_number' value='" . $item['id_number'] . "' />\n";
$ausgabe .= "<input type='submit' name='newubmitt' value='" . $language['INNEW'] . "' />\n";
$ausgabe .= '</form>';
