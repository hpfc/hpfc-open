<?php

/**
 * Shows in the admin area the item page .
 *
 * @package    HPFC\Tagesprophet\Administration
 *
 * @author    Jana Pape
 * @copyright    2009 Jana Pape
 * All rights reserved.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License (GPL)
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * To read the license please visit http://www.gnu.org/copyleft/gpl.html
 */
declare(strict_types=1);

use Hpfc\DailyProphetArchive\Issue;
use Hpfc\DailyProphetArchive\Item;
use Hpfc\DailyProphetArchive\User;

$item = isset($_GET['item']) ? strtoupper((string) $_GET['item']) : '';
$cause2 = [
    'del' => '0',
];
$cause = [];
$order = [];
$preview = null;
$item_active = "class='active'";


switch ($item) {
    /**
     * Alle Artikel auflisten.
     */
    case 'ALL':
        $title .= 'Alle Artikel';
        $file = 'item';
        $all_active = "class='active'";
        break;
        /*
         * Alle  Artikel, die in der neuen Ausgabe erscheinen, auflisten
         */
    case 'NEW':
        $title .= 'Artikel in der vorbereiteten';
        $cause1 = [
            'status' => 'NEW',
            'id_issue' => 'NOT 0',
        ];
        $file = 'item';
        $newissue_active = "class='active'";
        break;
        /*
         * Alle Artikel, die neu sind und in keiner Ausgabe bisher erscheinen, auflisten
         */
    case 'WAIT':
        $title .= 'Artikel in der Warteschlange';
        $cause1 = [
            'status' => 'NEW',
            'id_issue' => '0',
        ];
        $file = 'item';
        $wait_active = "class='active'";
        break;
        /*
         * Alle Artikel, die in der aktuellen Ausgabe sind, auflisten ->Bearbeitung nur durch Chefredakteure
         */
    case 'NOW':
        $title .= 'Artikel in der aktuellen Ausgabe';
        $file = 'item';
        $cause1 = [
            'status' => 'NOW',
        ];
        $nowissue_active = "class='active'";
        break;
        /*
         * Alle archivierten Artikel auflisten, die nur von Chefredakteuren bearbeitet werden können.
         */
    case 'ARCHIV':
        $title .= 'Archivierte Artikel';
        $file = 'item';
        $cause1 = [
            'status' => 'ARCHIV',
        ];
        $archiv_active = "class='active'";
        break;
        /*
         * Alle gelöschten Artikel auflisten, die nur von Chefredakteuren bearbeitet werden können.
         */
    case 'DEL':
        if ($wbbuserdata['admin'] === 1 || $wbbuserdata['TP'] === 1 || $wbbuserdata['is_techno'] === 1) {
            $title .= 'Gel&ouml;schte Artikel';
            $file = 'item';
            $cause2 = [
                'del' => '1',
            ];
            $del_active = "class='active'";
        } else {
            $title .= 'Alle Artikel';
            $file = 'item';
            $all_active = "class='active'";
        }
        break;
    default:
        $action = isset($_GET['action']) ? strtolower((string) $_GET['action']) : '';
        switch ($action) {
            //einen Artikel in die aktuelle Ausgabe einfügen
            case 'insert_now':
                $item_id = $_POST['id_number'] ?? $_GET['item'];
                Item::insertIssue($item_id, 'now');
                $file = 'index';
                $temp = '<p>Der Artikel wurde in der aktuellen Ausgabe veröffentlicht.</p>';
                break;
                //einen Artikel in die neue Ausgabe einfügen
            case 'insert_new':
                $item_id = $_POST['id_number'] ?? $_GET['item'];
                $issue = Issue::getValue('status', 'NEW', 'id_number');
                if ($issue !== 0) {
                    Item::insertIssue($item_id);
                    $file = 'index';
                    $temp = '<p>Der Artikel wurde in die neue Ausgabe verschoben.</p>';
                } else {
                    $file = 'index';
                    $temp = '<p>Der Artikel konnte nicht verschoben werden, da noch keine neue Ausgabe erzeugt wurde.</p>';
                    $temp .= "<p><a href='?mod=admin&admin=issue&action=edit&issue='>Neue Ausgabe erstellen?</a></p>";
                }
                break;
            case 'edit':
                //Artikel bearbeiten
                $dir = $config['dir_path'];

                $dh = is_dir($dir) ? opendir($dir) : false;
                if ($dh) {
                    $img = [];
                    $i = 0;
                    while (($file = readdir($dh)) !== false) {
                        $type = explode('_', $file);
                        if ($type[0] === $_GET['item']) {
                            $img[$i] = $file;
                            $i++;
                        }
                    }
                    closedir($dh);
                }
                $file = 'itemedit';
                /*
                 * Wurden Artikeldaten per POST übergeben?
                 */

                if (isset($_POST['php_submit'])) {
                    $objItem = Item::getInstance((int) $_GET['item']);
                    $objUser = User::getInstance($_GET['item']);
                    $correct = null;
                    /*
                     * Wurde der Artikel korrigiert? Wenn ja, wird der Benutzername gespeichert.
                     */
                    if ($_POST['corrector'] === 'yes') {
                        echo $correct = $wbbuserdata['username'];
                    }
                    /*
                     * Überprüfen, ob schon Korrektur für Ausgabe gemacht wurde, wenn ja welche
                     */

                    $array = [
                        '\"' => '"',
                    ]; //,"/images/items/"=>"/hp-fc/tagesprophet/images/items/");
                    /*
                     * Text bereinigen und Artikeldaten in eine Array speichern
                     */
                    $text = strtr($_POST['mytext'], $array);
                    $temp_item = [
                        'new' => $_POST['new'],
                        'title' => $_POST['title'],
                        'summary' => $_POST['summary'],
                        'text' => $text,
                        'category' => $_POST['category'],
                        'id_number' => $_POST['id_number'],
                        'correct' => $correct,
                        'spelling' => $_POST['spelling'],
                        'term' => $_POST['term'],
                        'grammar' => $_POST['grammar'],
                        'notice' => $_POST['notice'],
                        'points' => $_POST['points'],
                    ];
                    if ($_POST['sign'] === 'yes') {
                        $temp_item['sign'] = 1;
                    }
                    /**
                     * Autoren in ein mehrdimensionales Array speichern.
                     */
                    $i = 0;
                    foreach ($_POST['user'] as $user) {
                        if ($user['user_id'] !== null && $user['user_id'] !== 0 && !$user['del']) {
                            $temp_user[$i]['kind'] = $user['kind'];
                            $temp_user[$i]['user_id'] = $user['user_id'];
                            $temp_user[$i]['id_item'] = $_GET['item'];
                            $i++;
                        }
                    }
                    /**
                     * Artikel- und Autorendaten überprüfen und zurückgeben.
                     */

                    $objItem->checkData($temp_item);
                    $item = $objItem->getData();

                    $objUser->checkData($temp_user);
                    $user = $objUser->getData();

                    /*
                     * Überprüfen, ob die Userid aufgelöst werden konnte
                     * Wenn ja, dann kann eine Vorschau oder Speichern erfolgen.
                     */
                    $usererror = false;

                    foreach ($user as $user_foreach) {
                        if (isset($user_foreach['error'])) {
                            $usererror = true;
                            $preview = '<p class="warning">Es wurde kein Autor angegeben<br /> Es ist keine Vorschau/kein Speichern möglich.</p>';
                        }
                    }
                    $usererror = $objUser->getError();
                    if (isset($usererror['noautor'])) {
                        $usererror = true;
                        $preview = '<p class="warning">Es wurde kein Autor angegeben<br /> Es ist keine Vorschau/kein Speichern möglich.</p>';
                    }
                    switch ($_POST['php_submit']) {
                        case $language['PREVIEW']:
                            // Vorschau
                            if (!$usererror) {
                                include_once($adminpath . 'preview.tpl.php');
                            }
                            break;
                        case $language['SAVE']:
                            // Speichern
                            if (!$usererror) {
                                $item_id = $objItem->saveData();
                                $objUser->saveData($item_id);
                                $temp = '<p>Der Artikel ' . $item['title'] . ' wurde erfolgreich gespeichert.</p>';
                                if ($item['id_number'] !== null && ($wbbuserdata['canpress'] === 1 || $wbbuserdata['is_techno'] === 1 || $wbbuserdata['admin'] === 1)) {
                                    $temp .= "<form method='post' action='?mod=admin&admin=item&action=insert_now&item=" . $item['id_number'] . "'>\n";
                                    $temp .= "<input type='hidden' name='id_number' value='" . $item['id_number'] . "' />\n";
                                    $temp .= "<input type='submit' name='issuesubmitt' value='" . $language['INISSUE'] . "' />\n";
                                    $temp .= '</form>';
                                }
                                if ($item['id_number'] !== null) {
                                    $temp .= "<form method='post' action='?mod=admin&admin=item&action=insert_new&item=" . $item['id_number'] . "'>\n";
                                    $temp .= "<input type='hidden' name='id_number' value='" . $item['id_number'] . "' />\n";
                                    $temp .= "<input type='submit' name='newubmitt' value='" . $language['INNEW'] . "' />\n";
                                    $temp .= '</form>';
                                }
                                $temp .= "<a href='?mod=admin&admin=item'>Zur&uuml;ck zu den Artikeln</a>";
                                $file = 'index';
                            }
                            break;
                        case $language['RESET']:
                            // Zurücksetzen
                            $item = [];
                            $user = [];
                            break;
                    }
                } else {
                    //Artikel aus der Datenbank auslesen und Daten ausgeben
                    $objUser = User::getInstance($_GET['item']);
                    $objItem = Item::getInstance((int) $_GET['item']);
                    $item = $objItem->getData();
                    $user = $objUser->getData();
                }
                break;
                //Artikel löschen
            case 'del':
                if ($wbbuserdata['admin'] === 1 || $wbbuserdata['canpresse'] === 1 || $wbbuserdata['tp'] === 1 || $wbbuserdata['is_techno'] === 1) {
                    $file = 'index';
                    $objUser = User::getInstance($_GET['item']);
                    $objItem = Item::getInstance((int) $_GET['item']);
                    $item = $objItem->getValue('id_number', $_GET['item'], 'title');
                    switch ($_GET['del']) {
                        case 'yes':
                            $objItem->delData();
                            $objUser->delData();
                            $temp = '<p> Der Artikel ' . $item . ' wurde erfolgreich gel&ouml;scht.</p>';
                            break;
                        case 'no':
                            $temp = '<p>Der Artikel ' . $item . ' wurde nicht gelöscht.</p>';
                            break;
                        default:
                            $temp = '<p>Soll der Artikel ' . $item . ' wirklich gelöscht werden?</p>';
                            $temp .= "<form method='post' action='?mod=admin&admin=item&action=del&item=" . $_GET['item'] . "&del=yes'>\n<input type='submit' name='issue_submit' value='Artikel L&ouml;schen' />\n</form>";
                            $temp .= "<form method='post' action='?mod=admin&admin=item&action=del&item=" . $_GET['item'] . "&del=no'>\n<input type='submit' name='issue_submit' value='Nicht L&ouml;schen' />\n</form>";
                            break;
                    }
                } else {
                    $title .= 'Alle Artikel';
                    $file = 'item';
                    $all_active = "class='active'";
                }
                break;
                //Artikel aus Ausgabe entfernen
            case 'uncase':
                $file = 'index';
                Item::uncaseIssue($_GET['item']);
                $temp = '<p> Der Artikel ' . $item . ' wurde zurück in die Warteschleife gestellt.</p>';
                break;
            case 'undel':
                $file = 'index';
                $objItem = Item::getInstance((int) $_GET['item']);
                $item = $objItem->getData();
                $objItem->undelete($_GET['item']);
                $temp = '<p> Der Artikel <i>' . $item['title'] . '</i> wurde erfolgreich wiederhergestellt.</p>';
                break;
            default:
                $title .= 'Alle Artikel';
                $file = 'item';
                $all_active = "class='active'";
                break;
        }
        break;
}
//Abfrage einschränken
$cause = $cause1 !== null ? array_merge($cause1, $cause2) : $cause2;
//Auflistung der Artikel
if ($file === 'item') {
    $liste = Item::getList($cause, $order, 'DESC');
}
$item_nav = "<ul id='item_nav'>";
$item_nav .= "<li><a href='?mod=admin&admin=item&action=edit&item='>" . $language['NEWITEM'] . "</a></li>\n";
$item_nav .= '<li  ' . $wait_active . '><a ' . $wait_active . " href='?mod=admin&admin=item&item=wait'>Warteschleife</a></li>\n";
$item_nav .= '<li ' . $all_active . '><a ' . $all_active . " href='?mod=admin&admin=item&item=all'>Alle Artikel</a></li>\n";
$item_nav .= '<li  ' . $archiv_active . '><a ' . $archiv_active . " href='?mod=admin&admin=item&item=archiv'>Archiv</a></li>\n";
if ($wbbuserdata['admin'] === 1 || $wbbuserdata['TP'] === 1 || $wbbuserdata['is_techno'] === 1) {
    $item_nav .= '<li  ' . $del_active . '><a  ' . $del_active . " href='?mod=admin&admin=item&item=del'>gel&ouml;scht</a></li>";
}
$item_nav .= '</ul>';

if (isset($item['id_number']) && $item['id_number'] === 0) {
    $item['new'] = 'new';
    $itemfile = $adminpath . 'item.txt';
    $fp = fopen($itemfile, 'r+');
    if (!file_exists($itemfile)) {
        die('Konnte die Datei nicht finden!');
    }
    if (!is_resource($fp)) {
        die('Konnte die Datei nicht öffnen!');
    }
    if (!flock($fp, LOCK_EX)) {
        die('Sperren der Datei fehlgeschlagen!');
    }
    $newItemNumber = (int) fread($fp, filesize($itemfile));
    $newItemNumber++;
    fseek($fp, 0, SEEK_SET);
    ftruncate($fp, 0);
    fwrite($fp, (string) $newItemNumber);
    fclose($fp);
    $item['id_number'] = $newItemNumber;
}
