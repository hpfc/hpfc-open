<?php

/**
 * Shows the admin page.
 *
 * @package    HPFC\Tagesprophet\Administration
 *
 * @author    Jana Pape
 * @copyright    2009 Jana Pape
 * All rights reserved.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License (GPL)
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * To read the license please visit http://www.gnu.org/copyleft/gpl.html
 */
declare(strict_types=1);

use Hpfc\DailyProphetArchive\Config;
use Hpfc\DailyProphetArchive\Contact;
use Hpfc\DailyProphetArchive\IssueAdmin;
use Hpfc\DailyProphetArchive\Item;
use Hpfc\DailyProphetArchive\Readers;

if ($wbbuserdata['admin'] !== 1 && $wbbuserdata['TP'] !== 1 && $wbbuserdata['is_techno'] !== 1) {
    access_error();
} else {
    if (isset($_GET['delete'])) {
        IssueAdmin::delete();
        Item::delete();
        Contact::delete();
        Readers::delete();
        $text = '<p>Der Papierkorb wurde geleert.</p>';
    }
    $config_temp['intervall'] = $_POST['intervall'];
    $config_temp['name'] = $_POST['name'];
    $config_temp['contact_email'] = $_POST['contact_email'];
    $config_temp['readers_email'] = $_POST['readers_email'];
    $config_temp['summary'] = isset($_POST['summary']) ? htmlentities((string) $_POST['summary'], ENT_QUOTES) : '';
    $config_temp['readers_notice'] = isset($_POST['readers_notice']) ? htmlentities((string) $_POST['readers_notice'], ENT_QUOTES) : '';
    $config_temp['contact_notice'] = isset($_POST['contact_notice']) ? htmlentities((string) $_POST['contact_notice'], ENT_QUOTES) : '';
    $config_temp['home_path'] = $_POST['home_path'];
    $config_temp['dir_path'] = $_POST['dir_path'];
    if ($_POST['php_submit'] === $language['SAVE']) {
        Config::saveConfig($config_temp);
        $print .= 'Die Konfiguration wurde erfolgreich gespeichert.';
        $file = 'index';
    }
}
