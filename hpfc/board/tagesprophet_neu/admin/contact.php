<?php

/**
 * Shows in the admin area the contact page.
 *
 * @package    HPFC\Tagesprophet\Administration
 *
 * @author    Jana Pape
 * @copyright    2009 Jana Pape
 * All rights reserved.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License (GPL)
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * To read the license please visit http://www.gnu.org/copyleft/gpl.html
 */
declare(strict_types=1);

use Hpfc\DailyProphetArchive\Contact;
use Hpfc\DailyProphetArchive\Issue;

$contact = isset($_GET['contact']) ? strtoupper((string) $_GET['contact']) : '';
$cause2 = [
    'del' => '0',
];
$cause = [];
$order = [];
$preview = null;
$file = 'contact';
$contact_active = "class='active'";

switch ($contact) {
    /**
     * Alle Artikel auflisten.
     */
    case 'ALL':
        $title .= 'Alle Anzeigen';
        $all_active = "class='active'";
        break;
        /*
         * Alle  Anzeigen, die in der neuen Ausgabe erscheinen, auflisten
         */
    case 'NEW':
        $title .= 'Anzeigen in der vorbereiteten';
        $cause1 = [
            'status' => 'NEW',
            'id_issue' => 'NOT 0',
        ];
        $newissue_active = "class='active'";
        break;
        /*
         * Alle Anzeigen, die neu sind und in keiner Ausgabe bisher erscheinen, auflisten
         */
    case 'WAIT':
        $title .= 'Anzeigen in der Warteschlange';
        $cause1 = [
            'status' => 'NEW',
            'id_issue' => '0',
        ];
        $wait_active = "class='active'";
        break;
        /*
         * Alle Anzeigen, die in der aktuellen Ausgabe sind, auflisten ->Bearbeitung nur durch Chefredakteure
         */
    case 'NOW':
        $title .= 'Anzeigen in der aktuellen Ausgabe';
        $cause1 = [
            'status' => 'NOW',
        ];
        $nowissue_active = "class='active'";
        break;
        /*
         * Alle archivierten Anzeigen auflisten, die nur von Chefredakteuren bearbeitet werden können.
         */
    case 'ARCHIV':
        $title .= 'Archivierte Anzeigen';
        $file = 'contact';
        $cause1 = [
            'status' => 'ARCHIV',
        ];
        $archiv_active = "class='active'";
        break;
        /*
         * Alle gel�schten Anzeigen auflisten, die nur von Chefredakteuren bearbeitet werden können.
         */
    case 'DEL':
        if ($wbbuserdata['admin'] === 1 || $wbbuserdata['TP'] === 1 || $wbbuserdata['is_techno'] === 1) {
            $title .= 'Gel&ouml;schte Anzeigen';
            $file = 'contact';
            $cause2 = [
                'del' => '1',
            ];
            $del_active = "class='active'";
        } else {
            $title .= 'Alle Anzeigen';
            $file = 'contact';
            $all_active = "class='active'";
        }
        break;
    default:
        $action = isset($_GET['action']) ? strtolower((string) $_GET['action']) : '';
        switch ($action) {
            //einen Anzeigen in die aktuelle Ausgabe einfügen
            case 'insert_now':
                Contact::insertIssue($_POST['id_number'], 'now');
                $file = 'index';
                $temp = '<p>Die Anzeige wurde in der aktuellen Ausgabe veröffentlicht.</p>';
                break;
                //einen Anzeigen in die neue Ausgabe einfügen
            case 'insert_new':
                $issue = Issue::getIssue('NEW');
                $contact_id = $_POST['id_number'] ?? $_GET['contact'];
                if ($issue['id_number'] !== 0) {
                    Contact::insertIssue($contact_id);
                    $file = 'index';
                    $temp = '<p>Die Anzeige wurde in die neue Ausgabe verschoben.</p>';
                } else {
                    $file = 'index';
                    $temp = '<p>Die Anzeige wurde in nicht die neue Ausgabe verschoben.</p>';
                }
                break;
                //Anzeigen bearbeiten
            case 'edit':
                $file = 'contactedit';
                /*
                 * Wurden Anzeigendaten per POST übergeben?
                 */
                if (isset($_POST['php_submit'])) {
                    $objcontact = Contact::getInstance($_GET['contact']);
                    $correct = null;
                    /*
                     * Wurde die Anzeige korrigiert? Wenn ja, wird der Benutzername gespeichert.
                     */
                    if ($_POST['correct'] === 'yes') {
                        $correct = $wbbuserdata['username'];
                    }
                    /*
                     * Text bereinigen und Anzeigendaten in eine Array speichern
                     */
                    $temp_contact = [
                        'username' => $_POST['username'],
                        'title' => $_POST['title'],
                        'text' => $_POST['mytext'],
                        'id_number' => $_POST['id_number'],
                        'correct' => $correct,
                    ];
                    /**
                     * Leserbriefdaten überprüfen und zur�ckgeben.
                     */
                    $objcontact->checkData($temp_contact);
                    $contact = $objcontact->getData();

                    /*
                     * überprüfen, ob die Userid Aufgel�st werden konnte
                     * Wenn ja, dann kann eine Vorschau oder Speichern erfolgen.
                     */
                    switch ($_POST['php_submit']) {
                        case $language['PREVIEW']:
                            // Vorschau
                            $author = $contact['username'];
                            $text = $contact['text'];
                            include_once($adminpath . 'preview.tpl.php');
                            break;
                        case $language['SAVE']:
                            // Speichern
                            $contact_id = $objcontact->saveData();
                            if ($error === true) {
                                $temp = '<p>Die Anzeige ' . $contact['title'] . ' wurde nicht gespeichert.</p>';
                            } else {
                                $temp = '<p>Die Anzeige ' . $contact['title'] . ' wurde erfolgreich gespeichert.</p>';
                                if ($contact['id_number'] !== null && ($wbbuserdata['canpress'] === 1 || $wbbuserdata['is_techno'] === 1 || $wbbuserdata['admin'] === 1)) {
                                    $temp .= "<form method='post' action='?mod=admin&admin=contact&action=insert_now&contact=" . $contact['id_number'] . "'>\n";
                                    $temp .= "<input type='hidden' name='id_number' value='" . $contact['id_number'] . "' />\n";
                                    $temp .= "<input type='submit' name='issuesubmitt' value='" . $language['INISSUE'] . "' />\n";
                                    $temp .= '</form>';
                                }
                                if ($contact['id_number'] !== null) {
                                    $temp .= "<form method='post' action='?mod=admin&admin=contact&action=insert_new&contact=" . $contact['id_number'] . "'>\n";
                                    $temp .= "<input type='hidden' name='id_number' value='" . $contact['id_number'] . "' />\n";
                                    $temp .= "<input type='submit' name='newubmitt' value='" . $language['INNEW'] . "' />\n";
                                    $temp .= '</form>';
                                }
                            }
                            $file = 'index';
                            break;
                        case $language['RESET']:
                            // Zurücksetzen
                            $contact = [];
                            break;
                    }
                } else {
                    $objcontact = Contact::getInstance($_GET['contact']);
                    $contact = $objcontact->getData();
                }
                break;
                //Anzeigen löschen
            case 'del':
                if ($wbbuserdata['admin'] === 1 || $wbbuserdata['canpresse'] === 1 || $wbbuserdata['is_techno'] === 1) {
                    $file = 'index';
                    $objContact = Contact::getInstance($_GET['contact']);
                    $contact = $objContact->getValue('id_number', $_GET['contact'], 'title');
                    switch ($_GET['del']) {
                        case 'yes':
                            $objContact->delData();
                            $temp = '<p>Die Anzeige ' . $contact . ' wurde erfolgreich gel&ouml;scht.</p>';
                            break;
                        case 'no':
                            $temp = '<p>Die Anzeige ' . $contact . ' wurde nicht gel&ouml;scht.</p>';
                            break;
                        default:
                            $temp = '<p>Soll die Anzeige ' . $contact . ' wirklich gelöscht werden?</p>';
                            $temp .= "<form method='post' action='?mod=admin&admin=contact&action=del&contact=" . $_GET['contact'] . "&del=yes'>\n<input type='submit' name='issue_submit' value='Anzeige L&ouml;schen' />\n</form>";
                            $temp .= "<form method='post' action='?mod=admin&admin=contact&action=del&contact=" . $_GET['contact'] . "&del=no'>\n<input type='submit' name='issue_submit' value='Nicht L&ouml;schen' />\n</form>";
                            break;
                    }
                } else {
                    $title .= 'Alle Anzeigen';
                    $file = 'contact';
                    $all_active = "class='active'";
                }
                break;
                //Anzeigen aus Ausgabe entfernen
            case 'uncase':
                $file = 'index';
                Contact::uncaseIssue($_GET['contact']);
                $temp = '<p> Der Anzeigen ' . $contact . ' wurde zur�ck in die Warteschleife gestellt.</p>';
                break;
            case 'undel':
                $file = 'index';
                $objContact = Contact::getInstance($_GET['contact']);
                $contact = $objContact->getData();
                $objContact->undelete($_GET['contact']);
                $temp = '<p> Der Leserbriefe <i>' . $contact['title'] . '</i> wurde zurück in die Warteschleife gestellt.</p>';
                break;
            default:
                $title .= 'Alle Anzeigen';
                $file = 'contact';
                $all_active = "class='active'";
                break;
        }
}
//Abfrage einschr�nken
$cause = $cause1 !== null ? array_merge($cause1, $cause2) : $cause2;
//Auflistung der Anzeigen
if ($file === 'contact') {
    $liste = Contact::getList($cause, $order);
}
$contact_nav = "<ul id='contact_nav'><li " . $all_active . '>';
$contact_nav .= '<a ' . $all_active . " href='?mod=admin&admin=contact&contact=all'>Alle Anzeigen</a></li>\n";
$contact_nav .= '<li  ' . $newissue_active . '><a ' . $newissue_active . " href='?mod=admin&admin=contact&contact=new'>Ausgabe in Vorbereitung</a></li>\n";
$contact_nav .= '<li  ' . $wait_active . '><a ' . $wait_active . " href='?mod=admin&admin=contact&contact=wait'>Warteschleife</a></li>\n";
$contact_nav .= '<li  ' . $nowissue_active . '><a ' . $nowissue_active . " href='?mod=admin&admin=contact&contact=now'>aktuelle Ausgabe</a></li>\n";
$contact_nav .= '<li  ' . $archiv_active . '><a ' . $archiv_active . " href='?mod=admin&admin=contact&contact=archiv'>archivierte Anzeigen</a></li>\n";
if ($wbbuserdata['admin'] === 1 || $wbbuserdata['canpresse'] === 1 || $wbbuserdata['is_techno'] === 1) {
    $contact_nav .= '<li  ' . $del_active . '><a  ' . $del_active . " href='?mod=admin&admin=contact&contact=del'>gel&ouml;schte Anzeigen</a></li>";
}
$contact_nav .= '</ul>';
