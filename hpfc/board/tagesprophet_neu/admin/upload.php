<?php

/**
 * Shows in the admin area the imageupload(Template).
 *
 * @package    HPFC\Tagesprophet\Administration
 *
 * @author    Jana Pape
 * @copyright    2009 Jana Pape
 * All rights reserved.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License (GPL)
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * To read the license please visit http://www.gnu.org/copyleft/gpl.html
 */
declare(strict_types=1);

use Hpfc\DailyProphetArchive\Config;

$config = Config::loadData();
$home_url = $config['home_path'];
$imagepath = "{$scriptpath}images/";
$_GET['item'] = (int) $_GET['item'];
$ausgabe = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">';
$ausgabe .= '<html xmlns="http://www.w3.org/1999/xhtml"><head>';
$ausgabe .= "<link rel='stylesheet' type='text/css' href='{$adminpath}style.css'>";
$ausgabe .= '<title>Bilder hochladen</title></head>';
$ausgabe .= "<body>\n<div id='main'>";
$ausgabe .= "<form enctype='multipart/form-data' action='?mod=admin&admin=upload&item=$_GET[item]' method='post'>";
$ausgabe .= "<input type='hidden' name='MAX_FILE_SIZE' value='300000'>";
$ausgabe .= "Bild: <input name='userfile' type='file'>";
$ausgabe .= "<input type='submit' value='Upload'>";
$ausgabe .= '</form>';

$item['id_number'] = $_GET['item'];
$uploaddir = $imagepath . 'items/';

if ($_FILES['userfile']['name'] !== null) {
    $type = explode('/', (string) $_FILES['userfile']['type']);
    $filename = explode('.', (string) $_FILES['userfile']['name']);
    $filename = array_reverse($filename);
    $type[1] = $filename[0];

    if ($type[0] === 'image') {
        $i = 0;
        $destination = $uploaddir . $item['id_number'] . '_' . $i . '.' . $type[1];

        while (file_exists($destination)) {
            $i++;
            $destination = $uploaddir . $_GET['item'] . '_' . $i . '.' . $type[1];
        }
        print '<pre>';
        if (move_uploaded_file($_FILES['userfile']['tmp_name'], $destination)) {
            $ausgabe .= "Das Bild wurde hochgeladen:\n";
            chmod($destination, 0766);
            $link = substr($destination, 3);
            $ausgabe .= $destination;
        } else {
            print "Possible file upload attack!  Here's some debugging info:\n";
            print_r($_FILES);
        }
    } else {
        switch ($_FILES['userfile']['error']) {
            case 1:
            case 2:
                $ausgabe .= ('Die Datei ist zu groß.');
                break;
            case 3:
                $ausgabe .= ('Die Datei wurde nur teilweise hochgeladen.');
                break;
            case 4:
                $ausgabe .= ('Es wurde keine Datei hochgeladen.');
                break;
        }
        $ausgabe .= '<br />Bitte eine Bilddatei hochladen.';
    }
}
$ausgabe .= '[ <a rel="nofollow" href="#" onclick="window.close(); return false;">Fenster schließen</a> ]';
$ausgabe .= '</div></body></html>';
echo $ausgabe;
