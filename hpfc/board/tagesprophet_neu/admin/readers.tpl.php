<?php

/**
 * Shows in the admin area the readers(Template).
 *
 * @package    HPFC\Tagesprophet\Administration
 * @author    Jana Pape
 * @copyright    2009 Jana Pape
 * All rights reserved.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License (GPL)
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * To read the license please visit http://www.gnu.org/copyleft/gpl.html
 */
declare(strict_types=1);

if ($liste == null) {
    $ausgabe .= 'Keine Leserbrief gefunden';
} else {
    $i = 0;
    $ausgabe .= "<table width='100%'><tr><th></th><th width='40%'>Titel</th><th>Artikel</th><th>Korrigiert</th><th>Bearbeiten</th></tr>";
    foreach ($liste as $readers) {
        if ($i % 2 != 0) {
            $ausgabe .= "<tr class='ungerade'>";
        } else {
            $ausgabe .= "<tr class='gerade'>";
        }
        //ausgabennummer
        $ausgabe .= '<td><a href=' . $scriptpath . '?issue=' . $readers['id_issue'] . '>' . $readers['issue_number'] . "</a></td>\n";
        //Titel
        $ausgabe .= '<td>' . $readers['title'] . "</td>\n";
        //Artikel
        $ausgabe .= "<td><a href='" . $scriptpath . '?item=' . $readers['item_id'] . "'>" . $readers['item'] . "</a></td>\n";
        //Korgiert
        $ausgabe .= '<td>' . $readers['correct'] . "</td>\n<td><nobr>";
        //Kategorie
        if ($readers['status'] != 'ARCHIV' && $readers['del'] != '1') {
            $ausgabe .= "<a href='?mod=admin&admin=readers&action=edit&readers=" . $readers['id_number'] . "'><img title='Leserbrief bearbeiten' alt='Leserbrief bearbeiten' src='" . $stylepath . "images/edit.gif' /></a>\n";
        }
        //In vorbereitet Ausgabe einfügen
        if ($readers['status'] == 'NEW' && $readers['id_issue'] == 0 && $readers['del'] != '1') {
            $ausgabe .= "<a href='?mod=admin&admin=readers&action=insert_new&readers=" . $readers['id_number'] . "'><img title='Leserbrief zur neuen Ausgabe hinzuf�gen' alt='Leserbrief zur neuen Ausgabe hinzuf�gen' src='" . $stylepath . "images/add.gif' /></a>\n";
        }
        //Aus vorbereiteter Ausgabe entfernen
        if ($readers['status'] == 'NEW' && $readers['id_issue'] != 0 && $readers['del'] != '1') {
            $ausgabe .= "<a href='" . $adminpath . '?mod=admin&admin=readers&action=uncase&readers=' . $readers['id_number'] . "'><img title='Leserbrief zur neuer Ausgabe entfernen' alt='Leserbrief aus neuer Ausgabe entfernen' src='" . $stylepath . "images/minus.gif' /></a>\n";
        }
        //Leserbrief löschen
        if (($readers['status'] == 'NEW' || (($wbbuserdata['admin'] == 1 || $wbbuserdata['TP'] == 1 || $wbbuserdata['is_techno'] == 1) && $readers['status'] != 'NEW')) && $readers['del'] != '1') {
            $ausgabe .= "<a href='?mod=admin&admin=readers&action=del&readers=" . $readers['id_number'] . "'><img title='Leserbrief l&ouml;schen' alt='Leserbrief l&ouml;schen' src='" . $stylepath . "images/delete.gif' /></a>\n";
        }
        //Leserbrief wiederherstellen
        if (($wbbuserdata['admin'] == 1 || $wbbuserdata['TP'] == 1 || $wbbuserdata['is_techno'] == 1) && $readers['del'] != '0') {
            $ausgabe .= "<a href='?mod=admin&admin=readers&action=undel&readers=" . $readers['id_number'] . "'><img title='Leserbrief l&ouml;schen' alt='Leserbrief l&ouml;schen' src='" . $stylepath . "images/delete.gif' /></a>\n";
        }
        //Vorschau
        if ($readers['del'] != '1') {
            $ausgabe .= "<a href='?readers=" . $readers['id_number'] . "'><img title='Leserbriefvorschau' alt='Leserbriefvorschau' src='" . $stylepath . "images/preview.gif' /></a></nobr></td></tr>\n";
        }
        $i++;
    }
    $ausgabe .= '</table>';
}
?>
