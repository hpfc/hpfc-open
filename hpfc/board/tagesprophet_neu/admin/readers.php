<?php

/**
 * Shows in the admin area the readers page.
 *
 * @package    HPFC\Tagesprophet\Administration
 *
 * @author    Jana Pape
 * @copyright    2009 Jana Pape
 * All rights reserved.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License (GPL)
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * To read the license please visit http://www.gnu.org/copyleft/gpl.html
 */
declare(strict_types=1);

use Hpfc\DailyProphetArchive\Issue;
use Hpfc\DailyProphetArchive\Readers;

$readers = strtoupper((string) $_GET['readers']);
$cause2 = [
    'del' => '0',
];
$cause = [];
$order = [];
$preview = null;
$file = 'readers';
$readers_active = "class='active'";

switch ($readers) {
    /**
     * Alle Artikel auflisten.
     */
    case 'ALL':
        $title .= 'Alle Leserbriefe';
        $all_active = "class='active'";
        break;
        /*
         * Alle  Leserbriefe, die in der neuen Ausgabe erscheinen, auflisten
         */
    case 'NEW':
        $title .= 'Leserbriefe in der vorbereiteten';
        $cause1 = [
            'status' => 'NEW',
            'id_issue' => 'NOT 0',
        ];
        $newissue_active = "class='active'";
        break;
        /*
         * Alle Leserbriefe, die neu sind und in keiner Ausgabe bisher erscheinen, auflisten
         */
    case 'WAIT':
        $title .= 'Leserbriefe in der Warteschlange';
        $cause1 = [
            'status' => 'NEW',
            'id_issue' => '0',
        ];
        $wait_active = "class='active'";
        break;
        /*
         * Alle Leserbriefe, die in der aktuellen Ausgabe sind, auflisten ->Bearbeitung nur durch Chefredakteure
         */
    case 'NOW':
        $title .= 'Leserbriefe in der aktuellen Ausgabe';
        $cause1 = [
            'status' => 'NOW',
        ];
        $nowissue_active = "class='active'";
        break;
        /*
         * Alle archivierten Leserbriefe auflisten, die nur von Chefredakteuren bearbeitet werden können.
         */
    case 'ARCHIV':
        $title .= 'Archivierte Leserbriefe';
        $cause1 = [
            'status' => 'ARCHIV',
        ];
        $archiv_active = "class='active'";
        break;
        /*
         * Alle gel�schten Leserbriefe auflisten, die nur von Chefredakteuren bearbeitet werden können.
         */
    case 'DEL':
        if ($wbbuserdata['admin'] === 1 || $wbbuserdata['TP'] === 1 || $wbbuserdata['is_techno'] === 1) {
            $title .= 'Gel&ouml;schte Leserbriefe';
            $file = 'readers';
            $cause2 = [
                'del' => '1',
            ];
            $del_active = "class='active'";
        } else {
            $title .= 'Alle Leserbriefe';
            $file = 'readers';
            $all_active = "class='active'";
        }
        break;
    default:
        $action = strtolower((string) $_GET['action']);
        switch ($action) {
            //einen Leserbriefe in die aktuelle Ausgabe einfügen
            case 'insert_now':
                Readers::insertIssue($_GET['readers'], 'now');
                $file = 'index';
                $temp = '<p>Der Leserbriefe wurde in der aktuellen Ausgabe ver�ffentlicht.</p>';
                break;
                //einen Leserbriefe in die neue Ausgabe einfügen
            case 'insert_new':
                $issue = Issue::getIssue('NEW');
                $readers_id = $_POST['id_number'] ?? $_GET['readers'];
                if ($issue['id_number'] !== 0) {
                    Readers::insertIssue($readers_id);
                    $file = 'index';
                    $temp = '<p>Der Leserbriefe wurde in die neue Ausgabe verschoben.</p>';
                } else {
                    $file = 'index';
                    $temp = '<p>Der Leserbriefe wurde in nicht die neue Ausgabe verschoben.</p>';
                }
                break;
                //Leserbriefe bearbeiten
            case 'edit':
                $file = 'readersedit';
                /*
                 * Wurden Leserbriefedaten per POST übergeben?
                 */
                if (isset($_POST['php_submit'])) {
                    $objreaders = Readers::getInstance($_GET['readers']);
                    $correct = null;
                    /*
                     * Wurde der Leserbriefe korrigiert? Wenn ja, wird der Benutzername gespeichert.
                     */
                    if ($_POST['correct'] === 'yes') {
                        $correct = $wbbuserdata['username'];
                    }
                    /*
                     * Text bereinigen und Leserbriefedaten in eine Array speichern
                     */
                    $temp_readers = [
                        'title' => $_POST['title'],
                        'text' => $_POST['text'],
                        'id_number' => $_POST['id_number'],
                        'correct' => $correct,
                    ];
                    /**
                     * Leserbriefdaten überprüfen und zur�ckgeben.
                     */
                    $objreaders->checkData($temp_readers);
                    $readers = $objreaders->getData();
                    /*
                     * überprüfen, ob die Userid Aufgel�st werden konnte
                     * Wenn ja, dann kann eine Vorschau oder Speichern erfolgen.
                     */
                    switch ($_POST['php_submit']) {
                        case $language['PREVIEW']:
                            // Vorschau
                            include_once($adminpath . 'preview.tpl.php');

                            break;
                        case $language['SAVE']:
                            // Speichern
                            $readers_id = $objreaders->saveData();
                            if ($error === true) {
                                $temp = '<p>Der Leserbriefe ' . $readers['title'] . ' wurde nicht gespeichert.</p>';
                            } else {
                                $temp = '<p>Der Leserbriefe ' . $readers['title'] . ' wurde erfolgreich gespeichert.</p>';
                                if ($readers['id_number'] !== null && ($wbbuserdata['canpress'] === 1 || $wbbuserdata['is_techno'] === 1 || $wbbuserdata['admin'] === 1)) {
                                    $temp .= "<form method='post' action='?mod=admin&admin=readers&action=insert_now&readers=" . $readers['id_number'] . "'>\n";
                                    $temp .= "<input type='hidden' name='id_number' value='" . $readers['id_number'] . "' />\n";
                                    $temp .= "<input type='submit' name='issuesubmitt' value='" . $language['INISSUE'] . "' />\n";
                                    $temp .= '</form>';
                                }
                                if ($readers['id_number'] !== null) {
                                    $temp .= "<form method='post' action='?mod=admin&admin=readers&action=insert_new&readers=" . $readers['id_number'] . "'>\n";
                                    $temp .= "<input type='hidden' name='id_number' value='" . $readers['id_number'] . "' />\n";
                                    $temp .= "<input type='submit' name='newubmitt' value='" . $language['INNEW'] . "' />\n";
                                    $temp .= '</form>';
                                }
                            }
                            $file = 'index';
                            break;
                        case $language['RESET']:
                            // Zur�cksetzen
                            $readers = [];

                            break;
                    }
                } //Leserbriefe aus der Datenbank auslesen und Daten ausgeben
                else {
                    $objreaders = Readers::getInstance($_GET['readers']);
                    $readers = $objreaders->getData();
                }
                break;
                //Leserbriefe löschen
            case 'del':
                if ($wbbuserdata['admin'] === 1 || $wbbuserdata['canpresse'] === 1 || $wbbuserdata['is_techno'] === 1) {
                    $file = 'index';
                    $objReaders = Readers::getInstance($_GET['readers']);
                    $readers = $objReaders->getValue('id_number', $_GET['readers'], 'title');
                    switch ($_GET['del']) {
                        case 'yes':

                            $objReaders->delData();
                            $temp = '<p> Der Leserbrief ' . $readers . ' wurde erfolgreich gel&ouml;scht.</p>';
                            break;
                        case 'no':
                            $temp = '<p>Der Leserbrief ' . $readers . ' wurde nicht gel&ouml;scht.</p>';
                            break;
                        default:
                            $temp = '<p>Soll der Leserbrief ' . $readers . ' wirklich gel�scht werden?</p>';
                            $temp .= "<form method='post' action='?mod=admin&admin=readers&action=del&readers=" . $_GET['readers'] . "&del=yes'>\n<input type='submit' name='issue_submit' value='Leserbrief L&ouml;schen' />\n</form>";
                            $temp .= "<form method='post' action='?mod=admin&admin=readers&action=del&readers=" . $_GET['readers'] . "&del=no'>\n<input type='submit' name='issue_submit' value='Nicht L&ouml;schen' />\n</form>";
                            break;
                    }
                } else {
                    $title .= 'Alle Leserbriefe';
                    $file = 'readers';
                    $all_active = "class='active'";
                }
                break;
                //Leserbriefe aus Ausgabe entfernen
            case 'uncase':
                $file = 'index';
                Readers::uncaseIssue($_GET['readers']);
                $temp = '<p> Der Leserbriefe ' . $readers . ' wurde zur�ck in die Warteschleife gestellt.</p>';
                break;
            case 'undel':
                $file = 'index';
                $objReaders = Readers::getInstance($_GET['readers']);
                $readers = $objReaders->getData();
                $objReaders->undelete($_GET['readers']);
                $temp = '<p> Der Leserbriefe <i>' . $readers['title'] . '</i> wurde zur�ck in die Warteschleife gestellt.</p>';
                break;
            default:
                $title .= 'Alle Leserbriefe';
                $file = 'readers';
                $all_active = "class='active'";
                break;
        }
}
//Abfrage einschr�nken
$cause = $cause1 !== null ? array_merge($cause1, $cause2) : $cause2;
//Auflistung der Leserbriefe
if ($file === 'readers') {
    $order[0] = 'id_issue';
    $liste = Readers::getList($cause, $order, 'DESC');
}
$readers_nav = "<ul id='readers_nav'><li " . $all_active . '>';
$readers_nav .= '<a ' . $all_active . " href='?mod=admin&admin=readers&readers=all'>Alle Leserbriefe</a></li>\n";
$readers_nav .= '<li  ' . $newissue_active . '><a ' . $newissue_active . " href='?mod=admin&admin=readers&readers=new'>Ausgabe in Vorbereitung</a></li>\n";
$readers_nav .= '<li  ' . $wait_active . '><a ' . $wait_active . " href='?mod=admin&admin=readers&readers=wait'>Warteschleife</a></li>\n";
$readers_nav .= '<li  ' . $nowissue_active . '><a ' . $nowissue_active . " href='?mod=admin&admin=readers&readers=now'>aktuelle Ausgabe</a></li>\n";
$readers_nav .= '<li  ' . $archiv_active . '><a ' . $archiv_active . " href='?mod=admin&admin=readers&readers=archiv'>archivierte Leserbriefe</a></li>\n";
if ($wbbuserdata['admin'] === 1 || $wbbuserdata['canpresse'] === 1 || $wbbuserdata['is_techno'] === 1) {
    $readers_nav .= '<li  ' . $del_active . '><a  ' . $del_active . " href='?mod=admin&admin=readers&readers=del'>gel&ouml;schte Leserbriefe</a></li>";
}
$readers_nav .= '</ul>';
