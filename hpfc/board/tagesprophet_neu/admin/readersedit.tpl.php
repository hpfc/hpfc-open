<?php

/**
 * Shows in the admin area the reader(Template).
 *
 * @package    HPFC\Tagesprophet\Administration
 * @author    Jana Pape
 * @copyright    2009 Jana Pape
 * All rights reserved.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License (GPL)
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * To read the license please visit http://www.gnu.org/copyleft/gpl.html
 */
declare(strict_types=1);

$ausgabe .= $preview;
/**
 * Formular zur Eingabe eines neuen Leserbriefes
 */

$ausgabe .= "<form method='post' action='?mod=admin&admin=readers&action=edit&readers=" . $_GET['readers'] . "'>\n";
/**
 * Leserbriefdaten
 */
$ausgabe .= '<fieldset><legend>' . $language['readers'] . "</legend>\n";
$ausgabe .= "<label for=\"title\">" . $language['TITLE'] . "<input type='text' name='title' size='30' value='" . $readers['title'] . "' />";
if (strlen($readers['text']['error']) > 1) {
    $ausgabe .= $readers['title']['error'];
}
$ausgabe .= "</label>\n";
$ausgabe .= $language['user'] . ': ' . $readers['username'];
$ausgabe . "<label for='mytext'>" . $language['readers'] . ':' . $readers['text']['error'] . "<br />\n";

// Gets replaced with TinyMCE, remember HTML in a textarea should be encoded -->
$ausgabe .= "<br /><textarea id='text' name='text' rows='15' cols='80' >\n";
$ausgabe .= $readers['text'] . "</textarea></label>\n<br />";
$ausgabe .= "</fieldset>\n";
$ausgabe .= "<fieldset>\n";
$ausgabe .= "<label for='corrector'>" . $language['correct'];
$ausgabe .= "<input type='checkbox' name='correct' value='yes'> " . $readers['correct'];
$ausgabe .= "</label><br />\n";
$ausgabe .= "<input type='hidden' name='id_number' value='" . $readers['id_number'] . "' />\n";
$ausgabe .= "<input type='hidden' name='user_id' value='" . $readers['red_id'] . "' />\n";
$ausgabe .= "<input type='submit' name='php_submit' value='" . $language['PREVIEW'] . "'>\n";
$ausgabe .= "<input type='submit' name='php_submit' value='" . $language['SAVE'] . "' />\n";
$ausgabe .= "<input type='reset' name='php_reset' value='" . $language['RESET'] . "' /></fieldset>\n";
$ausgabe .= "</form>\n";


?>
