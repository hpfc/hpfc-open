<?php

/**
 * Shows in the admin area the preview  (Template) .
 *
 * @package    HPFC\Tagesprophet\Administration
 * @author    Jana Pape
 * @copyright    2009 Jana Pape
 * All rights reserved.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License (GPL)
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * To read the license please visit http://www.gnu.org/copyleft/gpl.html
 */
declare(strict_types=1);

$preview = "<div id='preview'><h1>" . $language['PREVIEW'] . '</h1>';
$preview .= '<h2>' . $item['title'] . '</h2>';
if (isset($user)) {
    $author = null;
    $drawer = null;
    foreach ($user as $user_temp) {
        if ($user_temp['kind'] == 'author') {
            if ($author != null) {
                $author .= ', ';
            }
            $author .= $user_temp['username'] . '(' . $user_temp['haus'] . ')';
        }
        if ($user_temp['kind'] == 'drawer') {
            if ($drawer != null) {
                $drawer .= ', ';
            }
            $drawer .= $user_temp['username'] . '(' . $user_temp['haus'] . ')';
        }
    }
    if ($drawer != null) {
        $drawer = '<h3>' . $language['imagesfrom'] . $drawer . "</h3>\n";
    }
}
$preview .= '<h3>' . $language['wrotefrom'] . $author . "</h3>\n" . $drawer;
$preview .= "<p class='text'>" . $text . '</p></div>';
?>
