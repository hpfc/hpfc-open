<?php
/**
 * Shows in the admin area the issue page (Template).
 *
 * @package    HPFC\Tagesprophet\Administration
 * @author    Jana Pape
 * @copyright    2009 Jana Pape
 * All rights reserved.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License (GPL)
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * To read the license please visit http://www.gnu.org/copyleft/gpl.html
 */
declare(strict_types=1);


/**
 * Template
 */
if (($wbbuserdata['canpresse'] == 1 || $wbbuserdata['is_techno'] == 1 || $wbbuserdata['admin'] == 1)) {
    $ausgabe .= "<form method='post' action='?mod=admin&admin=issue&action=publish&issue=" . $issue['id_number'] . "'>\n";
    $ausgabe .= "<input type='submit' name='issue_submit' value='Ausgabe ver�ffentlichen' />\n";
    $ausgabe .= "</form>\n";
}
$ausgabe .= "<form method='post' action='?mod=admin&admin=issue&action=edit&issue=" . $issue['id_number'] . "'>\n";
$ausgabe .= "<label for='number'>" . $language['issueno'] . "<input type='text' name='number' size='5' value='" . $issue['number'] . "' /></label>\n";
$ausgabe . "<label for='my_text'>" . $language['TEXT'] . ":<br />\n";

$ausgabe .= "<textarea id='my_text' name='my_text' rows='15' cols='80' >\n";
$ausgabe .= $issue['text'] . "</textarea></label>\n";
$ausgabe .= "</fieldset>\n<br />";

$ausgabe .= "<fieldset><input type='hidden' name='id_number' value='" . $issue['id_number'] . "' />\n";
$ausgabe .= "<input type='submit' name='php_submit' value='" . $language['SAVE'] . "' />\n";
$ausgabe .= "<input type='reset' name='php_reset' value='" . $language['RESET'] . "' />\n</fieldset>";
$ausgabe .= "</form>\n";
if ($itemliste != null) {
    $i = 0;
    $ausgabe .= 'Artikel<table><tr><th>Titel</th><th>Kategorie</th><th>Haus</th><th>Bearbeiten</th></tr>';
    foreach ($itemliste as $item) {
        if ($i % 2 != 0) {
            $ausgabe .= "<tr class='ungerade'>";
        } else {
            $ausgabe .= "<tr class='gerade'>";
        }
        //Titel
        $ausgabe .= '<td>' . $item['title'] . "</td>\n";
        //Kategorie
        $ausgabe .= '<td>' . $item['category_name'] . "</td>\n";
        //Haus
        $ausgabe .= '<td>' . $item['haus'] . "</td>\n<td><nobr>";
        //Bearbeiten
        $ausgabe .= "<a href='?mod=admin&admin=item&action=edit&item=" . $item['id_number'] . "'><img title='Artikel bearbeiten' alt='Artikel bearbeiten' src='" . $stylepath . "images/edit.gif' /></a>\n";
        $ausgabe .= "<a href='?mod=admin&admin=item&action=uncase&item=" . $item['id_number'] . "'><img title='Artikel zur neuer Ausgabe entfernen' alt='Artikel aus neuer Ausgabe entfernen' src='" . $stylepath . "images/minus.gif' /></a>\n";
        //Artikel löschen
        if ($item['status'] == 'NEW' || (($wbbuserdata['admin'] == 1 || $wbbuserdata['TP'] == 1 || $wbbuserdata['is_techno'] == 1) && $item['status'] != 'NEW')) {
            $ausgabe .= "<a href='?mod=admin&admin=item&action=del&item=" . $item['id_number'] . "'><img title='Artikel l&ouml;schen' alt='Artikel l&ouml;schen' src='" . $stylepath . "images/delete.gif' /></a>\n";
        }
        //Vorschau
        $ausgabe .= "<a href='?mod=item&item=" . $item['id_number'] . "'><img title='Artikelvorschau' alt='Artikelvorschau' src='" . $stylepath . "images/preview.gif' /></a></nobr></td></tr>\n";
        $i++;
    }
    $ausgabe .= '</table>';
}
if ($readersliste != null) {
    $i = 0;
    $ausgabe .= 'Leserbriefe<table><tr><th>Titel</th><th>Haus</th><th>Bearbeiten</th></tr>';
    foreach ($readersliste as $readers) {
        if ($i % 2 != 0) {
            $ausgabe .= "<tr class='ungerade'>";
        } else {
            $ausgabe .= "<tr class='gerade'>";
        }
        //Titel
        $ausgabe .= '<td>' . $readers['title'] . "</td>\n";
        //Haus
        $ausgabe .= '<td>' . $readers['haus'] . "</td>\n<td><nobr>";
        //Bearbeiten
        $ausgabe .= "<a href='?mod=admin&admin=readers&action=edit&readers=" . $readers['id_number'] . "'><img title='Leserbrief bearbeiten' alt='Artikel bearbeiten' src='" . $stylepath . "images/edit.gif' /></a>\n";
        $ausgabe .= "<a href='?mod=admin&admin=readers&action=uncase&readers=" . $readers['id_number'] . "'><img title='Leserbrief von neuer Ausgabe entfernen' alt='Artikel aus neuer Ausgabe entfernen' src='" . $stylepath . "images/minus.gif' /></a>\n";
        //Artikel löschen
        if ($readers['status'] == 'NEW' || (($wbbuserdata['admin'] == 1 || $wbbuserdata['TP'] == 1 || $wbbuserdata['is_techno'] == 1) && $readers['status'] != 'NEW')) {
            $ausgabe .= "<a href='" . $adminpath . '?mod=readers&action=del&readers=' . $readers['id_number'] . "'><img title='Artikel l&ouml;schen' alt='Artikel l&ouml;schen' src='" . $stylepath . "images/delete.gif' /></a>\n";
        }
        //Vorschau
        $ausgabe .= "<a href='?readers=" . $readers['id_number'] . "'><img title='Artikelvorschau' alt='Artikelvorschau' src='" . $stylepath . "images/preview.gif' /></a></nobr></td></tr>\n";
        $i++;
    }

    $ausgabe .= '</table>';
}
if ($contactliste != null) {
    $i = 0;
    $ausgabe .= 'Kontaktanzeige<table><tr><th>Titel</th><th>Haus</th><th>Bearbeiten</th></tr>';
    foreach ($contactliste as $contact) {
        if ($i % 2 != 0) {
            $ausgabe .= "<tr class='ungerade'>";
        } else {
            $ausgabe .= "<tr class='gerade'>";
        }
        //Titel
        $ausgabe .= '<td>' . $contact['title'] . "</td>\n";
        //Haus
        $ausgabe .= '<td>' . $contact['haus'] . "</td>\n<td><nobr>";
        //Bearbeiten
        $ausgabe .= "<a href='?mod=admin&admin=contact&action=edit&contact=" . $contact['id_number'] . "'><img title='Artikel bearbeiten' alt='Artikel bearbeiten' src='" . $stylepath . "images/edit.gif' /></a>\n";
        $ausgabe .= "<a href='?mod=admin&admin=contact&action=uncase&contact=" . $contact['id_number'] . "'><img title='Artikel zur neuer Ausgabe entfernen' alt='Artikel aus neuer Ausgabe entfernen' src='" . $stylepath . "images/minus.gif' /></a>\n";
        //Artikel löschen
        if ($contact['status'] == 'NEW' || (($wbbuserdata['admin'] == 1 || $wbbuserdata['TP'] == 1 || $wbbuserdata['is_techno'] == 1) && $contact['status'] != 'NEW')) {
            $ausgabe .= "<a href='?mod=admin&admin=contact&action=del&contact=" . $contact['id_number'] . "'><img title='Artikel l&ouml;schen' alt='Artikel l&ouml;schen' src='" . $stylepath . "images/delete.gif' /></a>\n";
        }
        //Vorschau
        $ausgabe .= "<a href='?contact=" . $contact['id_number'] . "'><img title='Artikelvorschau' alt='Artikelvorschau' src='" . $stylepath . "images/preview.gif' /></a></nobr></td></tr>\n";
        $i++;
    }
    $ausgabe .= '</table>';
}
?>
