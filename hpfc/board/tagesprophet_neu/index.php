<?php

/**
 * Hauptdatei im öffentlichen Bereich.
 *
 * Welche Ausgabe angefordert wird, wird hier überprüft
 * index.php
 *
 * @create      on 20.04.2008
 * @modify      on 15.02.2009
 *
 * @author      Jana Pape (j.pape@dreagan-lin.de)
 *
 * @package     HPFC\Tagesprophet
 *
 * @copyright   2009 Jana Pape
 * All rights reserved.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License (GPL)
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * To read the license please visit http://www.gnu.org/copyleft/gpl.html
 */
declare(strict_types=1);

use Hpfc\DailyProphetArchive\Category;
use Hpfc\DailyProphetArchive\Config;
use Hpfc\DailyProphetArchive\Issue;

require_once __DIR__ . '/../../../bootstrap.php';

/**
 * Einbindung der Tabellen, Sprachen und Klassen.
 */
include_once($includepath . 'tables.php');
include_once($languageuagepath . 'main.php');
$config = Config::loadData();
$anzahl = null;
/**
 * Auslesen, welche Anzeige gewünscht ist.
 */
$mod = isset($_GET['mod']) ? strtolower((string) $_GET['mod']) : '';
$file = $mod;
if ($mod !== 'archiv' && $mod !== 'sitemap') {
    $file = 'issue';
}
if (isset($_GET['item'])) {
    $file = 'item';
}
if (isset($_GET['readers'])) {
    $file = 'readers';
}
if (isset($_GET['contact'])) {
    $file = 'contact';
}
if (isset($_GET['redaktion'])) {
    $file = 'redaktion';
}
$issue_header = null;
$admin = null;


/**
 * Für Redakteure werden verschiedene administrative Links angeboten.
 * $admin -> Link zur Redaktion
 * $edit -> Direkter Link zur Bearbeitung des Artikel bzw. der Ausgabe.
 */
if ($wbbuserdata['admin'] === 1 || $wbbuserdata['canpresse'] === 1 || $wbbuserdata['TP'] === 1 || $wbbuserdata['is_techno'] === 1) {
    $admin .= "<li><a href='?mod=admin'>Redaktion</a></li>";

    if (isset($_GET['issue']) || $_SERVER['QUERY_STRING'] === null) {
        if ($_SERVER['QUERY_STRING'] === null) {
            $issue = Issue::getIssue('NOW');
            $_GET['issue'] = $issue['id_number'];
        }
        $edit = "<a href='?mod=admin&admin=issue&action=edit&issue=" . $_GET['issue'] . "'>" . $language['edit'] . '</a>';
    }
    if (isset($_GET['item'])) {
        $edit = "<a href='?mod=admin&admin=item&action=edit&item=" . $_GET['item'] . "'>" . $language['edit'] . '</a>';
    }
    if (isset($_GET['readers'])) {
        $edit = "<a href='?mod=admin&admin=readers&action=edit&readers=" . $_GET['readers'] . "'>" . $language['edit'] . '</a>';
    }
}
/**
 * Titel der Seite.
 */
$title = "$language[name] | HP-FC";
$icounter = 0;

/**
 * Funktionalität der ausgewählten Seite in Abhängigkeite der URL-Parameter wird geladen.
 */

include_once($scriptpath . $file . '.php');
if ($mod === 'archiv' || $item['status'] ?? '' === 'ARCHIV' || $issue['status'] ?? '' === 'ARCHIV' || $readers['status'] ?? '' === 'ARCHIV' || $contact['status'] ?? '' === 'ARCHIV') {
    $archiv_active = " class='activ'";
} else {
    $issue_active = " class='activ'";
}
/**
 * Falls die Anzeige der Artikelvorschau mehrere Seiten einnimmt
 * $intervall: maximale Anzahl der Artikel auf der Seite
 * $side: Anzahl der Seiten.
 */
$anzahl = isset($liste) ? count($liste) : 0;
$start = 0;
$counter = $config['intervall'];
$page = "\n";
if (isset($_GET['side'])) {
    $start = ($_GET['side'] - 1) * $config['intervall'];
    $counter += $start;
}


$side = floor($anzahl / $config['intervall']);
$rest = $anzahl % $config['intervall'];
$page = $language['page'];
if ($rest === 0) {
    $side--;
}
for ($y = 0; $y < $side + 1; $y++) {
    if ($y === ($_GET['side']) - 1 || ($y === 0 && !isset($_GET['side']))) {
        $page .= ' [' . ($y + 1) . '] ';
    } else {
        $ref = explode('&', (string) $_SERVER['QUERY_STRING']);
        $loc = '';
        foreach ($ref as $var) {
            if (!str_starts_with($var, 'side')) {
                if ($loc !== '') {
                    $loc .= '&';
                }
                $loc .= $var;
            }
        }
        $page .= " <a href='?" . $loc . '&side=' . ($y + 1) . "'>[" . ($y + 1) . ']</a> ';
    }
}
$page .= "\n";


/**
 * Kategorien aus der Datenbank auslesen und Für die Navigation zur Verfügung stellen.
 */
$catList = Category::listData();

/**
 * Einbindung der Webanzeige.
 */
if ($file !== 'sitemap') {
    require_once(__DIR__ . '/../menu.inc.php');
    include_once($stylepath . 'header.tpl.php');
}

include_once($stylepath . $file . '.tpl.php');

if ($file !== 'sitemap') {
    include_once($stylepath . 'footer.tpl.php');
}

/**
 * Ausgabe der Anzeige.
 */
echo $ausgabe;
