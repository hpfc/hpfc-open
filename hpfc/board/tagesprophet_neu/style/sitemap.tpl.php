<?php
/**
 * @package    HPFC\Tagesprophet
 * @author      Jannis Grimm (jannis@huffle.de)
 * @version     2.0
 * All rights reserved.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License (GPL)
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * To read the license please visit http://www.gnu.org/copyleft/gpl.html
 */
declare(strict_types=1);

$ausgabe = '<?xml version="1.0" encoding="UTF-8"?>
    <urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';

$ausgabe .= "
    <url>
            <loc>https://www.hp-fc.de/hpfc/board/hpfc_tagesprophet.php</loc>
            <lastmod>$indexLastMod</lastmod>
        </url>
    ";

foreach ($issueLastMods as $issueId => $issueLastMod) {
    $ausgabe .= "
        <url>
            <loc>https://www.hp-fc.de/hpfc/board/hpfc_tagesprophet.php?mod=archiv&amp;issue=$issueId</loc>
            <lastmod>$issueLastMod</lastmod>
        </url>
    ";
}

foreach ($items as $item) {
    $ausgabe .= "
        <url>
            <loc>https://www.hp-fc.de/hpfc/board/hpfc_tagesprophet.php?item=$item[id]</loc>
            <lastmod>$item[lastmod]</lastmod>
        </url>
    ";
}

$ausgabe .= '
    </urlset>'; ?>
