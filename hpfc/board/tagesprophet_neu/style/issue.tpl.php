<?php

/**
 * @create      on 20.04.2008
 * @modify      on 15.02.2009
 *
 *
 * @author      Jana Pape (j.pape@dreagan-lin.de)
 * @version     2.0
 * @package    HPFC\Tagesprophet
 * @copyright   2009 Jana Pape
 * All rights reserved.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License (GPL)
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * To read the license please visit http://www.gnu.org/copyleft/gpl.html
 *
 *
 *
 */
declare(strict_types=1);

if (sizeof($liste) != null) {
    $ausgabe .= $edit;
    $ausgabe .= "<h2 class='issue'>" . $language['issueno'] . $issue['number'];
    if ($issue['publish_date'] != 0) {
        $ausgabe .= ' vom ' . date('d.m.Y', strtotime($issue['publish_date']));
    }
    $ausgabe .= "</h2>\n";
    $ausgabe .= "<div class='top'>" . $issue['text'] . '</div>';
    for ($x = $start; $x < $counter && $x < $anzahl; $x++) {
        $from = null;
        $item = $liste[$x];
        if ($item['kind'] == 'contact') {
            $ausgabe .= "\n<h2><a href='?contact=" . $item['id_number'] . "'>Kontaktanzeige: " . $item['title'] . '</a></h2>';
            $ausgabe .= "\n<p><a href='?contact=" . $item['id_number'] . "'>" . $item['text'] . "</a></p>\n";
        } else {
            $ausgabe .= "\n<h2><a href='?" . $item['kind'] . '=' . $item['id_number'] . "'>" . $item['title'] . '</a></h2>';
        }
        if ($item['kind'] == 'readers') {
            $ausgabe .= "\n<p>Leserbrief zu <a href='?mod=item&item=" . $item['item_id'] . "'>" . $item['item'] . "</a><br /><a href='?readers=" . $item['id_number'] . "'>" . $item['text'] . "</a></p>\n";
        }
        $ausgabe .= "<div class='path'><p>&gt;&gt; <a href='?mod=issue&issue=" . $item['id_issue'] . "'>" . $language['ISSUENR'] . ' ' . $item['issue_number'] . '</a>';
        if ($item['kind'] != 'item') {
            $ausgabe .= "&gt;&gt; <a href='?mod=issue&issue=" . $item['id_issue'] . '&' . $item['kind'] . "'>" . $language[$item['kind']] . '</a></p></div>';
            $from .= "<p class='write'>" . $language['wrotefrom'] . $item['username'] . "</p>\n";
        } else {
            $ausgabe .= " &gt;&gt; <a href='?mod=issue&issue=" . $item['id_issue'] . '&category=' . $item['category'] . "'>" . $item['category_name'] . '</a></p></div>';
            if ($item['user'][0]['username'] != null) {
                $author = null;
                $drawer = null;
                foreach ($item['user'] as $user) {
                    if ($user['kind'] == 'author') {
                        if ($author != null) {
                            $author .= ', ';
                        }
                        $author .= $user['username'] . '(' . $user['haus'] . ')';
                    }
                    if ($user['kind'] == 'drawer') {
                        if ($drawer != null) {
                            $drawer .= ', ';
                        }
                        $drawer .= $user['username'] . '(' . $user['haus'] . ')';
                    }
                }
                if ($drawer != null) {
                    $drawer = '<br />' . $language['imagesfrom'] . $drawer . "\n";
                }
                $from .= "<span class='write'>" . $language['wrotefrom'] . $author . $drawer . "<br /></span>\n";
            }
            $ausgabe .= "<a href='?" . $item['kind'] . '=' . $item['id_number'] . "'><p class='preview' >";
            if ($item['summary'] != null) {
                $ausgabe .= $item['summary'];
            } else {
                $ausgabe .= $item['text'];
            }
            $ausgabe .= "...</p></a>\n" . $from . "\n";
            $ausgabe .= "<div class='under'><br /><br /></div>";
            $k++;
        }
    }
} else {
    $ausgabe .= 'Keine Artikel gefunden.';
}

?>
