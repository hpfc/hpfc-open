<?php
declare(strict_types=1);

$ausgabe .= "<div class='redaktion'><h2>Redaktion</h2>\n";
$ausgabe .= "<ul><strong>Chefredakteure:</strong>\n";
foreach ($list as $red) {
    if ($red['hpfc_id'] == 53) {
        $ausgabe .= '<li>' . $red['username'];
        $ausgabe .= "<a href='pms.php?action=newpm&userid=";
        $ausgabe .= $red['userid'] . "'><img src='" . $stylepath . "images/pn.gif' alt='" . $red['username'] . " eine Private Nachricht schreiben' title='";
        $ausgabe .= $red['username'] . " eine Private Nachricht schreiben'></a>";
        $ausgabe .= "</li>\n";
    }
}
$ausgabe .= "</ul>\n<br /><ul><strong>Redakteure:</strong>\n";
foreach ($list as $red) {
    if ($red['hpfc_id'] == 99) {
        $ausgabe .= '<li>' . $red['username'];
        $ausgabe .= "<a href='pms.php?action=newpm&userid=";
        $ausgabe .= $red['userid'] . "'><img src='" . $stylepath . "images/pn.gif' alt='" . $red['username'] . " eine Private Nachricht schreiben' title='";
        $ausgabe .= $red['username'] . " eine Private Nachricht schreiben'></a>";
        $ausgabe .= "</li>\n";
    }
}

$ausgabe .= "</ul>\n<br /><ul><strong>Technik:</strong>\n";
$ausgabe .= '<li>qivis';
$ausgabe .= "<a href='pms.php?action=newpm&userid=21629";
$ausgabe .= "'><img src='" . $stylepath . "images/pn.gif' alt='qivis eine Private Nachricht schreiben' title='";
$ausgabe .= "qivis eine Private Nachricht schreiben'></a></li></ul>";
//$ausgabe.="<li>Vivianne";
//$ausgabe.="<a href='pms.php?action=newpm&userid=86763";
//$ausgabe.="'><img src='".$stylepath."images/pn.gif' alt='Vivianne eine Private Nachricht schreiben' title='";
//$ausgabe.="Vivianne eine Private Nachricht schreiben'></a></li></ul>\n";
$ausgabe .= "<br /><ul><strong>Mailadresse:</strong><li> <br /><a href='mailto:magischemedien&#64;pottercast.de'>magischemedien&#64;pottercast.de</a></li><li></ul></div>";
?>
