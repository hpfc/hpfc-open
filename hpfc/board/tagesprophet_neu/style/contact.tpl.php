<?php

/**
 * @create      on 20.04.2008
 * @modify      on 15.02.2009
 *
 *
 * @author      Jana Pape (j.pape@dreagan-lin.de)
 * @version     2.0
 * @package    HPFC\Tagesprophet
 * @copyright   2009 Jana Pape
 * All rights reserved.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License (GPL)
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * To read the license please visit http://www.gnu.org/copyleft/gpl.html
 *
 *
 *
 */
declare(strict_types=1);

if (isset($liste)) {
    if ($contact['status'] == 'ARCHIV') {
        $wildcard = 'mod=archiv';
        $archiv = "<a href='?mod=archiv'>" . $language['Archiv'] . '</a> &gt;&gt; ';
    } else {
        $wildcard = 'mod=issue';
    }
    $ausgabe .= "\n<h2>Kontaktanzeige</h2>\n";

    if (isset($liste)) {
        foreach ($liste as $contact) {
            $ausgabe .= $edit;
            $ausgabe .= "<p class='path'>" . $archiv . "<a href='?" . $wildcard . '&issue=' . $contact['id_issue'] . "'>";
            $ausgabe .= $language['issueno'] . $contact['issue_number'] . '</a> &gt;&gt; ';
            $ausgabe .= "<a href='?" . $wildcard . '&issue=' . $contact['id_issue'] . "&contact'>Kontaktanzeigen</a> &gt;&gt; </p>";
            if ($contact['title'] != null) {
                $ausgabe .= '<h4>' . $contact['title'] . '</h4>';
            }
            $ausgabe .= '<p>' . $language['wrotefrom'] . $contact['username'] . "</p>\n";
            $ausgabe .= "<p class='text'>" . $contact['text'] . '</p>';
            $ausgabe .= "<a href='?mod=contact&contact=" . $contact['id_number'] . "&action=reply'>Antworten</a>";
        }
    }
}
$ausgabe .= "<p><a href='?contact&action=new'>Anzeige aufgeben</a></p>";
?>
