<?php

/**
 * @create      on 20.04.2008
 * @modify      on 15.02.2009
 *
 * @author      Jana Pape (j.pape@dreagan-lin.de)
 *
 * @package    HPFC\Tagesprophet
 *
 * @copyright   2009 Jana Pape
 * All rights reserved.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License (GPL)
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * To read the license please visit http://www.gnu.org/copyleft/gpl.html
 */
declare(strict_types=1);

use Hpfc\DailyProphetArchive\Issue;

$navigation = [1];
if ($mod !== 'archiv') {
    $navigation = Issue::getNavigation();
}

$wildcard = '';
if ($mod === 'archiv') {
    $wildcard = 'mod=archiv&';
}
if ($mod === 'issue') {
    $wildcard = 'mod=issue&';
}

$ausgabe .= '</div>';
$ausgabe .= "<div class='bottom'><p class='page'>" . $page;
$ausgabe .= "<br />Tagesprophet &copy; Version 2.0 2009 by <a href='mailto:j.pape@dreagan-lin.de'>Vivianne</a>";
$ausgabe .= "<br /></p></div></div><div id='sidebar'><ul>\n";
/**
 * Navigation anhand von den Kategorien.
 */
foreach ($navigation as $navcat) {
    foreach ($catList as $category) {
        if (($category['id_number'] === $navcat['category'] && $navcat['anzahl'] !== 0) || $mod === 'archiv') {
            $ausgabe .= '<li';
            if ($_GET['category'] === $category['id_number']) {
                $ausgabe .= " class='active'><a class='active' ";
            } else {
                $ausgabe .= '><a ';
            }
            $ausgabe .= "href='?" . $wildcard . 'category=' . $category['id_number'] . "'>" . $category['name'] . "</a></li>\n";
        }
    }
}
// if(($navcat['category']=='readers'&&$navcat['anzahl']!=0)||$mod=='archiv'){
$ausgabe .= '<li';
if (isset($_GET['readers'])) {
    $ausgabe .= " class='active'><a class='active' ";
} else {
    $ausgabe .= '><a ';
}
$ausgabe .= "href='?" . $wildcard . "readers'>" . $language['readers'] . "</a></li>\n";
// }

//    if(($navcat['category']=='contact'&&$navcat['anzahl']!=0)||$mod=='archiv'){
$ausgabe .= '<li';
if (isset($_GET['contact'])) {
    $ausgabe .= " class='active'><a class='active' ";
} else {
    $ausgabe .= '><a ';
}
$ausgabe .= "href='?" . $wildcard . "contact'>" . $language['contact'] . "</a></li>\n";
//  }
//}
$ausgabe .= '<li';
if (isset($_GET['redaktion'])) {
    $ausgabe .= " class='active'><a class='active' ";
} else {
    $ausgabe .= '><a ';
}
$ausgabe .= "href='?redaktion'>Redaktion</a></li>\n";
$ausgabe .= "<li><a href='index.php'>Gro&szlig;e Halle</a></li>\n";
$ausgabe .= "</ul>\n</div>\n</main></body></html>";
