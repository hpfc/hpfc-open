<?php

/**
 * @create      on 20.04.2008
 * @modify      on 15.02.2009
 *
 * @author      Jana Pape (j.pape@dreagan-lin.de)
 *
 * @package    HPFC\Tagesprophet
 *
 * @copyright   2009 Jana Pape
 * All rights reserved.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License (GPL)
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * To read the license please visit http://www.gnu.org/copyleft/gpl.html
 */
declare(strict_types=1);

use Hpfc\DailyProphetArchive\Item;

if (isset($_GET['item']) && $wbbuserdata['username'] !== 'guest') {
    $ausgabe .= "<a href='?item=" . $_GET['item'] . "&readers=new'>" . $language['readers_write'] . '</a><br /><br />';
}
if (isset($liste) && count($liste) !== null) {
    if (isset($_GET['item'])) {
        $ausgabe .= "<h2 class='readers'>Leserbriefe zu " . $itemtitle;
        $ausgabe .= ' vom ' . date('d.m.Y', strtotime((string) $itemdate)) . "</h2>\n";
    }
    $listeCount = count($liste);
    for ($x = $start; $x < $counter && $x < $listeCount; $x++) {
        $readers = $liste[$x];
        if ($readers['status'] === 'NEW') {
            $ausgabe .= "<div id='new'>";
        }
        if (!isset($_GET['item'])) {
            $itemtitle = Item::getValue('id_number', $readers['item_id'], 'title');
            $itemdate = Item::getDate($readers['item_id']);
            $ausgabe .= "<h2 class='readers'>Leserbriefe zu <a href='?item=" . $readers['item_id'] . "'>";
            $ausgabe .= $itemtitle;
            $ausgabe .= '</a>';
            $ausgabe .= ' vom ' . date('d.m.Y', strtotime($itemdate)) . "</h2>\n";
        }
        $ausgabe .= "<h3 class='readers'>" . $readers['title'] . '</h3>';
        $ausgabe .= "<a href='?readers=" . $readers['id_number'] . "'><p class='preview' >" . $readers['text'];
        $ausgabe .= "...</p></a>\n";
        $ausgabe .= "<p class='write'>" . $language['wrotefrom'] . $readers['username'] . ' (am ';
        $ausgabe .= date('d.m.Y', strtotime((string) $readers['insert_date']));
        $ausgabe .= ").</p>\n";
        if ($readers['status'] === 'NEW') {
            $ausgabe .= '</div>';
        }
        $ausgabe .= "<div class='under'><br /><br /></div>";
        $k++;
    }
} else {
    $ausgabe .= $language['readers_publish'];
}
