<?php

/**
 * @create      on 20.04.2008
 * @modify      on 15.02.2009
 *
 *
 * @author      Jana Pape (j.pape@dreagan-lin.de)
 * @version     2.0
 * @package    HPFC\Tagesprophet
 * @copyright   2009 Jana Pape
 * All rights reserved.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License (GPL)
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * To read the license please visit http://www.gnu.org/copyleft/gpl.html
 *
 *
 *
 */
declare(strict_types=1);

if ($readers['status'] == 'ARCHIV') {
    $wildcard = 'mod=archiv';
    $archiv = "<a href='?mod=archiv'>" . $language['Archiv'] . '</a> &gt;&gt; ';
} else {
    $wildcard = 'mod=issue';
}
$ausgabe .= "\n<h2>" . $readers['title'] . "</h2>\n";
$ausgabe .= "\n<h3>Leserbrief zu <a href='?mod=item&item=" . $readers['item_id'] . "'>" . $readers['item'] . "</a></h3>\n";
$ausgabe .= '<h3>' . $language['wrotefrom'] . $readers['username'] . ' (am ';
$ausgabe .= date('d.m.Y', strtotime($readers['insert_date']));
$ausgabe .= ").</h3>\n";

$ausgabe .= $edit;
$ausgabe .= "<p class='path'>" . $archiv . "<a href='?" . $wildcard . '&issue=' . $readers['id_issue'] . "'>";
$ausgabe .= $language['issueno'] . $readers['issue_number'] . '</a> &gt;&gt; ';
$ausgabe .= "<a href='?" . $wildcard . '&issue=' . $readers['id_issue'] . "&readers='>" . $language['readers'] . '</a> &gt;&gt; </p>';
$ausgabe .= "<p class='text'>" . $readers['text'];
$ausgabe .= '</p>';
?>
