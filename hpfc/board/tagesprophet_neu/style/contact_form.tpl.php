<?php

/**
 * @create      on 20.04.2008
 * @modify      on 15.02.2009
 *
 *
 * @author      Jana Pape (j.pape@dreagan-lin.de)
 * @version     2.0
 * @package    HPFC\Tagesprophet
 * @copyright   2009 Jana Pape
 * All rights reserved.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License (GPL)
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * To read the license please visit http://www.gnu.org/copyleft/gpl.html
 *
 *
 *
 */
declare(strict_types=1);

if ($wbbuserdata['username'] != 'guest') {
    switch ($_GET['action']) {
        case 'reply':
            if ($contact['title'] != null) {
                $ausgabe .= '<h4>' . $contact['title'] . '</h4>';
            }
            $ausgabe .= '<p>
    ' . $language['wrotefrom'] . $contact['username'] . "</p>\n";
            $ausgabe .= "<p class='text'>" . $contact['text'] . '</p>';

            $ausgabe .= "
<form method='post' id='contact' name='contact' action='?contact=" . $_GET['
      contact'] . "&action=reply'>";
            $ausgabe .= 'Kontaktanzeige beantworten<br/>';
            if ($reply['text'] != null) {
                $ausgabe .= "
<div id='contact'>";
                $ausgabe .= $language['PREVIEW'];
                $ausgabe .= '<p>' . $contact_reply['text'] . '</p>';
                $ausgabe .= '
</div>';
            }
            $ausgabe .= "<p class='notice'>" . $config['contact_notice'] . '</p>';
            $ausgabe .= "<br/>
<label for='contact_reply'>" . $language['text'] . "
    <textarea name='contact_reply' rows='15' cols='80'>" . $contact_reply['text'] . " </textarea>
</label>\n";
            $ausgabe .= '
<fieldset>';
            $ausgabe .= "
    <input type='hidden' name='username' value='" . $wbbuserdata[' username
    '] . "' />\n";
            $ausgabe .= "
    <input type='hidden' name='userid' value='" . $wbbuserdata[' userid
    '] . "' />\n";
            $ausgabe .= "
    <input type='submit' name='php_submit' value='" . $language[' PREVIEW
    '] . "'>\n";
            $ausgabe .= "
    <input type='submit' name='php_submit' value='" . $language[' send
    '] . "' />\n";
            $ausgabe .= "
    <input type='reset' name='php_reset' value='" . $language[' RESET
    '] . "' />\n";
            $ausgabe .= '
</fieldset></form>';
            break;
        case 'new':
            $ausgabe .= '<h3>Kontaktanzeige
    verfassen</h3>';
            if ($_POST['php_submit'] == $language['PREVIEW']) {
                $ausgabe .= $language['PREVIEW'];
                $ausgabe .= $contact['title_error'];
                $ausgabe .= $contact['text_error'];
                $ausgabe .= '
<h3>' . $contact['title'] . '</h3>';
                $ausgabe .= '<p>' . $contact['text'] . '</p>';
            }
            $ausgabe .= "<p class='notice'>
    " . $config['contact_notice'] . '</p>';
            $ausgabe .= "
<form method='post' id='contact' name='contact' action='?contact&action=new'>";
            $ausgabe .= "
    <label for='title'>" . $language['TITLE'] . "
        <input type='text' name='title' value='" . $contact[' title
        '] . "' />
    </label>
    ";
            $ausgabe .= "<br/>
    <label for='contact_text'>" . $language['text'] . "
        <textarea name='contact_text' rows='15' cols='80'>" . $contact['text'] . "</textarea>
    </label>
    \n";
            $ausgabe .= '
    <fieldset>';
            $ausgabe .= "
        <input type='hidden' name='username' value='" . $wbbuserdata[' username
        '] . "' />\n";
            $ausgabe .= "
        <input type='hidden' name='userid' value='" . $wbbuserdata[' userid
        '] . "' />\n";
            $ausgabe .= "
        <input type='hidden' name='email' value='" . $wbbuserdata[' email
        '] . "' />\n";
            $ausgabe .= "
        <input type='submit' name='php_submit' value='" . $language[' PREVIEW
        '] . "'>\n";
            $ausgabe .= "
        <input type='submit' name='php_submit' value='" . $language[' send
        '] . "' />\n";
            $ausgabe .= "
        <input type='reset' name='php_reset' value='" . $language[' RESET
        '] . "' />\n";
            $ausgabe .= '
    </fieldset>
</form>';
            break;
    }
} else {
    $ausgabe .= 'Um eine Kontakanzeige aufzugeben, logge dich bitte ein.';
}

?>
