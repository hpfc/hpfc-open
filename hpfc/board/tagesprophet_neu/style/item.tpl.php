<?php
/**
 * @create      on 20.04.2008
 * @modify      on 15.02.2009
 *
 *
 * @author      Jana Pape (j.pape@dreagan-lin.de)
 * @version     2.0
 * @package    HPFC\Tagesprophet
 * @copyright   2009 Jana Pape
 * All rights reserved.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License (GPL)
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * To read the license please visit http://www.gnu.org/copyleft/gpl.html
 *
 *
 *
 */
declare(strict_types=1);

if ($item['status'] == 'ARCHIV') {
    $wildcard = 'mod=archiv';
    $archiv = "<a href='?mod=archiv'>" . $language['Archiv'] . '</a> &gt;&gt; ';
} else {
    $wildcard = 'mod=issue';
}
$item['text'] = preg_replace(
    '/\.\.\/images\/old\//i',
    '="' . $config['home_path'] . 'images/artikelbilder/',
    $item['text']
);
$ausgabe .= "\n<h1>" . $item['title'] . "</h1>\n";
if ($item['issue_date'] != 0) {
    $ausgabe .= "<p class='liste'>ver&ouml;ffentlicht am " . date('d.m.Y', strtotime($item['issue_date'])) . "</p>\n";
}
if ($item['user'][0]['username'] != null) {
    $author = null;
    $drawer = null;
    foreach ($item['user'] as $user) {
        if ($user['kind'] == 'author') {
            if ($author != null) {
                $author .= ', ';
            }
            $author .= $user['username'] . '(' . $user['haus'] . ')';
        }
        if ($user['kind'] == 'drawer') {
            if ($drawer != null) {
                $drawer .= ', ';
            }
            $drawer .= $user['username'] . '(' . $user['haus'] . ')';
        }
    }
    if ($drawer != null) {
        $drawer = '<footer>' . $language['imagesfrom'] . $drawer . "</footer>\n";
    }
    $ausgabe .= '<footer>' . $language['wrotefrom'] . $author . "</footer>\n" . $drawer;
}
$ausgabe .= $edit;
$ausgabe .= "<p class='path'>" . $archiv . "<a href='?" . $wildcard . '&issue=' . $item['id_issue'] . "'>";
$ausgabe .= $language['issueno'] . $item['issue_number'] . '</a> &gt;&gt; ';
$ausgabe .= "<a href='?" . $wildcard . '&issue=' . $item['id_issue'] . '&category=' . $item['category'] . "'>" . $item['category_name'] . '</a> &gt;&gt; </p>';
$ausgabe .= "<p class='text'>" . $item['text'];
$ausgabe .= "</p><p class='readers'><a href='?item=" . $item['id_number'] . "&readers'>" . $language['readers'] . "</a>\n";
?>
