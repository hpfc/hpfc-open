<?php

/**
 * @create      on 20.04.2008
 * @modify      on 15.02.2009
 *
 *
 * @author      Jana Pape (j.pape@dreagan-lin.de)
 * @version     2.0
 * @package    HPFC\Tagesprophet
 * @copyright   2009 Jana Pape
 * All rights reserved.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License (GPL)
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * To read the license please visit http://www.gnu.org/copyleft/gpl.html
 *
 *
 *
 */
declare(strict_types=1);

$ausgabe = '<!DOCTYPE html>' . "\n";
$ausgabe .= '<html lang="de">' . "\n<head>\n";
$ausgabe .= '<title>' . $title . "</title>\n";
$ausgabe .= $meta;
$ausgabe .= "<link rel='stylesheet' href='" . $stylepath . "style.css' />\n";
$ausgabe .= "<link rel='stylesheet' href='../css/menu.css?v9' />\n";
$ausgabe .= "<meta charset=\"utf-8\">";
$ausgabe .= "</head>\n<body>\n";
$ausgabe .= getHpfcMenu('hpfc');
$ausgabe .= '<main>';
$ausgabe .= "<div id='header'><h1>" . $language['name'] . "</h1>\n";
$ausgabe .= $issue_header;
$ausgabe .= "</div><div id='nav'>\n";
$ausgabe .= '<ul><li><a' . $issue_active . " href='?'>Aktuelle Ausgabe</a></li>\n";
$ausgabe .= '<li><a' . $archiv_active . " href='?mod=archiv'>Archiv</a></li>\n" . $admin;
$ausgabe .= "</ul>\n</div>\n<div id='container'>\n<div id='main' class='Spalte'>\n";
?>
