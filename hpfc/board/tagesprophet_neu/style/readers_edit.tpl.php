<?php

/**
 * @create      on 20.04.2008
 * @modify      on 15.02.2009
 *
 * @author      Jana Pape (j.pape@dreagan-lin.de)
 *
 * @package    HPFC\Tagesprophet
 *
 * @copyright   2009 Jana Pape
 * All rights reserved.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License (GPL)
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * To read the license please visit http://www.gnu.org/copyleft/gpl.html
 */
declare(strict_types=1);

use Hpfc\DailyProphetArchive\Item;
use Hpfc\DailyProphetArchive\Readers;

$status = Item::getValue('id_number', $_GET['item'], 'Status');

if ($status === 'NOW' || $status === 'ARCHIV') {
    if ($_POST['php_submit'] === $language['PREVIEW']) {
        $ausgabe .= $language['PREVIEW'];
        $ausgabe .= $readers['title_error'];
        $ausgabe .= $readers['text_error'];
        $ausgabe .= '<h2>' . $readers['item'] . '</h2>';
        $ausgabe .= '<h3>' . $readers['title'] . '</h3>';
        $ausgabe .= '<p>' . $readers['text'] . '</p>';
    }
    $ausgabe .= "<form method='post' id='readers' name='readers' action='?mod=item&item=" . $_GET['item'] . "&readers=new'>";
    $ausgabe .= "Leserbrief  zu <a href='?mod=item&item=" . $_GET['item'] . "'>" . Item::getValue(
        'id_number',
        $_GET['item'],
        'title'
    ) . '</a> verfassen<br />';
    $ausgabe .= "<p class='notice'>" . $config['readers_notice'] . '</p>';
    $ausgabe .= "<br /><label for ='title'>" . $language['TITLE'] . "<br /><input type='text' name='title' size='60' value='" . $readers['title'] . "' /></label>";
    $ausgabe .= "<br /><label for ='readerstext'>" . $language['text'] . "<br /><textarea name='readerstext'  rows='15' cols='70' >" . $readers['text'] . " </textarea></label>\n";
    $ausgabe .= '<fieldset>';

    $ausgabe .= "<input type='hidden' name='username' value='" . $wbbuserdata['username'] . "' />\n";
    $ausgabe .= "<input type='hidden' name='userid' value='" . $wbbuserdata['userid'] . "' />\n";
    $ausgabe .= "<input type='submit' name='php_submit' value='" . $language['PREVIEW'] . "'>\n";
    $ausgabe .= "<input type='submit' name='php_submit' value='" . $language['send'] . "' />\n";
    $ausgabe .= "<input type='reset' name='php_reset' value='" . $language['RESET'] . "' />\n";
    $ausgabe .= '</fieldset></form>';
} else {
    if (!isset($_GET['item']) && ($_GET['mod'] = 'readers' || $_GET['mod'] === 'issue' || $_GET['mod'] === '')) {
        $cause1 = [
            'status' => 'NOW',
        ];
        $cause = array_merge($cause1, $cause2);
        $liste = Readers::getList($cause);
        include_once($stylepath . 'liste.tpl.php');
    }
    $ausgabe .= 'Es existiert kein Artikel.';
}
