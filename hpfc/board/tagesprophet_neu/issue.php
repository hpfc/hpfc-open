<?php

/**
 * Ausgabenausgabe.
 *
 * Es werden Einzelne Artikel, Ausgaben, Kategorien
 * angezeigt, die veröffentlicht wurden.
 * Falls ein nicht archivierter, vorhandener oder
 * veröffentlichter Artikel angewählt wird, erscheint
 * Userabhängig eine entsprechende Meldung.
 *
 * issue.php
 *
 * @create      on 20.04.2008
 * @modify      on 15.02.2009
 *
 * @author      Jana Pape (j.pape@dreagan-lin.de)
 *
 * @package     HPFC\Tagesprophet
 *
 * @copyright   2009 Jana Pape
 * All rights reserved.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License (GPL)
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * To read the license please visit http://www.gnu.org/copyleft/gpl.html
 */
declare(strict_types=1);

use Hpfc\DailyProphetArchive\Contact;
use Hpfc\DailyProphetArchive\Issue;
use Hpfc\DailyProphetArchive\Item;
use Hpfc\DailyProphetArchive\Readers;

$cause2 = [
    'del' => '0',
];
$file = 'issue';
$liste1 = [];
$liste2 = [];
$liste3 = [];
$all = true;
/**
 * Auflistung der aktuellen Ausgabe/Artikel/
 * Leserbriefe/Kontaktanzeigen bzw.
 * Weiterleitung bei archivierter oder noch
 * nicht veröffentlichter Ausgabe
 * mit entsprechender userabhängigen Meldung.
 */

if (isset($_GET['category']) && $_GET['category'] !== null && is_numeric($_GET['category'])) {
    $cause3 = [
        'category' => $_GET['category'],
        'status' => 'NOW',
    ];
    $cause2 = array_merge($cause3, $cause2);
    $all = false;
}
if (is_numeric($_GET['issue'])) {
    $status = Issue::getValue('id_number', $_GET['issue'], 'status');
    switch ($status) {
        case 'NEW':
            if ($admin !== null) {
                $objIssue = Issue::getInstance((int) $_GET['issue']);
                $issue = $objIssue->getData();
                $cause1 = [
                    'status' => 'NEW',
                    'id_issue' => $_GET['issue'],
                ];
                $cause = array_merge($cause1, $cause2);
                $liste = Item::getList($cause);
                if (!isset($_GET['category'])) {
                    $liste2 = Readers::getList($cause);
                    $liste3 = Contact::getList($cause);
                }
            } else {
                $print = 'Die ausgewählte Ausgabe ist nicht vorhanden';
                $print = "<br /> Sie werden in wenigen Augenblicken zur <a href='?'>aktuellen Ausgabe</a> weitergeleitet.";
                $meta = "<meta http-equiv='Refresh' content='1; url=hpfc_tagesprophet.php?'>";
                $file = 'index';
            }
            break;
        case 'NOW':
            $objIssue = Issue::getInstance((int) $_GET['issue']);
            $issue = $objIssue->getData();
            $cause1 = [
                'status' => 'NOW',
            ];
            $cause = array_merge($cause1, $cause2);
            $liste = Item::getList($cause);
            if (!isset($_GET['category'])) {
                $liste2 = Readers::getList($cause);
                $liste3 = Contact::getList($cause);
            }
            break;
        case 'ARCHIV':
            $print = 'Die ausgewählte Ausgabe befindet sich bereits im Archiv';
            $print = "<br /> Sie werden in wenigen Augenblicken zum <a href='?archiv'>Archiv</a> weitergeleitet.";
            $meta = "<meta http-equiv='Refresh' content='1; url=hpfc_tagesprophet.php?mod=archiv&issue=" . $_GET['issue'] . "'>";
            $file = 'index';
            break;
        default:
            $print = 'Die ausgewählte Ausgabe ist nicht vorhanden';
            $print = "<br /> Sie werden in wenigen Augenblicken zur <a href='?'>aktuellen Ausgabe</a> weitergeleitet.";
            $meta = "<meta http-equiv='Refresh' content='1; url=hpfc_tagesprophet.php?'>";
            $file = 'index';
            break;
    }
} else {
    $issue = Issue::getIssue('NOW');
    $cause1 = [
        'status' => 'NOW',
    ];
    $cause = array_merge($cause1, $cause2);
    $liste = Item::getList($cause);
    if (!isset($_GET['category'])) {
        $liste2 = Readers::getList($cause);
        $liste3 = Contact::getList($cause);
    }
}

/**
 * Folgende IF-Abfragenüberprüfen, ob die einzelnen
 * Arrays leer sind und vereinnen diese ggf.
 */

if ($all) {
    if (count($liste) === 0) {
        if (count($liste2) === 0) {
            if (count($liste3) === 0) {
                $print .= 'In dieser Ausgabe befindet sich kein Artikel.';
                $file = 'index';
            } else {
                $liste = $liste3;
            }
        } elseif (count($liste3) === 0) {
            $liste = $liste2;
        } else {
            $liste = array_merge($liste2, $liste3);
        }
    } elseif (empty($liste2) || $liste2 === []) {
        if (isset($liste3) && $liste3 !== []) {
            $liste = array_merge($liste, $liste3);
        }
    } elseif (count($liste3) === 0) {
        $liste = array_merge($liste, $liste2);
    } else {
        $liste = array_merge($liste, $liste2, $liste3);
    }
}
$title = "$language[issueno]$issue[number] | " . $title;
$publishDate = date('d.m.Y', strtotime((string) $issue['publish_date']));
$articles = array_slice(
    $liste,
    isset($_GET['side']) ? ($_GET['side'] - 1) * $config['intervall'] : 0,
    (int) $config['intervall']
);
$articleJsonParts = [];
foreach ($articles as $number => $article) {
    $position = $number + 1;
    $articleJsonParts[] = "{
          \"@type\":\"ListItem\",
          \"position\":$position,
          \"url\":\"https://www.hp-fc.de/hpfc/board/hpfc_tagesprophet.php?$article[kind]=$article[id_number]\"
        }";
}
$meta .= "<meta name='description' content='Tagesprophet Ausgabe $issue[number] ist am $publishDate erschienen. Du findest hier kostenlos spannende Artikel aus der Welt von Harry Potter und dem Harry-Potter-Fanclub. Geschrieben von Schülern aus dem HP-FC.'>
    <script type='application/ld+json'>
    {
      \"@context\":\"http://schema.org\",
      \"@type\":\"ItemList\",
      \"itemListElement\":[" . implode(',', $articleJsonParts) . ']
    }
    </script>';
$issue_header = "<p class='number'>" . $language['issueno'] . $issue['number'] . "</p>\n";
$issue_header .= "<p class='date'>" . date('d.m.Y', strtotime((string) $issue['publish_date'])) . "</p></div>\n";
