<?php

/**
 * Leserbrief.
 *
 * Welche Leserbrief angefordert wird, wird hier überprüft
 * readers.php
 *
 * @create      on 20.04.2008
 * @modify      on 15.02.2009
 *
 * @author      Jana Pape (j.pape@dreagan-lin.de)
 *
 * @package     HPFC\Tagesprophet
 *
 * @copyright   2009 Jana Pape
 * All rights reserved.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License (GPL)
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * To read the license please visit http://www.gnu.org/copyleft/gpl.html
 */
declare(strict_types=1);

use Hpfc\DailyProphetArchive\Item;
use Hpfc\DailyProphetArchive\Readers;

/**
 * Falls eine Id für einen Leserbrief
 * übergeben worden ist,
 * wird überprüft, ob dieser Leserbrief archiviert ist,
 * bei aktuellen Leserbriefen wird in die aktuelle
 * Ausgabe umgeleitet, bei neuen Leserbriefen wird geprüft,
 * ob der User die Berechtigung hat, diesen Leserbrief zu sehen.
 */
$cause2 = [
    'del' => '0',
];
if (!$_GET['readers'] && !isset($_GET['item'])) {
    if (isset($_GET['issue'])) {
        $issue = $_GET['issue'];
    } else {
        $cause1 = match ($_GET['mod']) {
            'archiv' => [
                'status' => 'ARCHIV',
            ],
            default => [
                'status' => 'NOW',
            ],
        };
    }
    if (!$admin) {
        $cause3 = [
            'status' => 'NOT NEW',
        ];
        $cause2 = [...$cause3, ...$cause2];
    }
    $cause = $cause1 !== null ? array_merge($cause1, $cause2) : $cause2;
    $liste = Readers::getList($cause);
    $file = 'readersliste';
}

if (is_numeric($_GET['item']) && !$_GET['readers']) {
    $cause1 = [
        'item_id' => $_GET['item'],
    ];
    if (!$admin) {
        $cause1 = [
            'item_id' => $_GET['item'],
            'status' => 'NOT NEW',
        ];
    }
    $itemtitle = Item::getValue('id_number', $_GET['item'], 'title');
    $itemdate = Item::getDate($_GET['item']);
    $cause = array_merge($cause1, $cause2);
    $liste = Readers::getList($cause);
    $file = 'readersliste';
}

if (is_numeric($_GET['readers'])) {
    $status = Readers::getValue('id_number', $_GET['readers'], 'status');
    switch ($status) {
        case 'NEW':
            if ($admin) {
                $objReaders = Readers::getInstance($_GET['readers']);
                $readers = $objReaders->getData();
                $file = 'readers_view';
            } else {
                $print .= 'Der ausgewählte Lerserbrief ist nicht vorhanden';
                $print .= "<br /> Sie werden in wenigen Augenblicken zur <a href='?'>aktuellen Ausgabe</a> weitergeleitet.";
                $meta = "<meta http-equiv='Refresh' content='1; url=hpfc_tagesprophet.php'>";
                $file = 'index';
            }
            break;
        case 'NOW':
        case 'ARCHIV':
            $objReaders = Readers::getInstance($_GET['readers']);
            $readers = $objReaders->getData();
            $file = 'readers_view';
            break;
        default:
            $print = 'Der ausgewählte Leserbrief ist nicht vorhanden';
            $print .= "<br /> Sie werden in wenigen Augenblicken zur <a href='?'>aktuellen Ausgabe</a> weitergeleitet.";
            $meta = "<meta http-equiv='Refresh' content='1; url=hpfc_tagesprophet.php?'>";
            $file = 'index';
            break;
    }
} else {
    $_GET['readers'] = strtolower((string) $_GET['readers']);

    /**
     * Wenn der User eingeloggt ist, so kann er
     * einen Leserbrief schreiben. Dieser wird
     * dann in die Redaktion gestellt.
     */
    if ($wbbuserdata['username'] !== 'guest') {
        if ($_GET['readers'] === 'new' && is_numeric($_GET['item'])) {
            $file = 'readers_edit';
            $readers = null;
            if (isset($_POST['php_submit'])) {
                $readers_temp = [
                    'item_id' => $_GET['item'],
                    'title' => $_POST['title'],
                    'text' => $_POST['readerstext'],
                    'username' => $_POST['username'],
                    'user_id' => $_POST['userid'],
                ];
                $objReaders = Readers::getInstance();
                $objReaders->checkData($readers_temp);
                $readers = $objReaders->getData();
                if ($readers['text_error'] !== '' || $readers['title_error'] !== '') {
                    $_POST['php_submit'] = $language['PREVIEW'];
                }
                if ($_POST['php_submit'] === $language['PREVIEW']) {
                } elseif ($_POST['php_submit'] === $language['send']) {
                    $objReaders->insertData();
                    $print .= 'Der Leserbrief wurde erfolgreich an die Redaktion &uuml;bermittelt.<br />';
                    $print .= 'Sobald der Brief ver&ouml;ffentlicht wird, wirst du benachrichtigt werden.<br />';
                    $file = 'index';
                }
            }
        }
    } elseif ($_GET['readers'] === 'new' && is_numeric($_GET['item'])) {
        access_error();
    }
}
