<?php

/**
 * Kontakte verarbeiten.
 *
 * @author      Jana Pape (j.pape@dreagan-lin.de)
 *
 * @package    HPFC\Tagesprophet
 *
 * @copyright   2009 Jana Pape
 * All rights reserved.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License (GPL)
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * To read the license please visit http://www.gnu.org/copyleft/gpl.html
 * Definition von Ausgabefunktionen
 */
declare(strict_types=1);

namespace Hpfc\DailyProphetArchive;

use Hpfc\BoardInterface\EmailFactory;
use Hpfc\Wbb21Shims\WbbDatabase;
use Psr\Container\ContainerInterface;

/**
 * Kontakte verarbeiten.
 */
class Contact
{
    public mixed $contact;

    public static $instance;

    protected function __construct($contact_id = null)
    {
        $this->contact['id_number'] = $contact_id;
        $this->loadData();
    }

    public static function getInstance($conact_id = null): self|null
    {
        if (self::$instance === null) {
            self::$instance = new self($conact_id);
        }

        return self::$instance;
    }

    public static function getValue($key, $value, $back)
    {
        /** @var WbbDatabase $db */
        global $db;
        $sql = 'select ' . $back . ' from ' . TCONTACT . ' where ' . $key . " like '" . $value . "';";
        $return = $db->query_first($sql);

        return $return[$back];
    }

    public static function uncaseIssue($contact_id): void
    {
        /** @var WbbDatabase $db */
        global $db;
        $sql = 'update ' . TCONTACT . " set id_issue='0' where id_number like '" . $contact_id . "'";
        $db->query($sql);
    }

    /**
     * Email wird an Ersteller der Kontaktanzeige übermittelt.
     *
     * @param <type> $id_issue
     */
    public static function sendEmail($id_issue, $kind = 'publish'): void
    {
        /** @var ContainerInterface $container */
        global $container;
        $cause = [
            'id_issue' => $id_issue,
        ];
        $contacts = SELF::getList($cause);
        $number = Issue::getValue('id_number', $id_issue, 'number');
        foreach ($contacts as $contact) {
            if ($contact['email'] !== '') {
                if ($kind === 'publish') {
                    $contact['subject'] = 'Veröffentlichung deiner Kontaktanzeige in Ausgabe Nr.' . $number;
                    $contact['text'] = 'Hallo ' . $contact['username'] . ",\n\nDeine Konatkanzeige wurde in der Ausgabe Nr." . $number . " veröffentlicht.\n\nMfG \ndas Redaktionsteam des Tagespropheten";
                } else {
                    $contact['subject'] = 'Abweisung deiner Kontaktanzeige';
                    $contact['text'] = 'Hallo ' . $contact['username'] . ",\n\nDeine Konatkanzeige kann leider nicht" . " veröffentlicht werden.\n\nMfG \ndas Redaktionsteam des Tagespropheten";
                }
                $emailFactory = $container->get(EmailFactory::class);
                $email = $emailFactory->create();
                $email->addRecipient($contact['email'], $contact['username']);
                $email->setSubject($contact['subject']);
                $email->setTextContent($contact['text']);
                $email->send();
            }
        }
    }

    public static function getList(array $cause = [], array $sort = [], $desc = null): array|null
    {
        /** @var WbbDatabase $db */
        global $db, $icounter;
        $condition = null;
        $order = null;
        $return = null;
        if ($desc !== null) {
            $desc = ' DESC';
        }
        if ($cause !== []) {
            foreach ($cause as $key => $value) {
                if ($condition !== null) {
                    $condition .= ' AND ';
                }
                $split = explode(' ', strval($value));
                if ($split[0] === 'NOT') {
                    $condition .= ' ' . $key . " not like '" . $split[1] . "' ";
                } else {
                    $condition .= ' ' . $key . " like '" . $value . "' ";
                }
            }
            $condition = ' where ' . $condition;
        }
        if ($sort !== []) {
            if ($order !== null) {
                $order .= ', ';
            }
            foreach ($sort as $value) {
                $order .= ' ' . $value;
            }
            $order = ' order by ' . $order . $desc;
        } else {
            $order = ' order by insert_date';
        }
        $sql = 'select * from ' . TCONTACT . $condition . $order . ';';
        $erg = $db->query($sql);
        $i = $icounter;
        while ($result = $db->fetch_array($erg)) {
            $return[$i] = $result;
            $array1 = [
                '&ndash;' => '-',
                '&nbsp;' => ' ',
                '&auml;' => 'ä',
                '&uuml;' => 'ü',
                '&ouml;' => 'ö',
                '&Auml;' => 'Ä',
                '&Uuml;' => 'Ü',
                '&Ouml;' => 'Ö',
                '&szlig;' => 'ß',
                '<br/>' => ' ',
                '<br>' => ' ',
                '<BR>' => ' ',
                '<br />' => ' ',
            ];
            $text2 = '';
            $text = $result['text'];
            $text = strtr($text, $array1);
            $text = strip_tags($text);
            $text = str_word_count($text, 1, 'äöüß,.1234567890');
            for ($J = 0; $J < 50; $J++) {
                $text2 .= $text[$J] . ' ';
            }
            $return[$i]['text'] = $text2;
            $return[$i]['issue_number'] = Issue::getValue('id_number', $return[$i]['id_issue'], 'number');
            $return[$i]['kind'] = 'contact';
            $i++;
        }

        return $return;
    }

    public static function delete(): void
    {
        /** @var WbbDatabase $db */
        global $db;
        $sql = 'delete from ' . TCONTACT . " where del='1'";
        $db->query($sql);
    }

    public function saveData(): void
    {
        /** @var WbbDatabase $db */
        global $db;
        $set = ' set ';
        $set .= " text='" . mysql_real_escape_string($this->contact['text']) . "',";
        $set .= " title='" . mysql_real_escape_string($this->contact['title']) . "' ";
        $sql = 'update ' . TCONTACT . $set . "where id_number like '" . $this->contact['id_number'] . "';";
        $db->query($sql);
    }

    public function insertData(): void
    {
        /** @var WbbDatabase $db */
        global $db;
        $keys = null;
        $values = null;
        $keys = ' (user_id, username, status,title, text, email, insert_date) ';
        $values = " ('" . $this->contact['userid'] . "', '" . $this->contact['username'] . "','NEW','" . mysql_real_escape_string(
            $this->contact['title']
        ) . "','" . mysql_real_escape_string(
            $this->contact['text']
        ) . "','" . $this->contact['email'] . "',curdate()) ";
        $sql = 'insert into ' . TCONTACT . $keys . ' VALUES ' . $values . ';';
        $db->query($sql);
    }

    public function checkData($contact): void
    {
        global $config;
        $checkcontact = [];
        $checkcontact['id_number'] = $contact['id_number'];
        $checkcontact['username'] = $contact['username'];
        if (strlen(htmlentities((string) $contact['title'])) > 255) {
            $contact['title_error'] = "<p class='error'>Der Titel ist zu lang.</p>";
            $checkcontact['title'] = substr(htmlentities((string) $contact['text'], ENT_QUOTES), 0, 255);
        } else {
            $checkcontact['title'] = htmlentities((string) $contact['title'], ENT_QUOTES);
        }
        if (strlen(htmlentities((string) $contact['text'])) > $config['contact_long']) {
            $checkcontact['text_error'] = "<p class='error'>Der Text ist zu lang und wurde nach " . $config['contact_long'] . ' Zeichen abgeschnitten.</p>';
            $checkcontact['text'] = substr(htmlentities((string) $contact['text'], ENT_QUOTES), 0, 2056);
        } else {
            $checkcontact['text'] = htmlentities((string) $contact['text'], ENT_QUOTES);
        }
        if (!$this->emailcheck(($contact['email']))) {
            $contact['email_error'] = "<p class='error'>Die Emailadresse ist nicht korrekt.</p>";
        } else {
            $checkcontact['email'] = $contact['email'];
        }
        $checkcontact['userid'] = $contact['userid'];
        $this->putData($checkcontact);
    }

    public function getData()
    {
        return $this->contact;
    }

    public static function insertIssue($contact_id, $kind = 'NEW'): void
    {
        /** @var WbbDatabase $db */
        global $db;
        $set = null;
        $kind = strtoupper((string) $kind);
        $issue = Issue::getValue('status', 'NEW', 'id_number');
        if ($kind !== 'NEW') {
            $issue = Issue::getValue('status', 'NOW', 'id_number');
            $set = ", status='NOW' ";
        }
        $sql = 'update ' . TCONTACT . " set id_issue='" . $issue . "' " . $set . "where id_number like '" . $contact_id . "'";
        $db->query($sql);
    }

    public function delData(): void
    {
        /** @var WbbDatabase $db */
        global $db;
        $sql = 'update ' . TCONTACT . " set del='1' where id_number like '" . $this->contact['id_number'] . "'";
        $db->query($sql);
    }

    public function undelete($id_number): void
    {
        /** @var WbbDatabase $db */
        global $db;
        $sql = 'update ' . TCONTACT . " set del='0', id_issue='0', status='NEW' where id_number like '" . $id_number . "'";
        $db->query($sql);
    }

    protected function loadData(): void
    {
        /** @var WbbDatabase $db */
        global $db;
        $sql = 'select * from ' . TCONTACT . " where id_number like '" . $this->contact['id_number'] . "';";
        $erg = $db->query_first($sql);
        $this->putData($erg);
    }

    protected function putData($contact): void
    {
        $this->contact = $contact;
    }

    private function emailcheck($mail)
    {
        $muster = '/^[^@\s]+@([-a-z0-9]+\.)+[a-z]{2,}$/i';
        $erg = preg_match($muster, (string) $mail);
        switch ($erg) {
            case 0:
                return false;
            case 1:
                return true;
            case false:
                echo 'Fehler bei Funktion preg_match()!';

                return false;
        }
    }
}
