<?php

declare(strict_types=1);

namespace Hpfc\DailyProphetArchive;

class DebuggerVoid implements Debugger
{
    private static \Hpfc\DailyProphetArchive\DebuggerVoid|null $instance = null;

    public static function getInstance(): self
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    public function debug($message): void
    {
        //Alle Meldungen einfach ignorieren.
    }
}
