<?php
/**
 * Grundlagen für die Klassen des Projektes werden
 * definiert.
 *
 * @author      Jana Pape (j.pape@dreagan-lin.de)
 *
 * @package    HPFC\Tagesprophet
 *
 * @copyright   2009 Jana Pape
 * All rights reserved.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License (GPL)
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * To read the license please visit http://www.gnu.org/copyleft/gpl.html
 * Definition von Ausgabefunktionen
 */
declare(strict_types=1);

namespace Hpfc\DailyProphetArchive;

use Hpfc\Wbb21Shims\WbbDatabase;

/**
 * Configurationsklasse.
 *
 * Es werden Konfigurationsvariablen
 * aus der Datenbank geladen
 */
class Config
{
    /**
     * Configuration wird aus Datenbank in das klasseneigne Array geladen.
     *
     * @return array<int|string, mixed>
     */
    public static function loadData(): array
    {
        /** @var WbbDatabase $db */
        global $db;
        $config = [];
        $sql = 'select * from ' . TCONFIG . ';';
        $erg = $db->query($sql);
        while ($result = $db->fetch_array($erg)) {
            $config[$result['name']] = $result['value'];
        }

        return $config;
    }

    /** Configuration wird in der Datenbank gespeichert. */
    public static function saveConfig(array $config): void
    {
        /** @var WbbDatabase $db */
        global $db;
        foreach ($config as $key => $value) {
            $sql = 'replace into ' . TCONFIG . " (name, value) VALUES('$key', '$value')";
            $db->query($sql);
        }
    }
}
