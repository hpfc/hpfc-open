<?php

declare(strict_types=1);

namespace Hpfc\DailyProphetArchive;

use Exception;

class UnknownDebuggerException extends Exception
{
}
