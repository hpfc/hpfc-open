<?php

/**
 * Leserbriefe verarbeiten.
 *
 * @author      Jana Pape (j.pape@dreagan-lin.de)
 *
 * @package    HPFC\Tagesprophet
 *
 * @copyright   2009 Jana Pape
 * All rights reserved.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License (GPL)
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * To read the license please visit http://www.gnu.org/copyleft/gpl.html
 * Definition von Ausgabefunktionen
 */
declare(strict_types=1);

namespace Hpfc\DailyProphetArchive;

use Hpfc\BoardInterface\EmailFactory;
use Hpfc\Wbb21Shims\WbbDatabase;
use Psr\Container\ContainerInterface;

/**
 * Leserbriefe verarbeiten.
 */
class Readers
{
    public mixed $readers;

    public static $instance;

    protected function __construct($readers_id = null)
    {
        $this->readers['id_number'] = $readers_id;
        $this->loadData();
    }

    public static function getInstance($readers_id = null): self|null
    {
        if (self::$instance === null) {
            self::$instance = new self($readers_id);
        }

        return self::$instance;
    }

    public static function sendEmail($id_issue, $kind = 'publish'): void
    {
        /** @var ContainerInterface $container */
        global $container;
        $cause = [
            'id_issue' => $id_issue,
        ];
        $readers = Self::getList($cause);
        $number = Issue::getValue('id_number', $id_issue, 'number');
        foreach ($readers as $reader) {
            if ($reader['email'] !== '') {
                if ($kind === 'publish') {
                    $reader['subject'] = 'Veröffentlichung deines Leserbriefes in Ausgabe Nr.' . $number;
                    $reader['text'] = 'Hallo ' . $reader['username'] . ",\n\nDein Leserbrief zu dem Artikel " . $reader['item'] . ' wurde in der Ausgabe Nr.' . $number . " veröffentlicht.\n\nMfG \ndas Redaktionsteam des Tagespropheten";
                } else {
                    $reader['subject'] = 'Nichveröffentlichung deiner Kontaktanzeige ';
                    $contact['text'] = 'Hallo ' . $reader['username'] . ",\n\nDein Leserbrief wird nicht" . " veröffentlicht werden.\n\nMfG \ndas Redaktionsteam des Tagespropheten";
                }
                $emailFactory = $container->get(EmailFactory::class);
                $email = $emailFactory->create();
                $email->addRecipient($contact['email'], $contact['username']);
                $email->setSubject($contact['subject']);
                $email->setTextContent($contact['text']);
                $email->send();
            }
        }
    }

    public static function getList($cause = [], $sort = [], $desc = 'ASC'): array|null
    {
        /** @var WbbDatabase $db */
        global $db, $icounter;
        $condition = null;
        $order = null;
        $return = null;
        if ($desc !== 'ASC') {
            $desc = ' DESC';
        }
        if ($cause !== []) {
            foreach ($cause as $key => $value) {
                if ($condition !== null) {
                    $condition .= ' AND ';
                }
                $split = explode(' ', strval($value));
                if ($split[0] === 'NOT') {
                    $condition .= ' ' . $key . " not like '" . $split[1] . "' ";
                } else {
                    $condition .= ' ' . $key . " like '" . $value . "' ";
                }
            }
            $condition = ' where ' . $condition;
        }
        if ($sort !== []) {
            foreach ($sort as $value) {
                $order .= ' ' . $value;
            }
            $order = ' order by ' . $order . $desc;
        } else {
            $order = ' order by insert_date';
        }
        $sql = 'select * from ' . TREADERS . $condition . $order . ';';
        $erg = $db->query($sql);
        $i = $icounter;
        while ($result = $db->fetch_array($erg)) {
            $return[$i] = $result;
            $array1 = [
                '&auml;' => 'ä',
                '&uuml;' => 'ü',
                '&ouml;' => 'o',
                '&Auml;' => 'Ä',
                '&Uuml;' => 'Ü',
                '&Ouml;' => 'Ö',
                '&szlig;' => 'ß',
                '<br/>' => ' ',
                '<br>' => ' ',
                '<BR>' => ' ',
                '<br />' => ' ',
            ];
            $text2 = '';
            $text = $result['text'];
            $text = strtr($text, $array1);
            $text = strip_tags($text);
            $text = str_word_count($text, 1, 'äüöß,.1234567890');
            for ($J = 0; $J < 50; $J++) {
                $text2 .= ($text[$J] ?? '') . ' ';
            }
            $return[$i]['text'] = $text2;
            $return[$i]['issue_number'] = Issue::getValue('id_number', $return[$i]['id_issue'], 'number');
            $return[$i]['item'] = Item::getValue('id_number', $return[$i]['item_id'], 'title');
            $return[$i]['kind'] = 'readers';
            $i++;
        }
        $icounter = $i;

        return $return;
    }

    public static function getValue($key, $value, $back)
    {
        /** @var WbbDatabase $db */
        global $db;
        $sql = 'select ' . $back . ' from ' . TREADERS . ' where ' . $key . " like '" . $value . "';";
        $return = $db->query_first($sql);

        return $return[$back];
    }

    public static function uncaseIssue($readers_id): void
    {
        /** @var WbbDatabase $db */
        global $db;
        $sql = 'update ' . TREADERS . " set id_issue='0' where id_number like '" . $readers_id . "'";
        $db->query($sql);
    }

    public static function delete(): void
    {
        /** @var WbbDatabase $db */
        global $db;
        $sql = 'delete from ' . TREADERS . " where del ='1'";
        $db->query($sql);
    }

    public function saveData(): void
    {
        /** @var WbbDatabase $db */
        global $db;
        $set = null;
        $set .= " text='" . mysql_real_escape_string($this->readers['text']) . "'";
        $set .= ", title='" . mysql_real_escape_string($this->readers['title']) . "'";
        $sql = 'update ' . TREADERS . ' set ' . $set . " where id_number like '" . $this->readers['id_number'] . "';";
        $db->query($sql);
    }

    public function insertData(): void
    {
        /** @var WbbDatabase $db */
        global $db;
        $keys = null;
        $values = null;
        $keys = ' (user_id, username, status,title, text, email,item_id,insert_date) ';
        $values = " ('" . $this->readers['user_id'] . "', '" . $this->readers['username'] . "','NEW','" . mysql_real_escape_string(
            $this->readers['title']
        ) . "','" . mysql_real_escape_string(
            $this->readers['text']
        ) . "','" . $this->readers['email'] . "','" . $this->readers['item_id'] . "',curdate()) ";
        $sql = 'insert into ' . TREADERS . $keys . ' VALUES ' . $values . ';';
        $db->query($sql);
    }

    public function checkData($readers): void
    {
        $checkreaders = [];
        global $config;
        $checkreaders['text']['error'] = null;
        $checkreaders['username'] = $readers['username'];
        if (strlen(ltrim(rtrim(htmlentities((string) $readers['title'])))) > 255) {
            $readers['title_error'] = "<p class='error'>Der Titel ist zu lang.</p>";
            $checkreaders['title'] = substr(htmlentities((string) $readers['text'], ENT_QUOTES), 0, 255);
        } else {
            $checkreaders['title'] = htmlentities((string) $readers['title'], ENT_QUOTES);
        }
        if (strlen(ltrim(htmlentities((string) $readers['text']))) > $config['readers_long']) {
            $checkreaders['text_error'] = "<p class='error'>Der Text ist zu lang und wurde nach " . $config['readers_long'] . ' Zeichen abgeschnitten.</p>';
            $checkreaders['text'] = substr(
                ltrim(rtrim(htmlentities((string) $readers['text'], ENT_QUOTES))),
                0,
                $config['readers_long']
            );
        } else {
            $checkreaders['text'] = nl2br(ltrim(rtrim(htmlentities((string) $readers['text'], ENT_QUOTES))));
        }
        if (!$this->emailcheck(($readers['email']))) {
            $readers['email_error'] = "<p class='error'>Die Emailadresse ist nicht korrekt.</p>";
        } else {
            $checkreaders['email'] = $readers['email'];
        }
        $checkreaders['user_id'] = $readers['user_id'];
        $checkreaders['id_number'] = $readers['id_number'];
        $checkreaders['item'] = Item::getValue('id_number', $readers['item_id'], 'title');
        $checkreaders['item_id'] = $readers['item_id'];
        $this->putData($checkreaders);
    }

    public function delData(): void
    {
        /** @var WbbDatabase $db */
        global $db;
        $sql = 'update ' . TREADERS . " set del='1' where id_number like '" . $this->readers['id_number'] . "'";
        $db->query($sql);
    }

    public function getData()
    {
        return $this->readers;
    }

    public static function insertIssue($readers_id, $kind = 'NEW'): void
    {
        /** @var WbbDatabase $db */
        global $db;
        $set = null;
        $kind = strtoupper((string) $kind);
        $issue = Issue::getValue('status', 'NEW', 'id_number');
        if ($kind !== 'NEW') {
            $issue = Issue::getValue('status', 'NOW', 'id_number');
            $set = ", status='NOW' ";
        }
        $sql = 'update ' . TREADERS . " set id_issue='" . $issue . "' " . $set . "where id_number like '" . $readers_id . "'";
        $db->query($sql);
    }

    public function undelete($id_number): void
    {
        /** @var WbbDatabase $db */
        global $db;
        $sql = 'update ' . TREADERS . " set del='0', id_issue='0', status='NEW' where id_number like '" . $id_number . "'";
        $db->query($sql);
    }

    protected function loadData(): void
    {
        /** @var WbbDatabase $db */
        global $db;
        $sql = 'select * from ' . TREADERS . " where id_number like '" . $this->readers['id_number'] . "';";
        $erg = $db->query_first($sql);
        $this->putData($erg);
    }

    protected function putData($readers): void
    {
        $readers['issue_number'] = Issue::getValue('id_number', $readers['id_issue'], 'number');
        $readers['item'] = Item::getValue('id_number', $readers['item_id'], 'title');
        $readers['kind'] = 'readers';
        $this->readers = $readers;
    }

    private function emailcheck($mail)
    {
        $muster = '/^[^@\s]+@([-a-z0-9]+\.)+[a-z]{2,}$/i';
        $erg = preg_match($muster, (string) $mail);
        switch ($erg) {
            case 0:
                return false;
            case 1:
                return true;
            case false:
                echo 'Fehler bei Funktion preg_match()!';

                return false;
        }
    }
}
