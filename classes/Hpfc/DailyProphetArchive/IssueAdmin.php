<?php

/**
 * Definition von Ausgabefunktionen.
 *
 * Eine Ausgabe wird erstellt, bearbeitet, veröffentlicht.
 *
 * @author      Jana Pape (j.pape@dreagan-lin.de)
 *
 * @package    HPFC\Tagesprophet
 *
 * @copyright   2009 Jana Pape
 * All rights reserved.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License (GPL)
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * To read the license please visit http://www.gnu.org/copyleft/gpl.html
 * Definition von Ausgabefunktionen
 */
declare(strict_types=1);

namespace Hpfc\DailyProphetArchive;

use Hpfc\Wbb21Shims\WbbDatabase;

/**
 * Definition von Ausgabefunktionen.
 */
class IssueAdmin extends Issue
{
    public array $issue;

    public array $error;

    /** Erzeugt eine Instance der Klasse IssueAdmin */
    public static function getInstance(int|null $id_number = null): Guide
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * Veröffentlichung der Vorbereiteten Ausgabe.
     *
     * Die Artikel, Leserbriefe, Konaktanzeigen, die sich noch in der aktuell veröffentlichten
     * Ausgabe befinden, werden ins Archiv verschoben und die, die in der vorbereiteten Ausgabe stehen werden veröffentlicht.
     * Desweiteren werden, wenn definiert, Mails an die User geschrieben, deren Kontaktanzeigen veröffentlicht werden bzw. Leserbriefe.
     */
    public static function publishData(int $id_issue): void
    {
        /** @var WbbDatabase $db */
        global $db;
        $sql = 'update ' . TISSUE . " set status='ARCHIV'  where status like 'NOW';";
        $db->query($sql);
        $sql = 'update ' . TITEM . " set status='ARCHIV' where status like 'NOW';";
        $db->query($sql);
        $sql = 'update ' . TCONTACT . " set status='ARCHIV' where status like 'NOW';";
        $db->query($sql);
        $sql = 'update ' . TREADERS . " set status='ARCHIV' where status like 'NOW';";
        $db->query($sql);
        $sql = 'update ' . TISSUE . " set status='NOW', publish_date=curdate() where id_number like '" . $id_issue . "';";
        $db->query($sql);
        $sql = 'update ' . TITEM . " set status='NOW' where id_issue like '" . $id_issue . "';";
        $db->query($sql);
        $sql = 'update ' . TCONTACT . " set status='NOW' where id_issue like '" . $id_issue . "';";
        $db->query($sql);
        $sql = 'update ' . TREADERS . " set status='NOW' where id_issue like '" . $id_issue . "';";
        $db->query($sql);
    }

    /**
     * Rückholung der veröffentlichten Ausgabe.
     *
     * Die Artikel, Leserbriefe, Konaktanzeigen, die
     * sich in der aktuell veröffentlichten
     * Ausgabe befinden, werden in Redaktion verschoben
     * und die, die sich in der letzten Archivierten
     * Ausgabe stehen werden als aktuell veröffentlicht.
     * Allerdings nur, wenn sich keine Ausgabe in
     * der Vorbereitung befindet.
     */
    public static function unpublishData(): void
    {
        /** @var WbbDatabase $db */
        global $db;
        $sql = 'select * from ' . TISSUE . " where status like 'NEW';";
        $issue_new = $db->query_first($sql);
        if ($issue_new['id_number'] === 0) {
            $sql = 'update ' . TISSUE . " set status='NEW' where status like 'NOW';";
            $db->query($sql);
            $sql = 'update ' . TITEM . " set status='NEW' where status like 'NOW';";
            $db->query($sql);
            $sql = 'update ' . TREADERS . " set status='NEW' where status like 'NOW';";
            $db->query($sql);
            $sql = 'update ' . TCONTACT . " set status='NEW' where status like 'NOW';";
            $db->query($sql);
            $sql = 'select * from ' . TISSUE . " where status like 'ARCHIV' order by publish_date desc;";
            $issue_archiv = $db->query_first($sql);
            $sql = 'update ' . TISSUE . " set status='NOW' where id_number like '" . $issue_archiv['id_number'] . "';";
            $db->query($sql);
            $sql = 'update ' . TITEM . " set status='NOW' where id_issue like '" . $issue_archiv['id_number'] . "';";
            $db->query($sql);
            $sql = 'update ' . TREADERS . " set status='NOW' where id_issue like '" . $issue_archiv['id_number'] . "';";
            $db->query($sql);
            $sql = 'update ' . TCONTACT . " set status='NOW' where id_issue like '" . $issue_archiv['id_number'] . "';";
            $db->query($sql);
        }
    }

    public static function delete(): void
    {
        /** @var WbbDatabase $db */
        global $db;
        $sql = 'delete from ' . TISSUE . " where del='1'";
        $db->query($sql);
    }

    /** Array wird in der Datenbank gespeichert */
    public function saveData()
    {
        /** @var WbbDatabase $db */
        global $db;
        if ($this->issue['id_number'] !== 0 && $this->issue['id_number'] !== null) {
            $set = " set number='" . $this->issue['number'] . "',";
            $set .= " text='" . $this->issue['text'] . "'";
            $sql = 'update ' . TISSUE . $set . "where id_number like '" . $this->issue['id_number'] . "';";
            $db->query($sql);
            if ($db->affected_rows === 0) {
                $this->error['SAVE'] = true;
            }
        } else {
            $sql = 'select count(*) from ' . TISSUE . " where status like 'NEW';";
            $erg = $db->query_first($sql);
            if ($erg['count'] === 0) {
                $this->insertData();
            } else {
                return 'Es existiert schon eine Ausgabe in Vorbereitung';
            }
        }
    }

    /** Daten, die übergeben werden, werden aus Kausalität überprüft */
    public function checkData(array $issue): void
    {
        $issuecheck = [];
        if (is_string($issue['title'])) {
            $issuecheck['title'] = htmlentities(strip_tags($issue['title']), ENT_QUOTES);
        }
        $issuecheck['text'] = htmlentities((string) $issue['text'], ENT_QUOTES);
        if (is_numeric($issue['number'])) {
            $issuecheck['number'] = $issue['number'];
        }
        $issuecheck['id_number'] = $this->issue['id_number'];
        $this->putData($issuecheck);
    }

    /**
     * Daten wird in klasseneignes Array eingefügt.
     *
     * @param mixed[] $issue
     */
    public function putData(array $issue): void
    {
        $this->issue = $issue;
    }

    public function delData($kind): void
    {
        /** @var WbbDatabase $db */
        global $db;
        if ($kind === 'issue') {
            $sql = 'update ' . TITEM . " set status='NEW', id_issue='' where id_issue='" . $this->issue['id_number'] . "'";
            $db->query($sql);
            $sql = 'update ' . TREADERS . " set status='NEW',id_issue='' where id_issue='" . $this->issue['id_number'] . "'";
            $db->query($sql);
            $sql = 'update ' . TCONTACT . " set status='NEW',id_issue='' where id_issue='" . $this->issue['id_number'] . "'";
            $db->query($sql);
        } else {
            $sql = 'update ' . TITEM . " set del='1' where id_issue='" . $this->issue['id_number'] . "'";
            $db->query($sql);
            $sql = 'update ' . TREADERS . " set del='1' where id_issue='" . $this->issue['id_number'] . "'";
            $db->query($sql);
            $sql = 'update ' . TCONTACT . " set del='1' where id_issue='" . $this->issue['id_number'] . "'";
            $db->query($sql);
        }
        $sql = 'update ' . TISSUE . " set del='1' where id_number='" . $this->issue['id_number'] . "'";
        $db->query($sql);
    }

    public function undelete($id_number): void
    {
        /** @var WbbDatabase $db */
        global $db;
        $sql = 'update ' . TISSUE . " set del='0' where id_number like '" . $id_number . "'";
        $db->query($sql);
        $sql = 'update ' . TITEM . " set del='0' where id_issue like '" . $id_number . "'";
        $db->query($sql);
        $sql = 'update ' . TCONTACT . " set del='0'  where id_issue like '" . $id_number . "'";
        $db->query($sql);
        $sql = 'update ' . TREADERS . " set del='0'  where id_issue like '" . $id_number . "'";
        $db->query($sql);
    }

    /** Array wird in die Datenbank eingefügt */
    protected function insertData(): void
    {
        /** @var WbbDatabase $db */
        global $db;
        $keys = null;
        $values = null;
        $keys = ' (number,text,status, insert_date) ';
        $values = " ('" . $this->issue['number'] . "', '" . $this->issue['text'] . "','NEW',curdate()) ";
        $sql = 'insert into ' . TISSUE . $keys . ' VALUES ' . $values . ';';
        $db->query($sql);
        if ($db->affected_rows === 0) {
            $this->error['CREATEISSUE']['ISSUE'] = true;
        }
        $this->issue['id_number'] = $db->insert_id();
    }
}
