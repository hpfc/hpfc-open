<?php

declare(strict_types=1);

namespace Hpfc\DailyProphetArchive;

interface Debugger
{
    public function debug($message): void;
}
