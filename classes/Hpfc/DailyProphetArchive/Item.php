<?php
/**
 * Definition von Artikelfunktionen.
 *
 * Eine Artikel wird erstellt, bearbeitet, veröffentlicht.
 *
 * @author      Jana Pape (j.pape@dreagan-lin.de)
 *
 * @package    HPFC\Tagesprophet
 *
 * @copyright   2009 Jana Pape
 * All rights reserved.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License (GPL)
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * To read the license please visit http://www.gnu.org/copyleft/gpl.html
 */
declare(strict_types=1);

namespace Hpfc\DailyProphetArchive;

use Hpfc\Wbb21Shims\WbbDatabase;

/**
 * Definition von Artikelfunktionen.
 */
class Item extends Guide
{
    /** @var array<string, boolean> */
    public array $error = [];

    protected array $item = [];

    protected function __construct($id_number = null)
    {
        $this->item['id_number'] = $id_number;
        $this->loadData();
    }

    /** Item wird aus Datenbank in das klasseneigne Array geladen */
    public function loadData(): void
    {
        /** @var WbbDatabase $db */
        global $db;
        if (!$this->item['id_number']) {
            return;
        }

        $sql = 'select * from ' . TITEM . " where id_number = {$this->item['id_number']}";
        $this->item = $db->query_first($sql);
    }

    /**
     * Erzeugt ein neues Objekt der Klasse Item,
     * wenn noch kein Objekt mit der übergebenen $id_number vorhanden ist.
     * Ansonsten wird die Referenz des vorhandenen Objektes übergeben.
     */
    public static function getInstance(int $id_number = null): self
    {
        if (self::$instance === null) {
            self::$instance = new self($id_number);
        }

        return self::$instance;
    }

    public static function getList(array $cause = [], array $sort = [], $desc = null): array
    {
        /** @var WbbDatabase $db */
        global $db, $icounter;
        $condition = null;
        $order = null;
        $return = null;
        if ($desc !== null) {
            $desc = ' DESC';
        }
        foreach ($cause as $key => $value) {
            if ($condition !== null) {
                $condition .= ' AND ';
            }
            $split = explode(' ', strval($value));
            if ($split[0] === 'NOT') {
                $condition .= ' ' . $key . " not like '" . $split[1] . "' ";
            } else {
                $condition .= ' ' . $key . " like '" . $value . "' ";
            }
        }
        $condition = ' where ' . $condition;
        if ($sort !== []) {
            foreach ($sort as $value) {
                $order .= ' ' . $value;
            }
            $order = ' order by ' . $order . $desc;
        } else {
            $order = ' order by insert_date desc';
        }
        $sql = 'select * from ' . TITEM . $condition . $order . ';';
        $erg = $db->query($sql);
        $i = $icounter;
        while ($result = $db->fetch_array($erg)) {
            $return[$i] = $result;
            $array1 = [
                '&ndash;' => '-',
                '&nbsp;' => ' ',
                '&auml;' => 'ä',
                '&uuml;' => 'ü',
                '&ouml;' => 'ö',
                '&Auml;' => 'Ä',
                '&Uuml;' => 'Ü',
                '&Ouml;' => 'Ö',
                '&szlig;' => 'ß',
                '<br/>' => ' ',
                '<br>' => ' ',
                '<BR>' => ' ',
                '<br />' => ' ',
            ];
            $text2 = '';
            $text = $result['text'];
            $text = strtr($text, $array1);
            $text = strip_tags($text);
            $text = str_word_count($text, 1, 'äöüß,.1234567890');
            for ($J = 0; $J < 50; $J++) {
                $text2 .= ($text[$J] ?? '') . ' ';
            }
            $return[$i]['text'] = $text2;
            $return[$i]['issue_number'] = Issue::getValue('id_number', $return[$i]['id_issue'], 'number');
            $return[$i]['category_name'] = Category::getName($return[$i]['category']);
            $return[$i]['user'] = User::getUser($return[$i]['id_number']);
            $return[$i]['haus'] = $return[$i]['user'][0]['haus'];
            $return[$i]['kind'] = 'item';
            $i++;
        }
        $icounter = $i;

        return $return;
    }

    public static function uncaseIssue($item_id): void
    {
        /** @var WbbDatabase $db */
        global $db;
        $sql = 'update ' . TITEM . " set id_issue = 0 where id_number = $item_id";
        $db->query($sql);
    }

    public static function correctStatus($id_number): int
    {
        /** @var WbbDatabase $db */
        global $db;
        $sql = 'select count(*) as anzahl from ' . TITEM . " where id_issue = $id_number and sign = '0'";
        $erg = $db->query_first($sql);

        return $erg['anzahl'];
    }

    public static function getDate($id_number): string
    {
        $issue = self::getValue('id_number', $id_number, 'id_issue');

        return Issue::getValue('id_number', $issue, 'publish_date');
    }

    public static function getValue($key, $value, $back)
    {
        /** @var WbbDatabase $db */
        global $db;
        $sql = "select $back from " . TITEM . " where $key like '$value'";
        $return = $db->query_first($sql);

        return $return[$back];
    }

    public static function delete(): void
    {
        /** @var WbbDatabase $db */
        global $db;
        $sql = 'delete from ' . TITEM . ' where del = 1';
        $db->query($sql);
    }

    /** Array wird in der Datenbank gespeichert */
    public function saveData(): int
    {
        /** @var WbbDatabase $db */
        global $db;
        $item = $this->clearData();
        if ($item['new'] !== 'new') {
            $item['update_date'] = date('Y-m-d');
            $set = ' set ';
            $set .= "title ='" . $item['title'] . "', "; //Titel
            $set .= "text ='" . addslashes((string) $item['text']) . "', "; //Text
            if ($item['correct'] !== null) {
                $set .= "correct ='" . $item['correct'] . "', ";
            } //Erstkorrektur
            $set .= "sign='" . $this->item['sign'] . "',"; //Endkorrektur
            $set .= "points='" . $this->item['points'] . "',"; //Bepunktung
            $set .= "notice='" . $this->item['notice'] . "',"; //Notizen
            $set .= "spelling='" . $this->item['spelling'] . "',"; //Rechtschreibung
            $set .= "term='" . $this->item['term'] . "',"; //Ausdruck
            $set .= "grammar='" . $this->item['grammar'] . "',"; //Grammatik
            $set .= "category='" . $this->item['category'] . "',"; //Kategorie
            $set .= "update_date ='" . $item['update_date'] . "' "; //letzte Änderung
            $sql = 'update ' . TITEM . $set . "where id_number like '" . $this->item['id_number'] . "';";
            $db->query($sql);
            if ($db->affected_rows === 0) {
                $this->error['SAVE'] = true;
            }

            return $item['id_number'];
        }

        return $this->insertData($item);
    }

    /** @return mixed[] */
    public function getData(): array
    {
        $item = $this->item;
        $item['category_name'] = Category::getName($this->item['category']);
        $item['issue_number'] = Issue::getValue('id_number', $this->item['id_issue'], 'number');
        $item['issue_date'] = Issue::getValue('id_number', $this->item['id_issue'], 'publish_date');
        $item['user'] = User::getUser($this->item['id_number']);

        return $item;
    }

    /** Daten, die übergeben werden, werden aus Kausalität überprüft */
    public function checkData(array $item): void
    {
        $itemcheck = [];
        if ($item['title'] !== null) {
            if (is_string($item['title'])) {
                $itemcheck['title'] = htmlentities(strip_tags($item['title']), ENT_QUOTES);
            }
        } else {
            $this->error['title'] = 'Es wurde kein Titel eingegeben.';
        }
        if ($item['summary'] !== null) {
            if (is_string($item['summary'])) {
                $itemcheck['summary'] = htmlentities(nl2br(strip_tags($item['summary'])), ENT_QUOTES);
            } else {
                $this->error['summary'] = 'Es wurde keine Zusammenfassung eingegeben.';
            }
        }

        if ($item['text'] !== null) {
            $itemcheck['text'] = $item['text'];
        } else {
            $this->error['text'] = 'es wurde kein Text eingegben.';
        }
        if (is_numeric($item['category'])) {
            $itemcheck['category'] = $item['category'];
        }
        if (is_numeric($item['id_number'])) {
            $itemcheck['id_number'] = $item['id_number'];
        }
        $itemcheck['points'] = (int) $item['points'];
        $itemcheck['correct'] = strval($item['correct']);
        $itemcheck['grammar'] = (int) $item['grammar'];
        $itemcheck['notice'] = htmlentities((string) $item['notice'], ENT_QUOTES);
        $itemcheck['spelling'] = (int) $item['spelling'];
        $itemcheck['term'] = (int) $item['term'];
        $itemcheck['sign'] = $item['sign'];
        $itemcheck['new'] = $item['new'];
        $itemcheck['sly'] = $item['sly'];
        $itemcheck['gryff'] = $item['gryff'];
        $itemcheck['huffle'] = $item['huffle'];
        $itemcheck['rav'] = $item['rav'];
        $this->putData($itemcheck);
    }

    public static function insertIssue($item_id, $kind = 'NEW'): void
    {
        /** @var WbbDatabase $db */
        global $db;
        $set = null;
        $kind = strtoupper((string) $kind);
        $issue = Issue::getValue('status', 'NEW', 'id_number');
        if ($kind !== 'NEW') {
            $issue = Issue::getValue('status', 'NOW', 'id_number');
            $set = ", status='NOW' ";
        }
        $sql = 'update ' . TITEM . " set id_issue = $issue $set where id_number = $item_id";
        $db->query($sql);
    }

    public function delData(): void
    {
        /** @var WbbDatabase $db */
        global $db;
        $sql = 'update ' . TITEM . " set del = 1 where id_number = {$this->item['id_number']}";
        $db->query($sql);
    }

    public function undelete($id_number): void
    {
        /** @var WbbDatabase $db */
        global $db;
        $sql = 'update ' . TITEM . " set del = 0, id_issue = 0, status = 'NEW' where id_number = $id_number";
        $db->query($sql);
    }

    /** @return array{id_number: int, title: mixed, category: mixed, text: mixed, grammar: mixed, spelling: mixed, term: mixed, notice: mixed, points: mixed, sign: mixed, update_date: mixed, correct: mixed, new: mixed, sly: mixed, gryff: mixed, huffle: mixed, rav: mixed} */
    protected function clearData(): array
    {
        return [
            'id_number' => (int) $this->item['id_number'],
            'title' => $this->item['title'],
            'category' => $this->item['category'],
            'text' => $this->item['text'],
            'grammar' => $this->item['grammar'],
            'spelling' => $this->item['spelling'],
            'term' => $this->item['term'],
            'notice' => $this->item['notice'],
            'points' => $this->item['points'],
            'sign' => $this->item['sign'],
            'update_date' => $this->item['update_date'],
            'correct' => $this->item['correct'],
            'new' => $this->item['new'],
            'sly' => $this->item['sly'],
            'gryff' => $this->item['gryff'],
            'huffle' => $this->item['huffle'],
            'rav' => $this->item['rav'],
        ];
    }

    /** Array wird in die Datenbank eingefügt */
    protected function insertData($item): int
    {
        /** @var WbbDatabase $db */
        global $db;
        $item['status'] = 'NEW';
        $keys = ' (title, text, correct, category, status, insert_date, grammar, spelling, term, notice, points) ';
        $values = " ('$item[title]', '" . mysql_real_escape_string(
            $item['text']
        ) . "','$item[correct]', $item[category], 'NEW', curdate(), '$item[grammar]', '$item[spelling]', '$item[term]', '$item[notice]', '$item[points]') ";
        $sql = 'insert into ' . TITEM . "$keys VALUES $values";
        $db->query($sql);
        if ($db->affected_rows === 0) {
            $this->error['INSERT'] = true;
        } else {
            $this->item['id_number'] = $db->insert_id();
        }

        return (int) $this->item['id_number'];
    }

    /**
     * Daten wird in klaasseneignes Array eingefügt.
     *
     * @param mixed[] $item
     */
    protected function putData(array $item): void
    {
        $this->item = $item;
    }
}
