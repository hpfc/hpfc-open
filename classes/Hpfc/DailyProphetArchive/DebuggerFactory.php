<?php

declare(strict_types=1);

namespace Hpfc\DailyProphetArchive;

class DebuggerFactory
{
    public static function createDebugger($type): DebuggerEcho|DebuggerLog|DebuggerVoid
    {
        return match (strtolower((string) $type)) {
            'echo' => DebuggerEcho::getInstance(),
            'log' => DebuggerLog::getInstance(),
            'void' => DebuggerVoid::getInstance(),
            default => throw new UnknownDebuggerException(),
        };
    }
}
