<?php

/**
 * Verwaltung der Kategorien.
 *
 * @author      Jana Pape (j.pape@dreagan-lin.de)
 *
 * @package    HPFC\Tagesprophet
 *
 * @copyright   2009 Jana Pape
 * All rights reserved.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License (GPL)
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * To read the license please visit http://www.gnu.org/copyleft/gpl.html
 * Definition von Ausgabefunktionen
 */
declare(strict_types=1);

namespace Hpfc\DailyProphetArchive;

use Hpfc\Wbb21Shims\WbbDatabase;

/**
 * Verwaltung der Kategorien.
 */
class Category
{
    public static function getName($category)
    {
        /** @var WbbDatabase $db */
        global $db;
        $sql = 'select * from ' . TCATEGORY . " where id_number like '" . $category . "';";
        $return = $db->query_first($sql);

        return $return['name'];
    }

    /** @return mixed[] */
    public static function listData(): array
    {
        /** @var WbbDatabase $db */
        global $db;
        $return = [];
        $sql = 'select * from ' . TCATEGORY . ';';
        $erg = $db->query($sql);
        $i = 0;
        while ($result = $db->fetch_array($erg)) {
            $return[$i] = $result;
            $i++;
        }

        return $return;
    }
}
