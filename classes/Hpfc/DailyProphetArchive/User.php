<?php

/**
 * Userbearbeitung.
 *
 * @author      Jana Pape (j.pape@dreagan-lin.de)
 *
 * @package    HPFC\Tagesprophet
 *
 * @copyright   2009 Jana Pape
 * All rights reserved.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License (GPL)
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * To read the license please visit http://www.gnu.org/copyleft/gpl.html
 * Definition von Ausgabefunktionen
 */
declare(strict_types=1);

namespace Hpfc\DailyProphetArchive;

use Exception;
use Hpfc\Wbb21Shims\WbbDatabase;

/**
 * Userbearbeitung.
 */
class User
{
    public static $instance;

    protected array $user = [];

    protected array $error = [];

    protected array $item = [];

    protected function __construct($id_item = null)
    {
        $this->item['id_item'] = $id_item;
        $this->loadData();
    }

    /** Item wird aus Datenbank in das klasseneigne Array geladen */
    public function loadData(): void
    {
        /** @var WbbDatabase $db */
        global $db, $debuggerLog;
        $user_temp = [];
        $sql = 'select * from ' . TITEMUSER . " where id_item like '" . $this->item['id_item'] . "' order by kind;";
        $erg = $db->query($sql);
        if ($db->num_rows() !== 0) {
            $i = 0;
            while ($result = $db->fetch_array($erg)) {
                $sql = 'select * from ' . TUSER . " where user_id like '" . $result['user_id'] . "';";
                try {
                    $user_temp[$i] = $db->query_first($sql);
                    $user_temp[$i]['id_item'] = $this->user['id_item'];
                    $user_temp[$i]['kind'] = $result['kind'];
                    $i++;
                } catch (Exception) {
                    $debuggerLog->debug(
                        '.User ' . $result['user_id'] . ' nicht in der Tabelle ' . TUSER . ' enthalten.'
                    );
                }
            }
            $this->putData($user_temp);
        }
    }

    public static function getInstance($id_item = null): self|null
    {
        if (self::$instance === null) {
            self::$instance = new self($id_item);
        }

        return self::$instance;
    }

    /** @return mixed[] */
    public static function getList(array $cause = [], array $sort = [], string $desc = null): array
    {
        /** @var WbbDatabase $db */
        global $db;
        $return = [];
        $condition = null;
        $order = null;
        if ($desc !== null) {
            $desc = ' DESC';
        }
        if ($condition !== null) {
            $condition .= ' AND ';
        }
        foreach ($cause as $key => $value) {
            $condition .= $key . " like '" . $value . "' ";
        }
        $condition = ' where ' . $condition;
        if ($order !== null) {
            $order .= ', ';
            foreach ($sort as $value) {
                $order .= ' ' . $value;
            }
            $order = ' order by ' . $order . $desc;
        }
        $sort = ' order by kind';
        $sql = 'select * from ' . TUSER . $condition . $order . $sort . ';';
        $erg = $db->query($sql);
        $i = 0;
        while ($result = $db->fetch_array($erg)) {
            $return[$i] = $result;
            $i++;
        }

        return $return;
    }

    /** @return array<int, array{username: mixed, userid: mixed, hpfc_id: mixed}> */
    public static function listTP(): array
    {
        /** @var WbbDatabase $db */
        global $db;
        $user = [];
        $sql = 'SELECT u.userid as userid,  u.username as username, u.hpfc_id as hpfc_id  FROM hpfc_groups gr LEFT JOIN hpfc_user u ON u.hpfc_id=gr.hpfcgroupsid WHERE gr.canpresse=1';
        $result = $db->query($sql);
        $i = 0;
        while ($erg = $db->fetch_array($result)) {
            $user[$i]['username'] = $erg['username'];
            $user[$i]['userid'] = $erg['userid'];
            $user[$i]['hpfc_id'] = $erg['hpfc_id'];
            $i++;
        }

        return $user;
    }

    /** @return array<int, array{kind: mixed, username: mixed, user_id: mixed, haus: mixed}> */
    public static function getUser($id_item): array
    {
        /** @var WbbDatabase $db */
        global $db;
        $sql = 'select * from ' . TITEMUSER . " where id_item like '" . $id_item . "' order by kind;";
        $erg = $db->query($sql);
        $i = 0;
        $temp_user = [];
        while ($result = $db->fetch_array($erg)) {
            $sql = 'select * from ' . TUSER . " where user_id like '" . $result['user_id'] . "';";
            $temp = $db->query_first($sql);
            $temp_user[$i]['kind'] = $result['kind'];
            $temp_user[$i]['username'] = $temp['username'];
            $temp_user[$i]['user_id'] = $temp['user_id'];
            $temp_user[$i]['haus'] = $temp['haus'];
            $i++;
        }

        return $temp_user;
    }

    public function saveData($item_id): void
    {
        /** @var WbbDatabase $db */
        global $db;
        $sql = 'select count(*) as count from ' . TITEMUSER . " where id_item like '" . $item_id . "';";
        $counter = $db->query_first($sql);
        if ($counter['count'] === 0) {
            foreach ($this->user as $user_temp) {
                // user einfügen in TITEMUSER
                $sql = 'select count(*) as count from ' . TUSER . ' where user_id = ' . $user_temp['user_id'] . " AND username = '" . $user_temp['username'] . "';";
                $counter2 = $db->query_first($sql);
                if ($counter2['count'] === 0) {
                    $this->insertData($item_id, $user_temp);
                } else {
                    // insert in TITEMUSER
                    $key = '(user_id,id_item,kind)';
                    $values = $user_temp['user_id'] . ',' . (int) $item_id . ",'" . $user_temp['kind'] . "'";
                    $sql = 'insert into ' . TITEMUSER . ' ' . $key . ' Values(' . $values . ');';
                    $db->query($sql);
                }
            }
        } else {
            // schaun, ob User in Array auch in DB steht ansonsten einfügen.
            foreach ($this->user as $user_temp) {
                $sql = 'select count(*) as count from ' . TITEMUSER . ' where user_id like ' . $user_temp['user_id'] . " and kind like '" . $user_temp['kind'] . "' and  id_item like " . $item_id . ';';
                $user_count = $db->query_first($sql);
                if ($user_count['count'] === 0) {
                    // User in TITEMUSER einfügen
                    $this->insertData($item_id, $user_temp);
                }
            }
        }
        $sql = 'select count(*) as count from ' . TITEMUSER . " where id_item like '" . $item_id . "';";
        $counter = $db->query_first($sql);
        if ($counter['count'] !== count($this->user)) {
            $sql = 'select * from ' . TITEMUSER . " where id_item like '" . $item_id . "';";
            $result = $db->query($sql);
            while ($erg = $db->fetch_array($result)) {
                $erg['IntoArray'] = false;
                foreach ($this->user as $user_temp) {
                    if ($user_temp['user_id'] === $erg['user_id']) {
                        $erg['IntoArray'] = true;
                    }
                }
                if (!$erg['IntoArray']) {
                    $sql = 'delete from ' . TITEMUSER . " where user_id like '" . $erg['user_id'] . "' and id_item like '" . $item_id . "';";
                    $db->query($sql);
                }
            }
        }
    }

    /** @return mixed[] */
    public function getData(): array
    {
        return $this->user;
    }

    /** Userid wird auf Vorhandensein geprüft. */
    public function checkData($userarray): void
    {
        /** @var WbbDatabase $db */
        global $db;
        $userdata = [];
        $i = 0;
        if ($userarray[0]['user_id'] !== 0 || $userarray[0]['user_id'] !== null) {
            foreach ($userarray as $user) {
                $sql = 'SELECT * FROM ' . BBUSER . " WHERE userid = '" . $user['user_id'] . "';";
                $erg = $db->query($sql);
                if ($db->num_rows() > 0) {
                    while ($temp_user = $db->fetch_array($erg)) {
                        $bbuserdata[$i]['username'] = $temp_user['username'];
                        $bbuserdata[$i]['hid'] = $temp_user['haus'];
                        $bbuserdata[$i]['kind'] = $user['kind'];
                        $bbuserdata[$i]['user_id'] = $user['user_id'];
                        $bbuserdata[$i]['id_item'] = $user['id_item'];
                    }
                } else {
                    $userdata[$i]['user_id'] = $user['user_id'];
                    $userdata[$i]['error'] = 'User nicht gefunden.';
                }

                $sql = 'select count(*) as count  from ' . TUSER . " where user_id like '" . $user['user_id'] . "';";
                $count = $db->query_first($sql);
                if ($count['count'] !== 0) {
                    $sql = 'select * from ' . TUSER . " where  user_id = '" . $user['user_id'] . "';";
                    $erg = $db->query($sql);
                    while ($temp_user = $db->fetch_array($erg)) {
                        if ($temp_user['username'] === $bbuserdata[$i]['username']) {
                            $userdata[$i]['username'] = $temp_user['username'];
                            $userdata[$i]['hid'] = $temp_user['hid'];
                            $userdata[$i]['kind'] = $user['kind'];
                            $userdata[$i]['user_id'] = $user['user_id'];
                            $userdata[$i]['id_item'] = $user['id_item'];
                        } else {
                            $userdata[$i]['username'] = $bbuserdata[$i]['username'];
                            $userdata[$i]['hid'] = $bbuserdata[$i]['hid'];
                            $userdata[$i]['kind'] = $user['kind'];
                            $userdata[$i]['user_id'] = $user['user_id'];
                            $userdata[$i]['id_item'] = $user['id_item'];
                            $updatesql = 'update ' . TUSER . " SET username = '" . $bbuserdata[$i]['username'] . "' where  user_id = '" . $user['user_id'] . "';";
                            $db->query($updatesql);
                        }
                    }
                } else {
                    $userdata[$i]['username'] = $bbuserdata[$i]['username'];
                    $userdata[$i]['hid'] = $bbuserdata[$i]['hid'];
                    $userdata[$i]['kind'] = $user['kind'];
                    $userdata[$i]['user_id'] = $user['user_id'];
                    $userdata[$i]['id_item'] = $user['id_item'];
                }
                if ($userdata[$i]['username'] === null) {
                    $userdata[$i]['error'] = 'Keinen User gefunden';
                }

                switch ($userdata[$i]['hid']) {
                    case '1':
                        $userdata[$i]['haus'] = 'Slytherin';
                        break;
                    case '2':
                        $userdata[$i]['haus'] = 'Ravenclaw';
                        break;
                    case '3':
                        $userdata[$i]['haus'] = 'Gryffindor';
                        break;
                    case '4':
                        $userdata[$i]['haus'] = 'Hufflepuff';
                        break;
                    case '5':
                        $userdata[$i]['haus'] = 'Hauslos';
                        break;
                }
                $i++;
            }
            $this->putData($userdata);
        } else {
            $this->error['noautor'] = 'Kein Autor angegeben';
        }
    }

    /** @return string[] */
    public function getError(): array
    {
        return $this->error;
    }

    public function delData(): void
    {
        /** @var WbbDatabase $db */
        global $db;
        $sql = 'update ' . TITEMUSER . " set del='1' where id_item like '" . $this->item['id_item'] . "'";
        $db->query($sql);
    }

    public function restoreData($id_item): void
    {
        /** @var WbbDatabase $db */
        global $db;
        $sql = 'update ' . TITEMUSER . " set del='0' where id_item like '" . $id_item . "'";
        $db->query($sql);
    }

    /**
     * Daten wird in klaasseneignes Array eingefügt.
     *
     * @param mixed[] $user
     */
    protected function putData(array $user): void
    {
        $this->user = $user;
    }

    protected function insertData($item_id, $user): void
    {
        /** @var WbbDatabase $db */
        global $db;
        $sql = 'select count(*) as count from ' . TUSER . " where user_id like '" . $user['user_id'] . "';";
        $counter = $db->query_first($sql);
        if ($counter['count'] === 0) {
            // insert in TUSER
            $key = '(user_id, username, haus)';
            $values = "'" . $user['user_id'] . "','" . $user['username'] . "','" . $user['haus'] . "'";
            $sql = 'insert into ' . TUSER . ' ' . $key . ' Values(' . $values . ');';
            $db->query($sql);
        }
        // insert in TITEMUSER
        $key = '(user_id,id_item,kind)';
        $values = $user['user_id'] . ',' . (int) $item_id . ",'" . $user['kind'] . "'";
        $sql = 'insert into ' . TITEMUSER . ' ' . $key . ' Values(' . $values . ');';
        $db->query($sql);
    }
}
