<?php
/**
 * Ausgaben verarbeiten.
 *
 * @author      Jana Pape (j.pape@dreagan-lin.de)
 *
 * @package    HPFC\Tagesprophet
 *
 * @copyright   2009 Jana Pape
 * All rights reserved.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License (GPL)
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * To read the license please visit http://www.gnu.org/copyleft/gpl.html
 * Definition von Ausgabefunktionen
 */
declare(strict_types=1);

namespace Hpfc\DailyProphetArchive;

/**
 * Ausgaben verarbeiten.
 */
class Issue extends Guide
{
    protected array $issue = [];

    protected array $error = [];

    protected function __construct(int|null $id_number = null)
    {
        $this->issue['id_number'] = $id_number;
        $this->loadData();
    }

    /** Item wird aus Datenbank in das klasseneigne Array geladen */
    public function loadData(): void
    {
        /** @var \Hpfc\Wbb21Shims\WbbDatabase $db */
        global $db;
        $sql = 'select * from ' . TISSUE . " where id_number like '" . (int) $this->issue['id_number'] . "';";
        $issue = $db->query_first($sql);
        if ($this->issue['id_number'] === null) {
            $sql = 'select * from ' . TISSUE . " where status like 'NEW';";
            $issue = $db->query_first($sql);
        }
        $this->issue['id_number'] = $issue['id_number'];
        $this->issue['number'] = $issue['number'];
        $this->issue['text'] = $issue['text'];
        $this->issue['publish_date'] = $issue['publish_date'];
        $this->issue['status'] = $issue['status'];
        $this->issue['text'] = $issue['text'];
        $this->issue['first_correct'] = $issue['first_correct'];
        $this->issue['second_correct'] = $issue['second_correct'];
        $this->issue['signer'] = $issue['signer'];
        $this->issue['del'] = $issue['del'];
    }

    public static function getInstance(int $id_number = null): Guide
    {
        if (self::$instance === null) {
            self::$instance = new self($id_number);
        }

        return self::$instance;
    }

    /**
     * Daten, die übergeben werden, werden aus Kausalität überprüft.
     *
     * @return mixed[]
     */
    public static function getNavigation(): array
    {
        /** @var \Hpfc\Wbb21Shims\WbbDatabase $db */
        global $db;
        $return = [];
        $i = 0;
        $sql = 'select category ,count(*) as anzahl from ' . TITEM . " where status like 'NOW' group by category";
        $erg = $db->query($sql);
        while ($result = $db->fetch_array($erg)) {
            $return[$i] = $result;
            $i++;
        }
        $sql = 'select count(*) as anzahl from ' . TREADERS . " where status like 'NOW'";
        $readers = [
            'category' => 'readers',
            $db->query_first($sql),
        ];
        $return[$i] = $readers;
        $i++;
        $sql = 'select count(*) as anzahl from ' . TCONTACT . " where status like 'NOW'";
        $contact = [
            'category' => 'contact',
            $db->query_first($sql),
        ];
        $return[$i] = $contact;

        return $return;
    }

    /** @return mixed[] */
    public static function getList(array $cause = [], array $sort = [], $desc = 'ASC'): array
    {
        /** @var \Hpfc\Wbb21Shims\WbbDatabase $db */
        global $db;
        $return = [];
        $condition = null;
        $order = null;
        foreach ($cause as $key => $value) {
            if ($condition !== null) {
                $condition .= ' AND ';
            }
            $condition .= $key . " like '" . $value . "' ";
        }
        $condition = ' where ' . $condition;
        if ($desc !== 'ASC') {
            $desc = 'DESC';
        }
        if ($sort !== []) {
            if ($order !== null) {
                $order .= ', ';
            }
            foreach ($sort as $value) {
                $order .= ' ' . $value;
            }
            $order = ' order by ' . $order . ' ' . $desc;
        }
        $sql = 'select * from ' . TISSUE . $condition . $order . ';';
        $erg = $db->query($sql);
        $i = 0;
        while ($result = $db->fetch_array($erg)) {
            $return[$i] = $result;
            $i++;
        }

        return $return;
    }

    public static function getValue($key, $value, $back)
    {
        /** @var \Hpfc\Wbb21Shims\WbbDatabase $db */
        global $db;
        $sql = 'select ' . $back . ' from ' . TISSUE . ' where ' . $key . " like '" . $value . "';";
        $return = $db->query_first($sql);

        return $return[$back];
    }

    public static function getIssue($kind)
    {
        /** @var \Hpfc\Wbb21Shims\WbbDatabase $db */
        global $db;
        $sql = 'select * from ' . TISSUE . " where status like '" . $kind . "';";

        return $db->query_first($sql);
    }

    /**
     * Array wird in der Datenbank gespeichert.
     *
     * @return mixed[]
     */
    public function getData(): array
    {
        return $this->issue;
    }

    /** @return mixed[] */
    public function getError(): array
    {
        return $this->error;
    }
}
