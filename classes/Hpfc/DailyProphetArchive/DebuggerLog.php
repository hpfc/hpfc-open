<?php

declare(strict_types=1);

namespace Hpfc\DailyProphetArchive;

class DebuggerLog implements Debugger
{
    private static \Hpfc\DailyProphetArchive\DebuggerLog|null $instance = null;

    public static function getInstance(): self
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    public function debug($message): void
    {
        global $scriptpath;
        error_log('{$message\n}', 3, $scriptpath . 'LOG/tp.log');
    }
}
